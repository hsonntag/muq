function [x_k,wcc] = compute_cc(n)
%from http://www.sam.math.ethz.ch/~waldvoge/Papers/fejer.html

% Weights of the Fejer2, Clenshaw-Curtis and Fejer1 quadratures by DFTs
% n>1. Nodes: x_k = cos(k*pi/n)

if(n==1)
    x_k = 0.0;
    wcc = 2.0;
else


N=[1:2:n-1]'; l=length(N); m=n-l; K=[0:m-1]';

v0=[2./N./(N-2); 1/N(end); zeros(m,1)];
v2=-v0(1:end-1)-v0(end:-1:2); wf2=ifft(v2);


x_k = cos((0:(n))*pi/n);

%Clenshaw-Curtis nodes: k=0,1,...,n; weights: wcc, wcc_n=wcc_0
g0=-ones(n,1); g0(1+l)=g0(1+l)+n; g0(1+m)=g0(1+m)+n;
g=g0/(n^2-1+mod(n,2)); wcc=ifft(v2+g);

wcc(end+1) = wcc(1);
end