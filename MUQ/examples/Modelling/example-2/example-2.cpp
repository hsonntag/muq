/** \example modelling-example-2.cpp
 *
 *  <h3> About this example </h3>
 *  <p>This example builds on example-1 by providing a more general implementation of the predator-prey model considered
 * before:
 *  \f{eqnarray*}{
 *  \frac{dP}{dt} & = & rP\left(1-\frac{P}{K}\right) - s\frac{PQ}{a+P}\\
 *  \frac{dQ}{dt} & = & u\frac{PQ}{a+P} - vQ
 *  \f}
 *  The focus is still only on implementating the right hand side of this ODE system, but this example will not fix the
 * growth
 *  and death rates.  Instead, \f$r\f$, \f$K\f$, \f$s\f$, \f$a\f$, \f$u\f$, and \f$v\f$ will be lumped into a second
 * vector valued input.
 *  The model will thus be defined as a function \f$f : R^2\times R^6 \rightarrow R^2 \f$.
 *  </p>
 *
 *  <p>
 *  From the MUQ perspective, defining a multiple input Model requires inheriting from the more detailed ModPiece class
 * instead of one the
 *  single input template classes.
 *  </p>
 *
 *  <h3>Line by line explanation</h3>
 *  <p>See example-2.cpp for finely documented code.</p>
 *
 *  <h3> Compiling this example and linking to MUQ </h3>
 *  <p>This code can be compiled in the same way as example-1.  That is, create a build directory, run cmake to find MUQ
 *  and create the Makefile, and then run "make" to compile the executable.</p>
 */
// Just as before, we will need Eigen and std::cout
#include <Eigen/Core>
#include <iostream>

// In this example, we will work directly from the ModPiece base class instead of the single input templates.  This line
// includes the ModPiece base directly.
#include <MUQ/Modelling/ModPiece.h>

// as before, we'll work in the muq::modelling and std namespaces
using namespace muq::Modelling;
using namespace std;


/*
 * As in example-1, this class describes the right hand side of our predator prey model.
 */
class PredPreyModel : public ModPiece {
public:

  /* Unlike the previous example, our Model constructor needs to call the ModPiece constructor directly.  Like the
   * templates, the ModPiece constructor requires the input dimensions and output dimensions.  However, the ModPiece
   * constructor also requires us to tell it a bit about our model. Specifically, the ModPiece class needs to knoow
   * what kind of derivative information this class provides, and whether or not the output of the Model is stochastic.
   * Also note that now we have to define multiple input sizes.  In general, this is done with an Eigen::VectorXi object
   * passed to the PredPreyModel constructor.  However, we have used a little shorthand here, "Eigen::Vector2i{2,6}", to
   * construct the vector of input sizes.  The first input containes the current predator and prey populations, while
   * the second input contains the growth/death parameters.
   */
  PredPreyModel() : ModPiece(Eigen::Vector2i{2, 6},        // input sizes (this examples uses the c++11 ability to construct vectors with a curly bracketed array)
                             2,        // the output dimension, still 2 as in example-1
                             false,    // does this class provide an efficient implementation of the GradientImpl function?
                             false,    // does this class provide an efficient implementation of the JacobianImpl function?
                             false,    // does this class provide an efficient implementation of the JacobianActionImpl function?
                             false,    // does this class provide an efficient implementation of the HessianImpl function?
                             false) {} // does this class return a random result?

  // virtual default constructor as before
  virtual ~PredPreyModel() = default;

private:

  /* Even with more than one input, the EvaluateImpl function performs the forward model computations.  However, when a
   * model has
   * multiple inputs, EvaluateImpl takes a container of vector valued inputs instead of a single Eigen::VectorXd.
   */
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override
  {
    // for clarity, we grab the prey and predator populations from the input vector
    double preyPop = inputs.at(0) (0); // notice the indexing here, the .at(0) extracts the first vector valued input
                                       // and the .at(0)(0) grabs the first entry in the first input vector
    double predPop = inputs.at(0) (1);

    // extract the model parameters from the input vector.  Note this requires an extra copy of the model parameters,
    // but results in more readable code
    double preyGrowth, preyCapacity, predationRate, predationHunger, predatorLoss, predatorGrowth;

    preyGrowth      = inputs.at(1) (0);
    preyCapacity    = inputs.at(1) (1);
    predationRate   = inputs.at(1) (2);
    predationHunger = inputs.at(1) (3);
    predatorLoss    = inputs.at(1) (4);
    predatorGrowth  = inputs.at(1) (5);

    // create a vector to hold the ModPiece output, this will hold the time derivatives of population
    Eigen::VectorXd output(2);

    // compute the growth rates as we did in example-1
    output(0) = preyPop * (preyGrowth * (1 - preyPop / preyCapacity) - predationRate * predPop / (predationHunger + preyPop));
    output(1) = predPop * (predatorGrowth * preyPop / (predationHunger + preyPop) - predatorLoss);

    // return the output
    return output;
  }
};


/* As in the single input case, the model is evalauted using the Evaluate function of the ModPiece class instead of the
 * EvaluateImpl function from
 * the PredPreyModel class.  However, with multiple inputs, there are two ways to call Evaluate.  These are illustrated
 *below.
 */
int main()
{
  // create an instance of the modpiece defining the predator prey model
  auto predatorPreyModel = make_shared<PredPreyModel>();

  // set the population sizes
  Eigen::VectorXd populations(2);

  populations << 50, 5; // prey population, predator population

  // set the interaction parameters
  Eigen::VectorXd growthParams(6);
  growthParams << 0.8, 100, 4.0, 15, 0.6, 0.8; //preyGrowth, preyCapacity, predationRate, predationHunger, predatorLoss,
                                               // predatorGrowth;

  // Evaluate the predator prey growth rate ModPiece as we did before, but now include the second input
  Eigen::VectorXd growthRates = predatorPreyModel->Evaluate(populations, growthParams);

  // print the growth rates
  cout << "\nModel output with multiple argument Evaluate call:\n";
  cout << "  The prey growth rate evaluated at     (" << populations(0) << "," << populations(1) << ") is " <<
  growthRates(0) << endl;
  cout << "  The predator growth rate evaluated at (" << populations(0) << "," << populations(1) << ") is " <<
  growthRates(1) << endl;


  /* Notice that we called the Evaluate function above with multiple Eigen::VectorXd arguments even though the
   * EvaluateImpl function we defined
   * in the PredPreyModel class accepts a single argument containing a std::vector of Eigen::VectorXd objects.  Have no
   *fear, it is also possible
   * to call the Evaluate the model with a single argument, as shown below.  In fact, the multiple argument call the
   *Evaluate pushes all the inputs
   * into a single container and calls the single argument version.
   */
  vector<Eigen::VectorXd> modelParams;
  modelParams.push_back(populations);
  modelParams.push_back(growthParams);

  // Evaluate the model again with the single argument
  growthRates = predatorPreyModel->Evaluate(modelParams);

  // print the growth rates
  cout << "\nModel output with single argument Evaluate call:\n";
  cout << "  The prey growth rate evaluated at     (" << modelParams.at(0) (0) << "," << modelParams.at(0) (1) <<
  ") is " << growthRates(0) << endl;
  cout << "  The predator growth rate evaluated at (" << modelParams.at(0) (0) << "," << modelParams.at(0) (1) <<
  ") is " << growthRates(1) << endl << endl;
}

