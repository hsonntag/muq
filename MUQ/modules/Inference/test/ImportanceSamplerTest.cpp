#include "gtest/gtest.h"

#include <boost/property_tree/ptree.hpp> // this include is weird, but must be here to avoid a strange bug on osx

#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Utilities/RandomGenerator.h"

#include "MUQ/Modelling/GaussianPair.h"
#include "MUQ/Modelling/VectorPassthroughModel.h"

#include "MUQ/Inference/ImportanceSampling/ImportanceSampler.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Inference;

class GooMod : public ModPiece {
public:

  GooMod() : ModPiece(Eigen::Vector2i(3, 3), 3, false, false, false, false, false) {}

  virtual ~GooMod() = default;

private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override
  {
    return inputs[1];
  }
};

TEST(ImportanceSampling, BasicTest)
{
  RandomGeneratorTemporarySetSeed tempSeed(56335740);

  // want the expectation of this model
  auto f = make_shared<GooMod>();

  // with respect to this density
  Eigen::Vector3d piMean(0.1, 0.01, 0.0);
  Eigen::MatrixXd piCov(3, 3);

  piCov <<
  0.5, 0.1, 0.0,
  0.1, 0.5, 0.0,
  0.0, 0.0, 0.5;
  auto pi = make_shared<GaussianDensity>(piMean, piCov);

  // importance sampline density and RV
  auto qPair = make_shared<GaussianPair>(3);

  // sampling this many times
  const unsigned int N = 1e5;

  // with respect to this input
  const unsigned int inputDimWrt = 1;

  auto graph = make_shared<ModGraph>();

  graph->AddNode(f, "function");
  graph->AddNode(pi, "pi");
  graph->AddNode(make_shared<VectorPassthroughModel>(3), "para");
  graph->AddNode(make_shared<VectorPassthroughModel>(3), "inputPara");
  graph->AddNode(qPair->density, "qDens");
  graph->AddNode(qPair->rv, "qRV");

  graph->AddEdge("para", "pi", 0);
  graph->AddEdge("para", "qDens", 0);
  graph->AddEdge("para", "function", 1);
  graph->AddEdge("inputPara", "function", 0);

  boost::property_tree::ptree pt;

  pt.put("ImportanceSampler.OutputNames", "function,pi,qDens,qRV");
  pt.put("ImportanceSampler.MarginalizedNode", "para");
  pt.put("ImportanceSampler.N", N);
  pt.put("ImportanceSampler.Normalize", true);

  // create this IP sampler
  auto ipSampler = make_shared<ImportanceSampler>(graph, pt);

  Eigen::VectorXd input(3);
  input << 1.0, 2.0, 3.0;

  Eigen::VectorXd result = ipSampler->Evaluate(input);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, piMean, result, 1e-2);
}
