
# define the source files in this directory that we want to include in the
#   generic interface library
SET(Inference_SOURCES


    MCMC/AMProposal.cpp
    MCMC/AMALA.cpp
    MCMC/DRKernel.cpp
    MCMC/GaussianIndependence.cpp
    MCMC/MCMCBase.cpp
    MCMC/MCMCKernel.cpp
    MCMC/MCMCProposal.cpp
    MCMC/MCMCState.cpp
    MCMC/MHProposal.cpp
    MCMC/MHKernel.cpp
    MCMC/MixtureProposal.cpp
    MCMC/MMALA.cpp
    MCMC/PreMALA.cpp
    MCMC/SingleChainMCMC.cpp
    MCMC/StanNUTS.cpp
    MCMC/StanHMC.cpp
    MCMC/TransportMapKernel.cpp
    
    ImportanceSampling/ImportanceSampler.cpp

    MAP/MAPbase.cpp
    
    ProblemClasses/SamplingProblem.cpp
    ProblemClasses/InferenceProblem.cpp
    ProblemClasses/MarginalInferenceProblem.cpp
    ProblemClasses/AbstractMetric.cpp
    ProblemClasses/StanModelInterface.cpp
    ProblemClasses/GaussianFisherInformationMetric.cpp

    TransportMaps/MapFactory.cpp
    TransportMaps/MapUpdateManager.cpp
    TransportMaps/TransportMap.cpp
    TransportMaps/PolynomialMap.cpp
    TransportMaps/IsoTransportMap.cpp
    TransportMaps/UnivariateBasis.cpp
    TransportMaps/Sigmoids.cpp
    TransportMaps/RbfMap.cpp
    TransportMaps/BasisSet.cpp
    TransportMaps/BasisFactory.cpp
  )

if(MUQ_USE_NLOPT AND Approximation_build)
    set(Inference_SOURCES
        ${Inference_SOURCES}
        
  ProblemClasses/MarginalSamplingProblem.cpp
	MCMC/LocalApproximationKernel.cpp
	)
endif(MUQ_USE_NLOPT AND Approximation_build)


if(MUQ_USE_PYTHON)
    set(Inference_SOURCES
        ${Inference_SOURCES}
	
	../python/InferencePythonLibrary.cpp
	
	../python/InferenceProblemPython.cpp  
	../python/MetricPython.cpp 
	
	../python/MCMCBasePython.cpp

	../python/ImportanceSamplerPython.cpp
	
	)
endif(MUQ_USE_PYTHON)


if(MUQ_USE_NLOPT AND MUQ_USE_PYTHON AND Approximation_build)
  set(Inference_SOURCES
      ${Inference_SOURCES}
      
       ../python/MarginalSamplingProblemPython.cpp
	)
endif(MUQ_USE_NLOPT AND MUQ_USE_PYTHON AND Approximation_build)


##########################################
#  Objects to build


# Define the GenericInterface library
ADD_LIBRARY(muqInference ${Inference_SOURCES})
TARGET_LINK_LIBRARIES(muqInference muqModelling muqApproximation muqOptimization muqUtilities ${MUQ_LINK_LIBS})

if(APPLE AND MUQ_USE_PYTHON)
    set_property(TARGET muqInference PROPERTY PREFIX "lib")
    set_property(TARGET muqInference PROPERTY OUTPUT_NAME "muqInference.so")
    set_property(TARGET muqInference PROPERTY SUFFIX "")
    set_property(TARGET muqInference PROPERTY SOVERSION "32.1.2.0")
endif(APPLE AND MUQ_USE_PYTHON)

install(TARGETS muqInference
    EXPORT MUQDepends
    LIBRARY DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
    ARCHIVE DESTINATION "${CMAKE_INSTALL_PREFIX}/lib")
