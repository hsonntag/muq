
#include "MUQ/Inference/TransportMaps/MapUpdateManager.h"

#include <Eigen/Dense>
#include <boost/math/constants/constants.hpp>
#include <boost/math/special_functions/erf.hpp>

using namespace muq::Modelling;
using namespace muq::Inference;
using namespace std;


MapUpdateManager::MapUpdateManager(std::shared_ptr<TransportMap> mapIn,
                                   int expectedSamps,
                                   double priorPrecisionIn) : map(mapIn),
                                                              oldNumSamps(0),
                                                              newNumSamps(0),
                                                              maxNumSamps(expectedSamps),
                                                              priorPrecision(priorPrecisionIn),
                                                              ffs(mapIn->outputSize),
                                                              fs(mapIn->outputSize),
                                                              gs(mapIn->outputSize),
                                                              initialCoeffs(mapIn->GetCoeffs())
{
  assert(map != nullptr);
  assert(maxNumSamps > 0);
  assert(priorPrecision >= 0);

  // initialize the basis evaluation vectors
  for (int d = 0; d < map->outputSize; ++d) {
    ffs.at(d) = make_shared<Eigen::MatrixXd>(Eigen::MatrixXd::Zero(map->GetNumTerms(d), map->GetNumTerms(d)));

    fs.at(d) = make_shared<Eigen::MatrixXd>(maxNumSamps, map->GetNumTerms(d));
    gs.at(d) = make_shared<Eigen::MatrixXd>(maxNumSamps, map->GetNumTerms(d));
  }
}

void MapUpdateManager::UpdateMap(const Eigen::Ref<const Eigen::MatrixXd>& newSamps,
                                std::vector<Eigen::VectorXi>             optLevels,
				                        boost::property_tree::ptree              options)
{
  assert(oldNumSamps + newSamps.cols() <= maxNumSamps);
  newNumSamps = oldNumSamps + newSamps.cols();
  
  if(optLevels.size()==0){
    optLevels.resize(map->outputSize);
    for(int i=0; i<map->outputSize; ++i)
      optLevels.at(i) = Eigen::VectorXi::Constant(1,map->coeffs.at(i).size());
  }
  
  // first, update the basis evaluations
  UpdateBasisEvaluations(newSamps);
  
#pragma omp parallel for schedule(static,1)
  for (int d = map->outputSize-1; d >=0; --d) {

    // first, update the basis evaluations for dimension d
    //UpdateBasisEvaluations(d,newSamps);
    
    // now, update the ff decompositions
    UpdateHessians(d);

    // now that all the hessians and basis evaluations are up to date, solve the optimization problem for the map
    // coefficients
    map->coeffs.at(d) = MinimizeKL(d,optLevels.at(d),options);
  }

  // update the number of samples to use in the optimization problem
  oldNumSamps = newNumSamps;
}

void MapUpdateManager::UpdateBasisEvaluations(const Eigen::Ref<const Eigen::MatrixXd>& newSamps)
{
  map->FillForConstruction(newSamps,fs,gs,oldNumSamps,newNumSamps);
}

void MapUpdateManager::UpdateBasisEvaluations(int dim, const Eigen::Ref<const Eigen::MatrixXd>& newSamps)
{
  fs.at(dim)->block(oldNumSamps,0,newNumSamps-oldNumSamps,fs.at(dim)->cols()) = map->GetBasisValues(newSamps,dim);
  gs.at(dim)->block(oldNumSamps,0,newNumSamps-oldNumSamps,fs.at(dim)->cols()) = map->GetDerivValues(newSamps,dim);
}

void MapUpdateManager::UpdateHessians(int dim)
{
  *ffs.at(dim) += fs.at(dim)->block(oldNumSamps, 0, newNumSamps - oldNumSamps, map->GetNumTerms(dim)).transpose() * fs.at(dim)->block(
      oldNumSamps,
      0,
      newNumSamps - oldNumSamps,
      map->GetNumTerms(dim));
}

Eigen::VectorXd MapUpdateManager::MinimizeKL(int d, const Eigen::VectorXi &optLevels,boost::property_tree::ptree &options)
{

  Eigen::VectorXd x = map->coeffs.at(d);
  for(int level=0; level<optLevels.size(); ++level)
    x.head(optLevels(level)) = MinimizeKL(d,optLevels(level),x.head(optLevels(level)),options);
  return x;
}


Eigen::VectorXd MapUpdateManager::MinimizeKL(int d, const int maxTerm, const Eigen::VectorXd &x0, boost::property_tree::ptree &options)
{
  Eigen::VectorXd x          = x0;
  const double objScale      = 1.0 / newNumSamps;
  const int    dim           = x.size();
  const double machEps       = 2*std::numeric_limits<double>::epsilon();
  const double monoTol       = options.get("TransportMapOpt.MonoTol",sqrt(machEps));
  const int    maxIts        = options.get("TransportMapOpt.MaxIts",15000); // maximum number of newton iterations to take
  const int    maxLineIts    = options.get("TransportMapOpt.MaxLineIts",40);   // maximum allowed line search iterations
  const double c1            = 1e-4; // sufficient decrease
  const double maxStepSize   = options.get("TransportMapOpt.MaxStepSize",sqrt(static_cast<double>(dim)));
  const double stepScale     = options.get("TransportMapOpt.StepScale",1.0);
  const double gtol          = options.get("TransportMapOpt.gtol",1e-5); // tolerance on the gradient magnitude
  const double ftol          = options.get("TransportMapOpt.ftol",1e-6); // tolerance on the change in the objective function
  const double xtol          = options.get("TransportMapOpt.xtol",1e-6); // tolerance on the change in the position
  const int    verbosity     = options.get("TransportMapOpt.Verbosity",0);
  const bool   useFullHess   = options.get("TransportMapOpt.FullHess",true);
  
  //Eigen::VectorXd fx = F*x;
  Eigen::VectorXd gx = gs.at(d)->topRows(newNumSamps).leftCols(maxTerm) * x;
  // check for initial feasibility.  If not feasible, find a feasible point
  int minInd;
  double minGx = gx.minCoeff(&minInd);

  // If the map is not monotone at all the points, make the initial map the identity
  if (minGx < machEps) {
    // find the nonlinear terms and set those coefficients to zero
    for (int i = 0; i < x.size(); ++i) {
      if (abs(gs.at(d)->col(i).head(newNumSamps).minCoeff() - gs.at(d)->col(i).head(newNumSamps).maxCoeff()) >
          machEps) {
        x(i) = 0.0;
      }
    }

    // make sure we did what we were supposed to do
    gx    = gs.at(d)->topRows(newNumSamps).leftCols(maxTerm) * x;
    minGx = gx.minCoeff(&minInd);
    assert(minGx > monoTol);
  }


  Eigen::VectorXd diff = x - initialCoeffs.at(d).head(maxTerm);

  double logTerm = gx.array().log().sum();
  double regTerm = 0.5 *priorPrecision *diff.squaredNorm();
  double obj = (0.5 * (fs.at(d)->topRows(newNumSamps).leftCols(maxTerm) * x).squaredNorm() - logTerm) * objScale + regTerm;

  Eigen::VectorXd logGrad = gs.at(d)->topRows(newNumSamps).leftCols(maxTerm).transpose() * gx.array().inverse().matrix();
  Eigen::VectorXd grad = (ffs.at(d)->topLeftCorner(maxTerm,maxTerm).selfadjointView<Eigen::Lower>() * x - logGrad) * objScale + priorPrecision * diff;


  bool belowGTOL = false;
  bool belowFTOL = false;
  bool belowXTOL = false;

  
  Eigen::LDLT<Eigen::MatrixXd> hessSolver;
  Eigen::MatrixXd hess;
  if(!useFullHess){
    Eigen::VectorXd ggScale = gx.array().pow(-2.0).matrix();
    auto ggs = gs.at(d)->topRows(newNumSamps).leftCols(maxTerm).transpose() * ggScale.asDiagonal() * gs.at(d)->topRows(newNumSamps).leftCols(maxTerm);
    hess = ffs.at(d)->topLeftCorner(maxTerm,maxTerm) + ggs;
    hess *= objScale;
    hess += priorPrecision * Eigen::MatrixXd::Identity(maxTerm, maxTerm); //Diag.asDiagonal();
    hessSolver = hess.selfadjointView<Eigen::Lower>().ldlt();
    assert(hessSolver.info() == Eigen::Success);
  }
    

  // check for initial optimality
  if (grad.norm() < gtol) {
    return x;
  }
  // optimization loop
  int it;
  for (it = 0; it < maxIts; ++it) {
    
    if(useFullHess){
      Eigen::VectorXd ggScale = gx.array().pow(-2.0).matrix();
      auto ggs = gs.at(d)->topRows(newNumSamps).leftCols(maxTerm).transpose() * ggScale.asDiagonal() * gs.at(d)->topRows(newNumSamps).leftCols(maxTerm);
      hess = ffs.at(d)->topLeftCorner(maxTerm,maxTerm) + ggs;
      hess *= objScale;
      hess += priorPrecision * Eigen::MatrixXd::Identity(maxTerm, maxTerm); //Diag.asDiagonal();
      hessSolver = hess.selfadjointView<Eigen::Lower>().ldlt();
      assert(hessSolver.info() == Eigen::Success);
    }
    
    if(verbosity>0){
      std::cout << "Iteration " << it << std::endl;
      if(verbosity>1){
        std::cout << "  Objective = " << obj+logTerm  << " = " << logTerm << " + " << obj << std::endl;
        std::cout << "  Gradient norm = " << grad.norm() << std::endl;
      }
      if(verbosity>2){
       std::cout << "  Coefficients = " << x.transpose() << std::endl; 
      }
    }
    // compute the newton step
    Eigen::VectorXd step = -1.0 *stepScale *hessSolver.solve(grad);
    double tempProj = step.dot(grad);    
    if(tempProj>=0){
      step = -1.0*stepScale*grad;  
    }
    double stepNorm = step.norm();
    if(stepNorm>maxStepSize){
      step *= maxStepSize/stepNorm;
    }
    
    // perform a line search in the newton direction, checking for both sufficient descent
    double gamma = 1.0;
    bool   isMonotone, isDecrease;
    int    lineIt;
    double stepProj = step.dot(grad);
    for (lineIt = 0; lineIt < maxLineIts; ++lineIt) {
      Eigen::VectorXd newX;
      
	  newX = x + gamma * step;
      
       Eigen::VectorXd newGx = gs.at(d)->topRows(newNumSamps).leftCols(maxTerm) * newX;

        Eigen::VectorXd newDiff = (newX - initialCoeffs.at(d).head(maxTerm));
        
        double newLogTerm = newGx.array().log().sum();
        double minGx = newGx.minCoeff();
        double newObj;
        if(std::isnan(newLogTerm)||(minGx<monoTol)){
          newObj = std::numeric_limits<double>::infinity();  
        }else{
          double newRegTerm = 0.5 *priorPrecision *newDiff.squaredNorm();
          newObj = (0.5 * (fs.at(d)->topRows(newNumSamps).leftCols(maxTerm) * newX).squaredNorm() - newLogTerm) * objScale + newRegTerm;
        }
        isDecrease = (newObj < obj + c1 * stepProj + machEps);

        if (isDecrease) {
          belowXTOL = ((x - newX).norm() < xtol);
          if(verbosity>1){
            std::cout << "  Step size = " << gamma << " vs " << (x-newX).norm() << std::endl; 
          }
          x         = newX;
          gx        = newGx;
          diff      = newDiff;

          belowFTOL = abs(obj - newObj) < ftol;

          obj  = newObj;
          logTerm = newLogTerm;
          logGrad = gs.at(d)->topRows(newNumSamps).leftCols(maxTerm).transpose() * newGx.array().inverse().matrix();
          grad = (ffs.at(d)->topLeftCorner(maxTerm,maxTerm).selfadjointView<Eigen::Lower>() * newX - logGrad) * objScale + priorPrecision * newDiff;

          break;
        } else {
          gamma *= 0.5;
        }

      } // end of line search loop
      
	  if(lineIt>=maxLineIts){
		  std::cout << "FAILURE: Linesearch failed during optimization of dimension " << d << "\n";
		  std::cout << "Gradient = " << grad.transpose() << std::endl;
      std::cout << "Step     = " << step.transpose() << std::endl;
		  std::cout << "Regularization parameter = " << priorPrecision << std::endl;
      assert(lineIt<maxLineIts); // this assert needs to be here.  I don't want to return a value if the line search fails -- something bad likely happened and the map is likely garbage.
    }
    
    // check to see if gradient has converged
    belowGTOL = (grad.norm() < gtol);
    
    // check for convergence
    if ((belowGTOL) || (belowXTOL) || (belowFTOL)) {
      break;
    }
  } // main optimization loop

  return x;
}

Eigen::VectorXd MapUpdateManager::GetInducedDensities(int startInd, int segmentLength) const
{
  assert(startInd + segmentLength <= oldNumSamps);

  Eigen::MatrixXd refSamps(map->outputSize, segmentLength);
  Eigen::MatrixXd refDerivs(map->outputSize, segmentLength);

  for (int i = 0; i < map->outputSize; ++i) {
    refSamps.row(i)  = (fs.at(i)->block(startInd, 0, segmentLength, fs.at(i)->cols()) * map->coeffs.at(i)).transpose();
    refDerivs.row(i) = (gs.at(i)->block(startInd, 0, segmentLength, fs.at(i)->cols()) * map->coeffs.at(i)).transpose();
  }


  Eigen::VectorXd output = refDerivs.array().log().matrix().colwise().sum() - 0.5 *
                           refSamps.array().pow(2).matrix().colwise().sum() - 0.5 *map->outputSize *log(
    2.0 * boost::math::constants::pi<double>()) * Eigen::RowVectorXd::Ones(refDerivs.cols());

  return output.transpose();
}


Eigen::VectorXd MapUpdateManager::GetMarginalErrors(int numBins, double qLB, double qUB) const{
  
  // we need at least two quantiles unless qLB==qUB
  assert(numBins>0);
  assert((numBins>1)||(qLB==qUB));
  
  // get the quantile locations
  Eigen::VectorXd qLocs(numBins);
  qLocs.setLinSpaced(numBins,qLB,qUB);
  
  // now, compute the true gaussian quantiles
  Eigen::VectorXd gaussQuants(numBins);
  for(int i=0; i<numBins; ++i)
    gaussQuants(i) = sqrt(2.0)*boost::math::erf_inv(2.0*qLocs(i)-1.0);
  
  // loop over all the dimensions and compute the error
  Eigen::VectorXd output(map->outputSize);
  for(int d=0; d<map->outputSize; ++d){
    
    // get the map output
    Eigen::VectorXd rd = fs.at(d)->topRows(newNumSamps)*map->coeffs.at(d);
    
    // sort the reference samples so we can easily compute the quantiles
    std::sort(&rd(0),&rd(newNumSamps-1));
    
    Eigen::VectorXd sampQuants(numBins);
    for(int q=0; q<numBins; ++q){
      double dblInd = qLocs(q)*newNumSamps;
      if(floor(dblInd)!=dblInd){
        sampQuants(q) = 0.5*(rd(floor(dblInd)) + rd(ceil(dblInd)));
      }else{
        sampQuants(q) = rd(floor(dblInd)); 
      }
    }
    
    output(d) = ((sampQuants-gaussQuants).array()/gaussQuants.array()).abs().sum()/numBins;
    
  }
  
  return output;
}
