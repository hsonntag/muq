#include "MUQ/Inference/TransportMaps/BasisFactory.h"
#include "MUQ/Inference/TransportMaps/LinearBasis.h"
#include "MUQ/Inference/TransportMaps/Sigmoids.h"

#include <iostream>

#include <boost/math/special_functions/erf.hpp>

using namespace std;
using namespace muq::Inference;

std::vector<BasisSet> BasisFactory::CreateLinear(int dim, bool isDiagonal){
  
  vector<BasisSet> output(dim);
  for(int d=0; d<dim;++d)
  {
    // add a constant term and diagonal linear term
    output.at(d).push_back(make_shared<ConstantBasis>(d));
    output.at(d).push_back(make_shared<LinearBasis>(d));
    
    // add lower triangular diagonal terms if desired
    if(!isDiagonal){
      for(int dd=0; dd<d; ++dd)
        output.at(d).push_back(make_shared<LinearBasis>(dd));
    }
  }
  return output;
}



std::vector<BasisSet> BasisFactory::CreateDiagonal(Eigen::MatrixXd const&      samps,
                                                   boost::property_tree::ptree options)
{
  const int dim = samps.rows();
  const int numSamps = samps.cols();
  const int numQuants = min<int>(50,floor(static_cast<double>(numSamps)/2.0));
  vector<BasisSet> output(dim);
  
  const double quantGap = 1.0/static_cast<double>(numQuants+2);
  Eigen::VectorXd pVals(numQuants);
  Eigen::VectorXd gaussianQuantiles(numQuants);
  
  for(int i=0; i<numQuants; ++i){
    pVals(i) = (i+1)*quantGap;
    gaussianQuantiles(i) = sqrt(2)*boost::math::erf_inv(2.0*pVals(i)-1);
  }
  
  
  
  for(int d=0; d<dim; ++d){
    
    Eigen::VectorXd sampsRow = samps.row(d).transpose();
    vector<double> rbfCenter;
    vector<double> rbfRadius;
    
    // a vector of positions
    std::vector<double> newPos;
    
    double muProj = sampsRow.sum()/numSamps;
    double varProj = (sampsRow.array()-muProj).pow(2).sum()/(numSamps-1);
    
    // scale the projected samples to have mean zero and variance 1 -- this allows us to compare the quantiles to a standard normal
    sampsRow = ((sampsRow.array()-muProj)/sqrt(varProj)).matrix().eval();
  
    // sort the projected samples so we can easily compute the quantiles
    std::sort(&sampsRow[0],&sampsRow(numSamps-1));
    
    Eigen::VectorXd quantiles(numQuants);
    for(int i=0; i<numQuants; ++i){
      double fracInd = pVals(i)*static_cast<double>(numSamps);
      double w = fracInd-floor(fracInd);
      quantiles(i) = (1-w)*sampsRow(floor(fracInd)) + w*sampsRow(ceil(fracInd));
    }
    
    Eigen::VectorXd quantileError = (quantiles-gaussianQuantiles).array().matrix();
    // put a basis function anywhere the sign in the error changes
    for(int i=0; i<numQuants-1; ++i){
      if((quantileError(i)>0)^(quantileError(i+1)>0)){
        // put a center where we think the error passes through 0
        rbfCenter.push_back(quantiles(i)-1.0*quantileError(i)*(quantiles(i+1)-quantiles(i))/(quantileError(i+1)-quantileError(i)));
      }
    }
    
    // now, scale the rbfs based on the distance to the neighbor
    rbfRadius.resize(rbfCenter.size(),-1.0);
    assert(rbfRadius.size()>0);
    double scale = options.get("TransportMaps.RbfScale",0.5);
    rbfRadius.at(0) = scale*(rbfCenter.at(1)-rbfCenter.at(0));
    for(int i=1; i<rbfCenter.size()-1; ++i){
      rbfRadius.at(i) = scale*min(0.5*(rbfCenter.at(i+1)-rbfCenter.at(i)),0.5*(rbfCenter.at(i)-rbfCenter.at(i-1)));
    }
    if(rbfCenter.size()>1)
      rbfRadius.at(rbfCenter.size()-1) = scale*(rbfCenter.at(rbfCenter.size()-1)-rbfCenter.at(rbfCenter.size()-2));
    
    // constant and linear terms
    output.at(d).push_back(make_shared<ConstantBasis>(d));
    output.at(d).push_back(make_shared<LinearBasis>(d));
    
    // rbf bases
    string rbfType = options.get("TransportMaps.RbfType","Sigmoid1");
    if(!rbfType.compare("Sigmoid1")){
      for(int i=0; i<rbfCenter.size(); ++i)
        output.at(d).push_back(make_shared<Sigmoid1>(d,rbfCenter.at(i),rbfRadius.at(i)));
      
    }else if(!rbfType.compare("Sigmoid2")){
      for(int i=0; i<rbfCenter.size(); ++i)
        output.at(d).push_back(make_shared<Sigmoid2>(d,rbfCenter.at(i),rbfRadius.at(i)));
      
    }else if(!rbfType.compare("Sigmoid3")){
      for(int i=0; i<rbfCenter.size(); ++i)
        output.at(d).push_back(make_shared<Sigmoid3>(d,rbfCenter.at(i),rbfRadius.at(i)));
    }else{
      std::cerr << "In BasisFactory: Invalid value for TransportMaps.RbfType, received " << rbfType << std::endl;
      assert(false);
    }
  } // loop over dimension
  
  return output;
}
