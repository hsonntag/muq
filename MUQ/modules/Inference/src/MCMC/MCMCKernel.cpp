#include "MUQ/Inference/MCMC/MCMCKernel.h"
#include "MUQ/Utilities/LogConfig.h"

#include "MUQ/Utilities/HDF5Logging.h"

using namespace muq::Inference;
using namespace muq::Utilities;

MCMCKernel::MCMCKernel(std::shared_ptr<AbstractSamplingProblem> samplingProblem, boost::property_tree::ptree& properties) : samplingProblem(samplingProblem){
}

std::shared_ptr<MCMCKernel> MCMCKernel::Create(std::shared_ptr<AbstractSamplingProblem> samplingProblem,
                                                   boost::property_tree::ptree            & properties)
{
  //grab the list of constructors
  auto map = GetFactoryMap();
  
  //find out which kernel to make
  auto kernelString = ReadAndLogParameter<std::string>(properties, "MCMC.Kernel", "MHKernel");
  LOG(INFO) << "Trying to create kernel: " << kernelString;
  
  //Make sure the kernel actually exists.
  if(map->find(kernelString) == map->end())
  {
    std::cout << "ERROR: Could not find " << kernelString << " in the list of kernels." << std::endl;
    std::cout << "       Current kernels include:" << std::endl;
    for(auto iter = map->begin(); iter!=map->end(); ++iter)
      std::cout << "         " << iter->first << std::endl;
    assert(map->find(kernelString) != map->end());  
  }
  //lookup the one we want and construct it
  return map->at(kernelString) (samplingProblem, properties);
}
