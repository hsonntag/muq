
#include "MUQ/Inference/MCMC/AMALA.h"

// standard library includes
#include <string>

#include <boost/property_tree/ptree.hpp>

// other muq related includes
#include "MUQ/Utilities/LogConfig.h"

#include "MUQ/Utilities/HDF5Wrapper.h"


// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;


REGISTER_MCMCPROPOSAL(AMALA);

/** This constructor defins the Langevin diffusion MCMC by reading necessary parameters in
 *  paramList
 */
AMALA::AMALA(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
                 boost::property_tree::ptree                            & properties) : MCMCProposal(inferenceProblem,
                                                                                                     properties)
{
  LOG(INFO) << "Constructing Adaptive MALA MCMC";

  // also load AM parameters
  adaptSteps = properties.get("MCMC.AMALA.AdaptSteps", 1);
  LOG(INFO) << "MCMC.AMALA.AdaptSteps = " << adaptSteps;

  adaptStart = properties.get("MCMC.AMALA.AdaptStart", 1);
  LOG(INFO) << "MCMC.AMALA.AdaptStart = " << adaptStart;

  // adaptive metropolis update scaling -- taken from Haario DRAM paper
  // adaptScale = properties.get("MCMC.AMALA.AdaptScale", 1.0);
 //  LOG(INFO) << "MCMC.AMALA.AdaptScale = " << adaptScale;
    
  maxDrift = properties.get("MCMC.AMALA.MaxDrift", std::numeric_limits<double>::infinity());
  LOG(INFO) << "MCMC.AMALA.MaxDrift = " << maxDrift;

  eps1 = properties.get("MCMC.AMALA.MinStd", 1e-5);
  LOG(INFO) << "MCMC.AMALA.MinStd = " << eps1;
  
  A1 = properties.get("MCMC.AMALA.MaxStd", 1e5);
  LOG(INFO) << "MCMC.AMALA.MaxStd = " << A1;
  
  targetAcceptance = properties.get("MCMC.AMALA.AcceptanceTarget", 0.576);
  LOG(INFO) << "MCMC.AMALA.MaxAcceptanceTarget = " << targetAcceptance;
  
  startVar = properties.get("MCMC.AMALA.ProposalVar", 1.0);
  stepSize = sqrt(startVar);
  LOG(INFO) << "MCMC.AMALA.StartSize = " << startVar;
  
  // tell the problem what information we are going to need from it at each step -> Just the gradient
  samplingProblem->SetStateComputations(true, true, false);

  const int dim = samplingProblem->samplingDim;
  
  // now, set the chain mean and covariance dimension
  sampleCov  = Eigen::MatrixXd::Zero(dim, dim);
  sampleMean = Eigen::VectorXd::Zero(dim);
  
  proposal = make_shared<GaussianPair>(Eigen::VectorXd::Zero(dim), pow(stepSize,2.0));

}

std::shared_ptr<muq::Inference::MCMCState> AMALA::DrawProposal(
  std::shared_ptr<muq::Inference::MCMCState>    currentState,
  std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry,
  int const currIteration)
{
	
  Eigen::VectorXd drift = 0.5 * proposal->specification->ApplyCovariance(currentState->GetDensityGrad());
  double driftNorm = drift.norm();
  if(driftNorm>maxDrift)
    drift *= maxDrift/driftNorm;
  
  Eigen::VectorXd propMu = currentState->state + drift;
 

  Eigen::VectorXd proposedVector = propMu + proposal->rv->Sample();

  return samplingProblem->ConstructState(proposedVector, currIteration, logEntry);
}

double AMALA::ProposalDensity(std::shared_ptr<muq::Inference::MCMCState> currentState,
                                std::shared_ptr<muq::Inference::MCMCState> proposedState)
{
    Eigen::VectorXd drift = 0.5 * proposal->specification->ApplyCovariance(currentState->GetDensityGrad());
    double driftNorm = drift.norm();
    if(driftNorm>maxDrift)
      drift *= maxDrift/driftNorm;
	  
  Eigen::VectorXd propMu = currentState->state + drift;
  
  double forwardDens = proposal->density->LogDensity(propMu - proposedState->state);

  return forwardDens;
}

void AMALA::updateSampleMeanAndCov(int const currIteration, std::shared_ptr<muq::Inference::MCMCState> const currentState)
{
  double t = currIteration - 1.0; //this is the number of existing

  // samples

  if (currIteration == 1) {                                                          //first estimate of mean
    sampleMean = currentState->state;
  } else if (currIteration == 2) {                                                   //first cov estimate
    auto oldMean = sampleMean;

    sampleMean = t / (1.0 + t) * sampleMean + 1.0 / (1.0 + t) * currentState->state; //update mean

    //compute covariance from scratch, from the definition
    sampleCov.selfadjointView<Eigen::Lower>().rankUpdate(oldMean - sampleMean, 1.0);
    sampleCov.selfadjointView<Eigen::Lower>().rankUpdate(currentState->state - sampleMean, 1.0);
  } else {                                                                           //now recursively update both
    sampleCov *= (t - 1.0) / t;
    auto oldmean = sampleMean;
    sampleMean = t / (1.0 + t) * sampleMean + 1.0 / (1.0 + t) * currentState->state; //update mean

    //note that the asymmetric form fixes the fact that the old mean was wrong
    sampleCov += 1.0 / t * (currentState->state - oldmean) * (currentState->state - sampleMean).transpose();

    //taken from en.wikipedia.org/wiki/Algorithms_for_calculating_variance but their N is off from ours
  }
  
  // project the mean and covariance back to feasible values
  double muNorm = sampleMean.norm();
  double covNorm = sampleCov.norm();
  if(muNorm>A1)
	  sampleMean *= A1/muNorm;
  if(covNorm>A1)
	  sampleCov *= A1/covNorm;
  
}

/** Updates the proposal covariance based on current point */
void AMALA::updateProp()
{
  Eigen::MatrixXd adjustedCov = pow(stepSize,2.0) * sampleCov + eps2 * Eigen::MatrixXd::Identity(sampleCov.rows(), sampleCov.cols());

  // update the proposal with this scaled covariance
  proposal = make_shared<GaussianPair>(Eigen::VectorXd::Zero(sampleCov.rows()), adjustedCov);
}


void AMALA::PostProposalProcessing(std::shared_ptr<muq::Inference::MCMCState> const currentState,
                                MCMCProposal::ProposalStatus const               proposalAccepted,
                                int const                                        currIteration,
                                std::shared_ptr<muq::Utilities::HDF5LogEntry>    logEntry)
{
  if(currIteration>=adaptStart){
  if (proposalAccepted == MCMCProposal::ProposalStatus::notRun) {
    return; //not run, we didn't learn anything
  } else if (proposalAccepted == MCMCProposal::ProposalStatus::accepted) {
    ++numProposalsAccepted;
  }
  ++numProposals;
  }
  // Update the mean and covariance
  updateSampleMeanAndCov(currIteration, currentState); // update the running proposal covariance

  if(currIteration>adaptStart){
    // update the scale based on the acceptance rate
    double acceptance = static_cast<double>(numProposalsAccepted) / static_cast<double>(numProposals);
    double gamma = 1.0/static_cast<double>(currIteration-adaptStart);
    stepSize += gamma*(acceptance-targetAcceptance);
    if(stepSize<eps1){
      stepSize = eps1;
    }else if(stepSize>A1){
      stepSize = A1;
    }
  }
  
  //but only actually use the new one at certain intervals
  if (((currIteration % adaptSteps) == 0) && (currIteration > adaptStart))
	updateProp();
  
  logEntry->loggedData["AMALAProposalDiag"] = proposal->specification->GetCovarianceMatrix().diagonal();
  logEntry->loggedData["AMALAStepSize"] = Eigen::VectorXd::Constant(1,stepSize);
}

void AMALA::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  HDF5Wrapper::WriteStringAttribute(groupName, prefix + "Proposal", "AMALA");
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "AMALA - Step size",   stepSize);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "AMALA - Max drift",   maxDrift);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "AMALA - Adapt Start", adaptStart);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "AMALA - Adapt Gap",   adaptSteps);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "AMALA - Start Size",  startVar);
}

