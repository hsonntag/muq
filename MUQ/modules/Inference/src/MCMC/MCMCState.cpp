
#include "MUQ/Inference/MCMC/SingleChainMCMC.h"

#include <assert.h>
#include <iostream>
#include <cmath>

#include <boost/property_tree/ptree.hpp>

#include "MUQ/Utilities//LogConfig.h"
#include <MUQ/Modelling/EmpiricalRandVar.h>

// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;

MCMCState::MCMCState(Eigen::VectorXd const          & state,
                     double                           logLikelihood,
                     boost::optional<Eigen::VectorXd> likelihoodGrad,
                     boost::optional<double>          logPrior,
                     boost::optional<Eigen::VectorXd> priorGrad,
                     boost::optional<Eigen::MatrixXd> metric,
                     boost::optional<Eigen::VectorXd> forwardModel,
                     double                           temperature) : state(state), logLikelihood(logLikelihood),
                                                                     likelihoodGrad(
                                                                       likelihoodGrad), logPrior(logPrior),
                                                                     priorGrad(priorGrad), metric(metric), forwardModel(
                                                                       forwardModel), temperature(
                                                                       temperature)
{}

double MCMCState::GetDensity() const
{
  if (logPrior) {
    return *logPrior + GetLikelihood();
  } else {
    return GetLikelihood();
  }
}

double MCMCState::GetLikelihood() const
{
  return logLikelihood * temperature;
}

double MCMCState::GetLikelihoodOtherTemp(double const& otherTemp) const
{
  return logLikelihood * otherTemp;
}

Eigen::VectorXd MCMCState::GetDensityGrad() const
{
  assert(likelihoodGrad);

  if (priorGrad) {
    return *priorGrad + GetLikelihoodGrad();
  } else {
    return GetLikelihoodGrad();
  }
}

Eigen::VectorXd MCMCState::GetLikelihoodGrad() const
{
  assert(likelihoodGrad);
  return *likelihoodGrad * temperature;
}

Eigen::VectorXd MCMCState::GetForwardModel() const
{
  assert(forwardModel);
  return *forwardModel;
}

bool MCMCState::HasForwardModel() const
{
	return static_cast<bool>(forwardModel);
}


Eigen::MatrixXd MCMCState::GetMetric() const
{
  assert(metric);
  if (temperature > 1e-14) {
    return (*metric) * temperature;
  } else {
    //else the temperature is zero, so just use the identity metric
    return Eigen::MatrixXd::Identity(state.rows(), state.rows());
  }
}

void MCMCState::SetTemperature(double const temp)
{
  temperature = temp;
}

void MCMCState::CloneFrom(std::shared_ptr<MCMCState> const other)
{
  logLikelihood  = other->logLikelihood;
  likelihoodGrad = other->likelihoodGrad;
  logLikelihood  = other->logLikelihood;
  priorGrad      = other->priorGrad;
  metric         = other->metric;
  temperature    = other->temperature;
  forwardModel   = other->forwardModel;
}

double MCMCState::GetTemperature()
{
  return temperature;
}