
#include "MUQ/Inference/MCMC/StanNUTS.h"

#include <Eigen/Dense>

#include <boost/property_tree/ptree.hpp>


#include "stan/mcmc/nuts.hpp"

#include "MUQ/Utilities/EigenUtils.h"
#include "MUQ/Utilities/VectorTranslater.h"
#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Inference/ProblemClasses/StanModelInterface.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"

#include "MUQ/Utilities/HDF5Wrapper.h"


using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Inference;

REGISTER_MCMCKERNEL(StanNUTS);

StanNUTS::StanNUTS(std::shared_ptr<muq::Inference::AbstractSamplingProblem> samplingProblem,
                   boost::property_tree::ptree                            & properties) : MCMCKernel(samplingProblem,
                                                                                                     properties)
{
  LOG(INFO) << "Constructing Stan No U-Turn Sampler MCMC";
  LOG(INFO) << "(has no parameters)";

  samplingProblem->SetStateComputations(true, true, false);
}

void StanNUTS::PrintStatus() const
{
  std::cout << "  NUTS: Acceptance rate = " << std::setw(5) << std::fixed << std::setprecision(1) << 100.0 *
    GetAccept() << "%\n";
}

void StanNUTS::SetupStartState(Eigen::VectorXd const& xStart)
{
  modelInterface = std::make_shared<StanModelInterface>(samplingProblem);


  vector<double> startVec;
  vector<int>    startInt;
  VectorTranslate(xStart, startVec, xStart.rows());

  nutsSampler = std::make_shared < stan::mcmc::nuts < boost::mt19937 >> (*modelInterface, startVec, startInt);
}

std::shared_ptr<muq::Inference::MCMCState> StanNUTS::ConstructNextState(
  std::shared_ptr<muq::Inference::MCMCState>    currentState,
  int const                                     iteration,
  std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry)
{
  //call the sampler
  auto newSample = nutsSampler->next();

  totalSteps++;

  //convert to our data taypes
  vector<double> newSampleVec;
  newSample.params_r(newSampleVec);
  Eigen::VectorXd newSampleEig;

  VectorTranslate(newSampleVec, newSampleEig, newSampleVec.size());

  //figure out whether the state is the same, i.e., whether the move was accepted
  bool accepted = !MatrixApproxEqual(newSampleEig, currentState->state, 1e-14);

  if (accepted) {
    numAccepts++;
  }

  auto proposedState = samplingProblem->ConstructState(newSampleEig, iteration, logEntry);
  return proposedState;
}

void StanNUTS::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  HDF5Wrapper::WriteStringAttribute(groupName, prefix + "Kernel", "Stan NUTS");
}

double StanNUTS::GetAccept() const
{
  return static_cast<double>(numAccepts) / static_cast<double>(totalSteps);
}
