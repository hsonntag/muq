
#include "MUQ/Inference/ProblemClasses/StanModelInterface.h"

#include <Eigen/Core>

#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Utilities/VectorTranslater.h"
#include "MUQ/Inference/MCMC/SingleChainMCMC.h"

using namespace muq::Utilities;
using namespace muq::Inference;


StanModelInterface::StanModelInterface(std::shared_ptr<muq::Inference::AbstractSamplingProblem> samplingProblem) : stan
                                                                                                                   ::
                                                                                                                   model
                                                                                                                   ::
prob_grad(samplingProblem->samplingDim),
                                                                                                                   samplingProblem(
                                                                                                                     samplingProblem)
{}

double StanModelInterface::grad_log_prob(std::vector<double>& params_r,
                                         std::vector<int>   & params_i,
                                         std::vector<double>& gradient,
                                         std::ostream        *output_stream)
{
  Eigen::VectorXd eigenParams;

    VectorTranslate(params_r, eigenParams, num_params_r());

  //try to construct the state we need
    auto mcmcState = samplingProblem->ConstructState(eigenParams, 0, nullptr);
  if (mcmcState) {
    //it worked, so put the gradient in the passed-by-reference ouptput
    VectorTranslate(mcmcState->GetDensityGrad(), gradient, num_params_r());
    //and return the density
    return mcmcState->GetDensity();
  } else {
    //failed, meaning the model didn't work, so return infinity density and zero grad,
    //which is the same as how they handle model failure.
    VectorTranslate(Eigen::VectorXd::Zero(num_params_r()), gradient, num_params_r());
    return -std::numeric_limits<double>::infinity();
  }
}

double StanModelInterface::log_prob(std::vector<double>& params_r,
                                    std::vector<int>   & params_i,
                                    std::ostream        *output_stream)
{
  Eigen::VectorXd eigenParams = Eigen::VectorXd::Zero(num_params_r());

    VectorTranslate(params_r, eigenParams, num_params_r());
    auto mcmcState = samplingProblem->ConstructState(eigenParams, 0, nullptr);

  //return the density if the state is valid, as above.
  if (mcmcState) {
    return mcmcState->GetDensity();
  } else {
    return -std::numeric_limits<double>::infinity();
  }
}

