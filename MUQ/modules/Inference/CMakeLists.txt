add_subdirectory(src)

if(Inference_tests)
#pass the test sources to the higher level
add_subdirectory(test)
set(all_gtest_sources ${all_gtest_sources} ${Test_Inference_Sources} PARENT_SCOPE)
set(all_long_gtest_sources ${all_long_gtest_sources} ${LongTest_Inference_Sources} PARENT_SCOPE)
endif(Inference_tests)


