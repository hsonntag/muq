#include "MUQ/Inference/python/LinearBasisPython.h"

using namespace std;
using namespace muq::Inference;

ConstantBasisPython::ConstantBasisPython(int inputSize) : ConstantBasis(inputSize) {}

LinearBasisPython::LinearBasisPython(int inputSizes, int linearDim) : LinearBasis(inputSizes, linearDim) {}

void muq::Inference::ExportConstantBasis()
{
  boost::python::class_<ConstantBasisPython, shared_ptr<ConstantBasisPython>,
                        boost::python::bases<TransportMapBasis>, boost::noncopyable> exportConstBasis(
    "ConstantBasis", boost::python::init<int>());

  boost::python::implicitly_convertible<shared_ptr<ConstantBasisPython>,
                                        shared_ptr<ConstantBasis> >();

  boost::python::implicitly_convertible<shared_ptr<ConstantBasisPython>,
                                        shared_ptr<TransportMapBasis> >();
}

void muq::Inference::ExportLinearBasis()
{
  boost::python::class_<LinearBasisPython, shared_ptr<LinearBasisPython>,
                        boost::python::bases<TransportMapBasis>, boost::noncopyable> exportLinBasis(
    "LinearBasis", boost::python::init<int, int>());

  boost::python::implicitly_convertible<shared_ptr<LinearBasisPython>,
                                        shared_ptr<LinearBasis> >();

  boost::python::implicitly_convertible<shared_ptr<LinearBasisPython>,
                                        shared_ptr<TransportMapBasis> >();
}

