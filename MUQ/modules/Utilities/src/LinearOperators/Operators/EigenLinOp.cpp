
#include "MUQ/Utilities/LinearOperators/Operators/EigenLinOp.h"

using namespace muq::Utilities;

Eigen::VectorXd EigenLinOp::apply(const Eigen::VectorXd& x)
{
  return A * x;
}

