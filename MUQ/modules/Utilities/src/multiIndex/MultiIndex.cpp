#include "MUQ/Utilities/multiIndex/MultiIndex.h"
#include <boost/graph/graph_concepts.hpp>

using namespace std;
using namespace muq::Utilities;


MultiIndex::MultiIndex(int dimensionIn) : dimension(dimensionIn), maxIndex(0), totalOrder(0){};

MultiIndex::MultiIndex(Eigen::RowVectorXu const& indIn) : dimension(indIn.cols())
{
  totalOrder = 0;
  for(unsigned int i=0; i<indIn.size(); ++i)
  {
    if(indIn(i)>0){
      nzInds.push_back(make_pair(i,indIn(i)));
      totalOrder += indIn(i);
    }
  }
  
  if(nzInds.size()>0){
    maxIndex = max_element(nzInds.begin(), nzInds.end(), [] (pair<unsigned,unsigned> a, pair<unsigned,unsigned> b){return a.second < b.second;})->second;
  }else{
    maxIndex = 0;
  }
  
  
}

MultiIndex::MultiIndex(MultiIndex const& indIn) : dimension(indIn.dimension),
                                                  nzInds(indIn.nzInds),
                                                  maxIndex(indIn.maxIndex),
                                                  totalOrder(indIn.totalOrder)
{}

Eigen::RowVectorXu MultiIndex::GetMulti() const{
  
  Eigen::RowVectorXu output = Eigen::RowVectorXu::Zero(dimension);
  for(auto p : nzInds)
    output(p.first) = p.second;
  return output;
}

bool MultiIndex::SetValue(int dim, int val)
{
  assert(dim<dimension);
  
  if(val==0)
  {
    return false;
    
  }else{
    
    // check to see if this dimension is already set
    bool dimFound = false;
    for(int i=0; i<nzInds.size(); ++i){
      
      if(nzInds[i].first==dim){
        
        unsigned int oldVal = nzInds[i].second;
        nzInds[i].second = val;
        if(oldVal==maxIndex){
          maxIndex = max_element(nzInds.begin(), nzInds.end(), [] (pair<unsigned,unsigned> a, pair<unsigned,unsigned> b){return a.second < b.second;})->second;
        }else if(val>maxIndex){
          maxIndex = val;
        }
        
        dimFound = true;
        break;
      }
    }
    
    // if dimension is not already set, add the value
    if(!dimFound){
      nzInds.push_back(make_pair(dim,val));
    
      if(val>maxIndex)
        maxIndex = val;
    }
    return dimFound;
  }
}

int MultiIndex::GetValue(int dim) const
{
  assert(dim<dimension);
  
  for(int i=0; i<nzInds.size(); ++i){
    if(nzInds[i].first==dim)
      return nzInds[i].second;
  }
  
  return 0;
}

void MultiIndex::SetDimension(int newDim){
  
  // first, make sure the new dimension doesn't interfere with any values
  for(auto pair : nzInds)
    assert(pair.first<newDim);
  
  // update the stored dimension
  dimension = newDim;
}


bool MultiIndex::operator==(const MultiIndex &b){
  if(nzInds.size()==b.nzInds.size())
  {
    if((maxIndex==b.maxIndex)&&(totalOrder==b.totalOrder)){
      
      Eigen::RowVectorXu vec = GetMulti();
      Eigen::RowVectorXu vecb = b.GetMulti();
      for(int i=0; i<min(vec.size(),vecb.size()); ++i){
        if(vec[i]!=vecb[i]){
          return false;
        }
      }
      return true;
      
    }else{
      return false;
    }
    
  }else{
    return false;
  }
}

bool MultiIndex::operator!=(const MultiIndex &b){
  if(nzInds.size()!=b.nzInds.size())
  {
    if((maxIndex==b.maxIndex)&&(totalOrder==b.totalOrder)){
      
      Eigen::RowVectorXu vec = GetMulti();
      Eigen::RowVectorXu vecb = b.GetMulti();
      for(int i=0; i<min(vec.size(),vecb.size()); ++i){
        if(vec[i]!=vecb[i]){
          return true;
        }
      }
      return false;
    }else{
      return true;
    }
    
  }else{
    return true;
  }
}

bool MultiIndex::operator<(const MultiIndex &b)
{
  if((nzInds.size()==0)&&(b.nzInds.size()>0)){
    return true;
  }else if((b.nzInds.size()==0)&&(nzInds.size()>0)){
    return false;
  }else if((nzInds.size()==0)&&(b.nzInds.size()==0)){
    return false;
  }else if(totalOrder<b.totalOrder){
    return true;
  }else if(totalOrder>b.totalOrder){
      return false;
  }else if(maxIndex<b.maxIndex){
    return true; 
  }else if(maxIndex>b.maxIndex){
    return false; 
  }else{
    
    Eigen::RowVectorXu vec = GetMulti();
    Eigen::RowVectorXu vecb = b.GetMulti();
    for(int i=0; i<min(vec.size(),vecb.size()); ++i){
      if(vec[i]<vecb[i]){
        return true;
      }else if(vec[i]>vecb[i]){
        return false;
      }
    }
   
    // it should never get to this point unless the multiindices are equal
    return false;
  }
  
}
