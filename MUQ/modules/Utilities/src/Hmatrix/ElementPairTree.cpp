
#include "MUQ/Utilities/Hmatrix/ElementPairTree.h"
#include "MUQ/Utilities/Hmatrix/ElementTree.h"
#include "MUQ/Utilities/Hmatrix/ElementPairNode.h"
#include "MUQ/Geostatistics/IsotropicCovKernel.h"

#include <Eigen/Eigenvalues>

using namespace muq::Utilities;


template<unsigned int dim>
ElementPairTree<dim>::ElementPairTree(double eta, std::shared_ptr < Mesh < dim >>& MeshIn)
{
  // first build the cluster tree
  ClusterTree = std::shared_ptr < ElementTree < dim >> (new ElementTree<dim>(MeshIn));

  // now recursively fill in the block cluster tree
  root = std::shared_ptr < ElementPairNode < dim >>
         (new ElementPairNode<dim>(eta, ClusterTree, ClusterTree->root, ClusterTree->root));
}

template class muq::Utilities::ElementPairTree<1>;
template class muq::Utilities::ElementPairTree<2>;
template class muq::Utilities::ElementPairTree<3>;
