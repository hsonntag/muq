#include <boost/math/constants/constants.hpp>

#include "gtest/gtest.h"

#include "MUQ/Utilities/EigenUtils.h"

#include "MUQ/Utilities/Quadrature/GaussChebyshevQuadrature1D.h"

using namespace Eigen;
using namespace muq::Utilities;

TEST(UtilitiesQuadrature1DTest, ChebyshevTest)
{
  //store of right answers - analytic
  RowVectorXd nodesStored3(3);

  nodesStored3 << -0.5 * sqrt(3.0), 0, 0.5 * sqrt(3.0);
  RowVectorXd weightsStored3 = RowVectorXd::Ones(3) * boost::math::constants::pi<double>() / 3.0;

  RowVectorXd nodesStored5(5);
  nodesStored5 << -0.5 *
        sqrt(0.5 *
         (5 +
        sqrt(5))), -0.5 * sqrt(0.5 * (5 - sqrt(5))), 1e-20, 0.5 * sqrt(0.5 * (5 - sqrt(5))), 0.5 * sqrt(0.5 * (5 + sqrt(
                                                                                                                 5)));
  RowVectorXd weightsStored5 = RowVectorXd::Ones(5) * boost::math::constants::pi<double>() / 5.0;

  //compute and test
  GaussChebyshevQuadrature1D ChebyshevQuad;

  int size                             = 3;
  std::shared_ptr<RowVectorXd> nodes   = ChebyshevQuad.GetNodes(size);
  std::shared_ptr<RowVectorXd> weights = ChebyshevQuad.GetWeights(size);

  EXPECT_TRUE(MatrixApproxEqual(*nodes, nodesStored3, 1e-15));
  EXPECT_TRUE(MatrixApproxEqual(*weights, weightsStored3, 1e-15));

  size    = 5;
  nodes   = ChebyshevQuad.GetNodes(size);
  weights = ChebyshevQuad.GetWeights(size);

  EXPECT_TRUE(MatrixApproxEqual(*nodes, nodesStored5, 1e-15));
  EXPECT_TRUE(MatrixApproxEqual(*weights, weightsStored5, 1e-15));
}
