import sys
import unittest
import math

from libmuqUtilities import *

class PolynomialTest(unittest.TestCase):
  def testHermiteEval(self):
    poly = HermitePolynomials1DRecursive()

    trueVal = 1.0
    testVal = poly.evaluate(0,0.4)
    self.assertAlmostEqual(testVal,trueVal,8)

    trueVal = 6219.5581337600015
    testVal = poly.evaluate(8, 1.6)
    self.assertAlmostEqual(testVal,trueVal,8)
  
  def testProbHermiteEval(self):
    tol = 1e-10
    
    poly = ProbabilistHermite()
    
    trueVal = 1.0
    testVal = poly.evaluate(0,0.4)
    self.assertAlmostEqual(testVal,trueVal,8)
    
    trueVal = -2.075637503999991e+01
    testVal = poly.evaluate(8, 1.6)
    self.assertAlmostEqual(testVal,trueVal,8)

  def testHermiteRoots(self):
    poly = [8.147236863931789e-01, 9.057919370756192e-01, 1.269868162935061e-01, 9.133758561390194e-01,     6.323592462254095e-01]
    roots = HermitePolynomials1DRecursive.Roots(poly,1e-8)
    self.assertAlmostEqual(roots[0],-1.904980078887119e+00,3)
    self.assertAlmostEqual(roots[1],-6.949715242251162e-01,3)
    self.assertAlmostEqual(roots[2],4.162658411042948e-01,3)
    self.assertAlmostEqual(roots[3],1.461488797552201e+00,3)
  
  def testProbHermiteRoots(self):
    poly = [8.147236863931789e-01, 9.057919370756192e-01, 1.269868162935061e-01, 9.133758561390194e-01,     6.323592462254095e-01]
    roots = ProbabilistHermite.Roots(poly,1e-8)
    self.assertAlmostEqual(roots[0],-2.924713468970414e+00,3)
    self.assertAlmostEqual(roots[1],-1.079711083256760e+00,3)
    self.assertAlmostEqual(roots[2],6.934823585835934e-01,3)
    self.assertAlmostEqual(roots[3],1.866548264732107e+00,3)
  
  def testLegendreEval(self):
    poly = LegendrePolynomials1DRecursive()
    
    trueVal = 0.3375579333496094
    testVal = poly.evaluate(5,0.325)
    self.assertAlmostEqual(testVal,trueVal,8)    
    
    trueVal = -.001892916076323403
    testVal = poly.evaluate(200, -0.3598)
    self.assertAlmostEqual(testVal,trueVal,8)

  def testLegendreRoots(self):
    poly = [9.754040499940952e-02,     2.784982188670484e-01,     5.468815192049838e-01,     9.575068354342976e-01,     9.648885351992765e-01]
    roots = LegendrePolynomials1DRecursive.Roots(poly,1e-8)
    self.assertAlmostEqual(roots[0],-9.051904659059379e-01,3)
    self.assertAlmostEqual(roots[1],-5.226204925205424e-01,3)
    self.assertAlmostEqual(roots[2],1.268658154013623e-01,3)
    self.assertAlmostEqual(roots[3],7.338881792978924e-01,3)
  
  def testMonomialEval(self):
    poly = Monomial()
    x = 0.25
    maxOrder = 11
    for p in range(maxOrder):
      trueVal = math.pow(x,p)
      testVal = poly.evaluate(p,x)
      self.assertAlmostEqual(testVal,trueVal,8)

  def testMonomialRoots(self):
    poly = [0.0656667, 1.00199, -0.0010416, -0.000209811]
    roots = Monomial.Roots(poly,1e-8)
    self.assertAlmostEqual(roots[0],-71.60147450590436,3)
    self.assertAlmostEqual(roots[1],-0.06553187753397455,3)
    self.assertAlmostEqual(roots[2],66.70253836221926,3)
