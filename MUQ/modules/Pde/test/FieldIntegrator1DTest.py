import unittest

import libmuqModelling
import libmuqUtilities
import libmuqPde

class IntegrateThisField(libmuqPde.MeshParameter):
    def __init__(self, system, param):
        super(IntegrateThisField, self).__init__(system, param)

    def ParameterEvaluate(self, x, y, z, var):
         return 3.0 * x * x + 2.0 * x - 1.0

class FieldIntegrator1DTest(unittest.TestCase):
    def testBasicTest(self):
        param = dict()

        # for the mesh
        param["mesh.type"] = "LineMesh"
        param["mesh.N"]    = 50
        param["mesh.L"]    = 1.0

        # for the PDE
        param["PDE.Unknowns"]                = "u"
        param["PDE.UnknownOrders"]           = "2"
        param["PDE.GaussianQuadratureOrder"] = 4
        param["PDE.Name"]                    = "FieldIntegrator1D"
        param["PDE.Parameters"]              = "IntegrateThisField"
        param["PDE.IsScalarParameter"]       = "false"
        param["PDE.NeedSpatialGradient"]     = "IntegrateThisField"

        system = libmuqPde.GenericEquationSystems(param)

        # create the field that will be integrated 
        param["MeshParameter.SystemName"]     = "IntegrateThisField"
        param["MeshParameter.VariableNames"]  = "field"
        param["MeshParameter.VariableOrders"] = "2"

        integrateThisFiledMod  = IntegrateThisField(system, param)

        inputSizes = [integrateThisFiledMod.outputSize]

        pnts = [[0.25], [0.5], [0.75], [1.0]]

        pde = libmuqPde.NontransientPDEModPiece(inputSizes, pnts, system, param)

        result = pde.Evaluate([[1.0]*pde.inputSizes[0], integrateThisFiledMod.Evaluate()])

        for i in range(len(pnts)):
            x = pow(pnts[i][0], 3.0) + pow(pnts[i][0], 2.0) - pnts[i][0]
            self.assertAlmostEqual(x, result[i], 3)

