#include "MUQ/Pde/FieldIntegrator1D.h"

using namespace std;
using namespace muq::Pde;

REGISTER_PDE(FieldIntegrator1D)

FieldIntegrator1D::FieldIntegrator1D(Eigen::VectorXi const& inputSizes, shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree const& para) :
  Nontransient(inputSizes, system, para) 
{
    assert(inputNames.size() == 1);
    assert(GetEquationSystemsPtr()->get_mesh().mesh_dimension() == 1);
    assert(inputNames[0].first!=vectorParameter);
    
  if( inputNames[0].first==scalarParameter ) {
    fieldVariableName = inputNames[0].second;
  } else {
    assert(GetEquationSystemsPtr()->get_system(inputNames[0].second).n_vars() == 1);
    fieldVariableName = GetEquationSystemsPtr()->get_system(inputNames[0].second).variable_name(0);
  }

  assert(needGradient.size()==1 && needGradient[0].compare(fieldVariableName)!=0);
}

bool FieldIntegrator1D::IsDirichlet(libMesh::Point const& point) const
{
  if (point(0) == 0.0) { // right side is dirichlet
    return true;
  }
  return false;
}

