
#include "MUQ/Modelling/python/LinearModelPython.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;

LinearModelPython::LinearModelPython(boost::python::list const& Ain) : LinearModel(GetEigenMatrix(Ain)) {}

LinearModelPython::LinearModelPython(boost::python::list const& bin,
                                     bool                       useId) : LinearModel(GetEigenVector<Eigen::VectorXd>(
                                                                                       bin),
                                                                                     useId)
{}

LinearModelPython::LinearModelPython(boost::python::list const& bin,
                                     boost::python::list const& Ain) : LinearModel(GetEigenVector<Eigen::VectorXd>(
                                                                                     bin),
                                                                                   GetEigenMatrix(
                                                                                     Ain))
{}

void muq::Modelling::ExportLinearModel()
{
  // expose to python
  boost::python::class_<LinearModelPython, shared_ptr<LinearModelPython>,
                        boost::python::bases<OneInputJacobianModPiece, ModPiece>, boost::noncopyable> exportLinearModel(
    "LinearModel",
    boost::python::init<boost::python::list>());

  // expose constructors
  exportLinearModel.def(boost::python::init<boost::python::list, bool>());
  exportLinearModel.def(boost::python::init<boost::python::list, boost::python::list>());

  // allow implicit conversion
  boost::python::implicitly_convertible<shared_ptr<LinearModelPython>, shared_ptr<LinearModel> >();
  boost::python::implicitly_convertible<shared_ptr<LinearModelPython>,
                                        shared_ptr<OneInputJacobianModPiece> >();
  boost::python::implicitly_convertible<shared_ptr<LinearModelPython>, shared_ptr<ModPiece> >();
}

