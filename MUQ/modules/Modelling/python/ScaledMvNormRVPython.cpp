#include "MUQ/Modelling/python/ScaledMvNormRVPython.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;

ScaledMvNormRVPython::ScaledMvNormRVPython(std::shared_ptr<ConstantMVNormSpecification> const& specification) :
  ScaledMvNormRV(
    specification)
{}

ScaledMvNormRVPython::ScaledMvNormRVPython(int dimIn) : ScaledMvNormRV(dimIn) {}

ScaledMvNormRVPython::ScaledMvNormRVPython(boost::python::list const& meanIn,
                                           double                     scalarCovariance) : ScaledMvNormRV(GetEigenVector
                                                                                                         <Eigen::
                                                                                                          VectorXd>(
                                                                                                           meanIn),
                                                                                                         scalarCovariance)
{}


ScaledMvNormRVPython::ScaledMvNormRVPython(Eigen::VectorXd const                        & meanIn,
                                           Eigen::VectorXd const                        & covDiag,
                                           ConstantMVNormSpecification::SpecificationMode mode) : ScaledMvNormRV(meanIn,
                                                                                                                 covDiag,
                                                                                                                 mode)
{}

ScaledMvNormRVPython::ScaledMvNormRVPython(Eigen::VectorXd const                        & meanIn,
                                           Eigen::MatrixXd const                        & cov,
                                           ConstantMVNormSpecification::SpecificationMode mode) : ScaledMvNormRV(meanIn,
                                                                                                                 cov,
                                                                                                                 mode)
{}

shared_ptr<ScaledMvNormRVPython> ScaledMvNormRVPython::Create(boost::python::list const& meanIn,
                                                              boost::python::list const& covIn,
                                                              bool                       isDiag)
{
  if (isDiag) {
    return make_shared<ScaledMvNormRVPython>(GetEigenVector<Eigen::VectorXd>(meanIn),
                                             GetEigenVector<Eigen::VectorXd>(covIn));
  } else {
    return make_shared<ScaledMvNormRVPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenMatrix(covIn));
  }
}

shared_ptr<ScaledMvNormRVPython> ScaledMvNormRVPython::CreateMode(boost::python::list const                    & meanIn,
                                                                  boost::python::list const                    & covIn,
                                                                  bool                                           isDiag,
                                                                  ConstantMVNormSpecification::SpecificationMode mode)
{
  if (isDiag) {
    return make_shared<ScaledMvNormRVPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenVector<Eigen::VectorXd>(
                                               covIn), mode);
  } else {
    return make_shared<ScaledMvNormRVPython>(GetEigenVector<Eigen::VectorXd>(meanIn), GetEigenMatrix(covIn), mode);
  }
}

boost::python::list ScaledMvNormRVPython::PySampleInputs(double const sigma,
                                                         int          numSamps)
{
  vector<Eigen::VectorXd> input(1, sigma * Eigen::VectorXd::Ones(1));

  return GetPythonMatrix(Sample(input, numSamps));
}

boost::python::list ScaledMvNormRVPython::PySampleInputsOneSamp(double const sigma)
{
  vector<Eigen::VectorXd> input(1, sigma * Eigen::VectorXd::Ones(1));

  return GetPythonVector<Eigen::VectorXd>(Sample(input, 1));
}

void muq::Modelling::ExportScaledMvNormRV()
{
  // expose constructors
  boost::python::class_<ScaledMvNormRVPython, std::shared_ptr<ScaledMvNormRVPython>,
                        boost::python::bases<RandVarPython, ModPiece>, boost::noncopyable> exportMvNormRV(
    "ScaledMvNormRV",
    boost::python::
    init<std::shared_ptr<ConstantMVNormSpecification> const&>());

  exportMvNormRV.def(boost::python::init<int>());
  exportMvNormRV.def(boost::python::init<boost::python::list const&, double>());
  exportMvNormRV.def("__init__", boost::python::make_constructor(&ScaledMvNormRVPython::Create));
  exportMvNormRV.def("__init__", boost::python::make_constructor(&ScaledMvNormRVPython::CreateMode));

  exportMvNormRV.def("Sample", &ScaledMvNormRVPython::PySampleInputs);
  exportMvNormRV.def("Sample", &ScaledMvNormRVPython::PySampleInputsOneSamp);

  exportMvNormRV.enable_pickling();

  // convert to parent, ModPiece, and RandVar
  boost::python::implicitly_convertible<std::shared_ptr<ScaledMvNormRVPython>, std::shared_ptr<ScaledMvNormRV> >();
  boost::python::implicitly_convertible<std::shared_ptr<ScaledMvNormRVPython>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<ScaledMvNormRVPython>, std::shared_ptr<RandVar> >();
}

