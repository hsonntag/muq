#include "MUQ/Modelling/python/RosenbrockDensityPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;

void muq::Modelling::ExportRosenbrockDensity() 
{
  py::class_<RosenbrockDensity, shared_ptr<RosenbrockDensity>, py::bases<Density, ModPiece>, boost::noncopyable> exportRosenDens("RosenbrockDensity", py::init<double const, double const>());

  // convert to ModPiece and Density
  py::implicitly_convertible<shared_ptr<RosenbrockDensity>, shared_ptr<ModPiece> >();
  py::implicitly_convertible<shared_ptr<RosenbrockDensity>, shared_ptr<Density> >();

}
