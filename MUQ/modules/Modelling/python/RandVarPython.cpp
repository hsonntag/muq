
#include "MUQ/Modelling/python/RandVarPython.h"
#include "MUQ/Utilities/python/PythonTranslater.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;

RandVarPython::RandVarPython(Eigen::VectorXi const& inputSizes,
                             int const              outputSize,
                             bool const             hasDirectGradient,
                             bool const             hasDirectJacobian,
                             bool const             hasDirectJacobianAction,
                             bool const             hasDirectHessian) : RandVar(inputSizes,
                                                                                                             outputSize,
                                                                                                             hasDirectGradient,
                                                                                                             hasDirectJacobian,
                                                                                                             hasDirectJacobianAction,
                                                                                                             hasDirectHessian)
{}

boost::python::list RandVarPython::PySample(boost::python::list const& pyInput, int NumSamps)
{
  vector<Eigen::VectorXd> input = PythonListToVector(pyInput);
  return GetPythonVector<Eigen::VectorXd>(Sample(input, NumSamps));
}

void muq::Modelling::ExportRandVar()
{
  // let python know about RandVar
  boost::python::class_<RandVarPython, std::shared_ptr<RandVarPython>, boost::python::bases<ModPiece>,
                        boost::noncopyable> exportRandVar("RandVar", boost::python::no_init);

  exportRandVar.def("Sample", &RandVarPython::PySample);

  boost::python::register_ptr_to_python<std::shared_ptr<RandVar> >();

  // allow ocnversion to parent and ModPiece
  boost::python::implicitly_convertible<std::shared_ptr<RandVarPython>, std::shared_ptr<RandVar> >();
  boost::python::implicitly_convertible<std::shared_ptr<RandVarPython>, std::shared_ptr<ModPiece> >();
}
