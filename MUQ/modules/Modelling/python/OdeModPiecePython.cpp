#include "MUQ/Modelling/python/OdeModPiecePython.h"
#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Utilities/python/PythonTranslater.h"

using namespace std;

using namespace muq::Utilities;
using namespace muq::Modelling;


OdeModPiecePython::OdeModPiecePython(std::shared_ptr<ModPiece>    const& fIn,
                                     std::shared_ptr<ModPiece>    const& gIn,
                                     boost::python::list const&   evalTimes,
                                     boost::python::dict const&   dict) : OdeModPiece(fIn,gIn,GetEigenVector<Eigen::VectorXd>(evalTimes),PythonDictToPtree(dict)){}
                                    
OdeModPiecePython::OdeModPiecePython(std::shared_ptr<ModPiece>    const& fIn,
                                    std::shared_ptr<ModPiece>     const& gIn,
                                    boost::python::list const&    evalTimes) : OdeModPiece(fIn,gIn,GetEigenVector<Eigen::VectorXd>(evalTimes)){}

OdeModPiecePython::OdeModPiecePython(std::shared_ptr<ModPiece>    const& fIn,
                                     boost::python::list const&   evalTimes,
                                     boost::python::dict const&   dict) : OdeModPiece(fIn,GetEigenVector<Eigen::VectorXd>(evalTimes),PythonDictToPtree(dict)){}

OdeModPiecePython::OdeModPiecePython(std::shared_ptr<ModPiece>    const& fIn,
                                    boost::python::list const&    evalTimes) : OdeModPiece(fIn,GetEigenVector<Eigen::VectorXd>(evalTimes)){}
                      
                    
                  

void muq::Modelling::ExportOdeModPiece()
{
  // create class and constructors
  boost::python::class_<OdeModPiecePython, std::shared_ptr<OdeModPiecePython>, boost::python::bases<ModPiece>, boost::noncopyable> exportOdeModPiece("OdeModPiece", 
    boost::python::init<std::shared_ptr<ModPiece> const&, std::shared_ptr<ModPiece> const&, boost::python::list const&, boost::python::dict const&>());
    
  exportOdeModPiece.def(boost::python::init<std::shared_ptr<ModPiece> const&, std::shared_ptr<ModPiece> const&, boost::python::list const&>());
  exportOdeModPiece.def(boost::python::init<std::shared_ptr<ModPiece> const&, boost::python::list const&, boost::python::dict const&>());
  exportOdeModPiece.def(boost::python::init<std::shared_ptr<ModPiece> const&, boost::python::list const&>());

  exportOdeModPiece.def("Gradient",&ModPiece::PyEvaluate);
  exportOdeModPiece.def("Gradient",&ModPiece::PyGradient);
  exportOdeModPiece.def("Jacobian",&ModPiece::PyJacobian);
  exportOdeModPiece.def("JacobianAction",&ModPiece::PyJacobianAction);
  exportOdeModPiece.def("Hessian",&ModPiece::PyHessian);

  // allow conversion to parent
  boost::python::implicitly_convertible<std::shared_ptr<OdeModPiecePython>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<OdeModPiecePython>, std::shared_ptr<OdeModPiece> >();
}

