
#include <iostream>
#include <math.h>
#include <vector>

// google testing library
#include "gtest/gtest.h"

// muq related includes
#include "MUQ/Modelling/ModPiece.h"


using namespace muq::Modelling;


class EigenMod : public ModPiece<> {
public:

  /** Constructor taking vector dimension and resizing the State.*/
  EigenMod(int dim)
  {
    DimIn[0] = dim;
    DimOut   = dim;
    State.resize(dim);
  }

  virtual void UpdateBase(const Eigen::VectorXd& VectorIn)
  {
    // fill in the State with sin(input)
    for (int i = 0; i < DimIn[0]; ++i) {
      State[i] = sin(VectorIn[i]);
    }
  }

  virtual void GradBase(const Eigen::VectorXd& VectorIn,
                        const Eigen::VectorXd& SensIn,
                        Eigen::VectorXd      & GradVec,
                        int                    dim)
  {
    // fill in the State with sin(input)
    for (int i = 0; i < DimIn[0]; ++i) {
      GradVec[i] = SensIn[i] * cos(VectorIn[i]);
    }
  }
};


class StdMod : public ModPiece<std::vector<double>, 1> {
public:

  /** Constructor taking vector dimension and resizing the State.*/
  StdMod(int dim)
  {
    DimIn[0] = dim;
    DimOut   = dim;
    State.resize(dim);
  }

  virtual void UpdateBase(const std::vector<double>& VectorIn)
  {
    // fill in the State with sin(input)
    for (int i = 0; i < DimIn[0]; ++i) {
      State[i] = sin(VectorIn[i]);
    }
  }

  virtual void GradBase(const std::vector<double>& VectorIn,
                        const std::vector<double>& SensIn,
                        std::vector<double>      & GradVec,
                        int                        dim)
  {
    // fill in the State with sin(input)
    for (int i = 0; i < DimIn[0]; ++i) {
      GradVec[i] = SensIn[i] * cos(VectorIn[i]);
    }
  }
};


TEST(ModellingBasicModPieceBaseTest, EigenEigenEval)
{
  int dim = 10;

  // create the input vector
  Eigen::VectorXd Input = Eigen::VectorXd::Ones(dim);
  Eigen::VectorXd Output(10);

  // create the forward model object
  EigenMod ForwardTest(dim);

  ForwardTest.Update(Input);
  ForwardTest.Eval(Output);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(sin(Input[i]), Output[i]);
  }
}

TEST(ModellingBasicModPieceBaseTest, EigenStdEval)
{
  int dim = 10;

  // create the input vector
  std::vector<double> Input(10, 1);
  std::vector<double> Output(10);

  // create the forward model object
  EigenMod ForwardTest(dim);

  ForwardTest.Update(Input);
  ForwardTest.Eval(Output);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(sin(Input[i]), Output[i]);
  }
}


TEST(ModellingBasicModPieceBaseTest, EigenDblPtrEval)
{
  int dim = 10;

  // create the input vector
  double *Input = new double[dim];

  for (int i = 0; i < dim; ++i) {
    Input[i] = 1;
  }
  double *Output = new double[dim];


  // create the forward model object
  EigenMod ForwardTest(dim);

  ForwardTest.Update(Input);
  ForwardTest.Eval(Output);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(sin(Input[i]), Output[i]);
  }

  delete[] Input;
  delete[] Output;
}

TEST(ModellingBasicModPieceBaseTest, StdEigenEval)
{
  int dim = 10;

  // create the input vector
  Eigen::VectorXd Input = Eigen::VectorXd::Ones(dim);
  Eigen::VectorXd Output(10);

  // create the forward model object
  StdMod ForwardTest(dim);

  ForwardTest.Update(Input);
  ForwardTest.Eval(Output);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(sin(Input[i]), Output[i]);
  }
}

TEST(ModellingBasicModPieceBaseTest, StdStdEval)
{
  int dim = 10;

  // create the input vector
  std::vector<double> Input(10, 1);
  std::vector<double> Output(10);

  // create the forward model object
  StdMod ForwardTest(dim);

  ForwardTest.Update(Input);
  ForwardTest.Eval(Output);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(sin(Input[i]), Output[i]);
  }
}


TEST(ModellingBasicModPieceBaseTest, StdDblPtrEval)
{
  int dim = 10;

  // create the input vector
  double *Input = new double[dim];

  for (int i = 0; i < dim; ++i) {
    Input[i] = 1;
  }
  double *Output = new double[dim];


  // create the forward model object
  StdMod ForwardTest(dim);

  ForwardTest.Update(Input);
  ForwardTest.Eval(Output);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(sin(Input[i]), Output[i]);
  }

  delete[] Input;
  delete[] Output;
}

TEST(ModellingGradModPieceBaseTest, EigenEigenGrad)
{
  int dim = 10;

  // create the input vector
  Eigen::VectorXd Input = Eigen::VectorXd::Ones(dim);
  Eigen::VectorXd GradVec(10);
  Eigen::VectorXd Sens = Eigen::VectorXd::Ones(dim);

  // create the forward model object
  EigenMod ForwardTest(dim);

  ForwardTest.Update(Input);
  ForwardTest.Grad(Input, Sens, GradVec, 0);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(cos(Input[i]), GradVec[i]);
  }

  // try finite differences
  ForwardTest.Grad(Input, Sens, GradVec, 0, 1e-4, 1e-10);
  for (int i = 0; i < dim; ++i) {
    EXPECT_NEAR(cos(Input[i]), GradVec[i], 5e-4);
  }
}

TEST(ModellingGradModPieceBaseTest, EigenStdGrad)
{
  int dim = 10;

  // create the input vector
  std::vector<double> Input(dim, 1);
  std::vector<double> GradVec(10);
  std::vector<double> Sens(dim, 1);

  // create the forward model object
  EigenMod ForwardTest(dim);

  ForwardTest.Update(Input);
  ForwardTest.Grad(Input, Sens, GradVec, 0);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(cos(Input[i]), GradVec[i]);
  }

  // try finite differences
  ForwardTest.Grad(Input, Sens, GradVec, 0, 1e-4, 1e-10);
  for (int i = 0; i < dim; ++i) {
    EXPECT_NEAR(cos(Input[i]), GradVec[i], 5e-4);
  }
}

TEST(ModellingGradModPieceBaseTest, EigenDblPtrGrad)
{
  int dim = 10;

  // create the input vector
  double *Input   = new double[dim];
  double *GradVec = new double[dim];
  double *Sens    = new double[dim];

  for (int i = 0; i < dim; ++i) {
    Input[i] = 1;
    Sens[i]  = 1;
  }

  // create the forward model object
  EigenMod ForwardTest(dim);

  ForwardTest.Update(Input);
  ForwardTest.Grad(Input, Sens, GradVec, 0);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(cos(Input[i]), GradVec[i]);
  }

  // try finite differences
  ForwardTest.Grad(Input, Sens, GradVec, 0, 1e-4, 1e-10);

  for (int i = 0; i < dim; ++i) {
    EXPECT_NEAR(cos(Input[i]), GradVec[i], 5e-4);
  }

  delete[] Input;
  delete[] GradVec;
  delete[] Sens;
}

TEST(ModellingGradModPieceBaseTest, StdEigenGrad)
{
  int dim = 10;

  // create the input vector
  Eigen::VectorXd Input = Eigen::VectorXd::Ones(dim);
  Eigen::VectorXd GradVec(10);
  Eigen::VectorXd Sens = Eigen::VectorXd::Ones(dim);

  // create the forward model object
  StdMod ForwardTest(dim);

  ForwardTest.Update(Input);
  ForwardTest.Grad(Input, Sens, GradVec, 0);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(cos(Input[i]), GradVec[i]);
  }

  // try finite differences
  ForwardTest.Grad(Input, Sens, GradVec, 0, 1e-4, 1e-10);
  for (int i = 0; i < dim; ++i) {
    EXPECT_NEAR(cos(Input[i]), GradVec[i], 5e-4);
  }
}

TEST(ModellingGradModPieceBaseTest, StdStdGrad)
{
  int dim = 10;

  // create the input vector
  std::vector<double> Input(dim, 1);
  std::vector<double> GradVec(10);
  std::vector<double> Sens(dim, 1);

  // create the forward model object
  StdMod ForwardTest(dim);

  ForwardTest.Update(Input);
  ForwardTest.Grad(Input, Sens, GradVec, 0);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(cos(Input[i]), GradVec[i]);
  }

  // try finite differences
  ForwardTest.Grad(Input, Sens, GradVec, 0, 1e-4, 1e-10);
  for (int i = 0; i < dim; ++i) {
    EXPECT_NEAR(cos(Input[i]), GradVec[i], 5e-4);
  }
}

TEST(ModellingGradModPieceBaseTest, StdDblPtrGrad)
{
  int dim = 10;

  // create the input vector
  double *Input   = new double[dim];
  double *GradVec = new double[dim];
  double *Sens    = new double[dim];

  for (int i = 0; i < dim; ++i) {
    Input[i] = 1;
    Sens[i]  = 1;
  }

  // create the forward model object
  StdMod ForwardTest(dim);

  ForwardTest.Update(Input);
  ForwardTest.Grad(Input, Sens, GradVec, 0);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(cos(Input[i]), GradVec[i]);
  }

  // try finite differences
  ForwardTest.Grad(Input, Sens, GradVec, 0, 1e-4, 1e-10);

  for (int i = 0; i < dim; ++i) {
    EXPECT_NEAR(cos(Input[i]), GradVec[i], 5e-4);
  }

  delete[] Input;
  delete[] GradVec;
  delete[] Sens;
}
