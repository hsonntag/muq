import unittest
from numpy import *

import libmuqModelling

# a simple test modpiece
class testMod(libmuqModelling.OneInputNoDerivModPiece):
    def __init__(self,inDim,outDim,retVal=1.0):
        libmuqModelling.OneInputNoDerivModPiece.__init__(self,inDim,outDim)
        self.retVal = retVal

    def EvaluateImpl(self,x):
        outVec = self.retVal*array([1.0]*self.outputSize)
        return outVec.tolist()

class testMod2(libmuqModelling.ModPiece):
    def __init__(self,indims,outdim):
        super(testMod2,self).__init__(indims,outdim,False,False,False,False,False)

    def EvaluateImpl(self,x):
        return [1.0]*self.outputSize

class sinSumMod(libmuqModelling.ModPiece):
    def __init__(self,dim,numIn=2):
        super(sinSumMod,self).__init__([dim]*numIn,dim,True,True,True,True,False)

    def EvaluateImpl(self,x):
        output = [0.0]*self.outputSize
        for i in range(len(x)):
            output += sin(x[i])
        return output.tolist()

    def GradientImpl(self,x,sens,dim):
        return (cos(x[dim])*array(sens)).tolist()

    def JacobianImpl(self,x,dim):
        output = zeros((self.outputSize,self.outputSize))
        for i in range(self.outputSize):
            output[i][i] = cos(x[dim][i])
        return output.tolist()

    def JacobianActionImpl(self,x,targ,dim):
        return dot(array(self.Jacobian(x,dim)),array(targ)).tolist()

    def HessianImpl(self,x,sens,dim):
        output = zeros((self.outputSize,self.outputSize))
        for i in range(self.outputSize):
            output[i][i] -= sens[i]*sin(x[dim][i])
        return output.tolist()

class ModGraphTest(unittest.TestCase):
    def testBasicConstruction(self):
        # construct an empty ModGraph
        myGraph = libmuqModelling.ModGraph()

        # create some models
        x0 = testMod(3,2)
        x1 = testMod(2,1)
        x2 = testMod(2,1)
        x3 = testMod2([1,1],1)

        # add a few nodes
        myGraph.AddNode(x0,"x0")
        myGraph.AddNode(x1,"x1")
        myGraph.AddNode(x2,"x2")
        myGraph.AddNode(x3,"x3")

        # make sure we have the right number of inputs and outputs w/o any connections
        self.assertEqual(4,myGraph.NumOutputs())
        self.assertEqual(5,myGraph.NumInputs())

        # create edges
        myGraph.AddEdge("x0","x1",0)
        myGraph.AddEdge("x0","x2",0)
        myGraph.AddEdge("x1","x3",0)
        myGraph.AddEdge("x2","x3",1)

        # check the number of out/inputs with diamond shaped graph
        self.assertEqual(1,myGraph.NumOutputs())
        self.assertEqual(1,myGraph.NumInputs())

    def testSwapNode(self):
        # construct an empty ModGraph
        myGraph = libmuqModelling.ModGraph()

        # base node
        mod1 = testMod(3,2,1.0)
        myGraph.AddNode(mod1,"x")

        # should evaluate to 1.0
        self.assertTrue([1.0,1.0]==myGraph.GetNodeModel("x").Evaluate([0.0]*3))

        # swap a node
        mod2 = testMod(3,2,0.5)
        myGraph.SwapNode("x",mod2)

        # should evaluate to 0.5
        self.assertTrue([0.5,0.5]==myGraph.GetNodeModel("x").Evaluate([0.0]*3))

        # swap a node and rename
        mod3 = testMod(3,2,0.25)
        myGraph.SwapNode("x",mod3,"y")

        # should evaluate to 0.25
        self.assertTrue([0.25,0.25]==myGraph.GetNodeModel("y").Evaluate([0.0]*3))

    def testBindNode(self):
        # construct an empty ModGraph
        myGraph = libmuqModelling.ModGraph()

        # create some models
        x0 = testMod(3,2)
        x1 = testMod(2,1)
        x2 = testMod(2,1)
        x3 = testMod2([1,1],1)

        # add a few nodes
        myGraph.AddNode(x0,"x0")
        myGraph.AddNode(x1,"x1")
        myGraph.AddNode(x2,"x2")
        myGraph.AddNode(x3,"x3")

        # make sure we have the right number of inputs and outputs w/o any connections
        self.assertEqual(4,myGraph.NumOutputs())
        self.assertEqual(5,myGraph.NumInputs())

        # create edges
        myGraph.AddEdge("x0","x1",0)
        myGraph.AddEdge("x0","x2",0)
        myGraph.AddEdge("x1","x3",0)
        myGraph.AddEdge("x2","x3",1)
        
        # check the number of out/inputs with diamond shaped graph
        self.assertEqual(1,myGraph.NumOutputs())
        self.assertEqual(1,myGraph.NumInputs())

        # bind a node
        myGraph.BindNode("x0",[0.1]*2)

        # make sure we have the right number of inputs and outputs w/o any connections
        self.assertEqual(1,myGraph.NumOutputs())
        self.assertEqual(0,myGraph.NumInputs())

    def testBindEdge(self):
        # construct an empty ModGraph
        myGraph = libmuqModelling.ModGraph()

        # create some models
        x0 = testMod(3,2)
        x1 = testMod(2,1)
        x2 = testMod(2,1)
        x3 = testMod2([1,1],1)

        # add a few nodes
        myGraph.AddNode(x0,"x0")
        myGraph.AddNode(x1,"x1")
        myGraph.AddNode(x2,"x2")
        myGraph.AddNode(x3,"x3")

        # make sure we have the right number of inputs and outputs w/o any connections
        self.assertEqual(4,myGraph.NumOutputs())
        self.assertEqual(5,myGraph.NumInputs())

        # create edges
        myGraph.AddEdge("x0","x1",0)
        myGraph.AddEdge("x0","x2",0)
        myGraph.AddEdge("x1","x3",0)
        myGraph.AddEdge("x2","x3",1)
        
        # check the number of out/inputs with diamond shaped graph
        self.assertEqual(1,myGraph.NumOutputs())
        self.assertEqual(1,myGraph.NumInputs())

        # bind an edge
        myGraph.BindEdge("x1",0,[0.1]*2)
        
        # make sure we have the right number of inputs and outputs w/o any connections
        self.assertEqual(1,myGraph.NumOutputs())
        self.assertEqual(1,myGraph.NumInputs())

    def testMultipleGraphsAndConstant(self):
        # construct an empty ModGraph
        myGraph = libmuqModelling.ModGraph()

        # create some models
        x0 = testMod(3,2)
        x1 = testMod(2,1)
        x2 = testMod(2,1)
        x3 = testMod2([1,1],1)

        # add a few nodes
        myGraph.AddNode(x0,"x0")
        myGraph.AddNode(x1,"x1")
        myGraph.AddNode(x2,"x2")
        myGraph.AddNode(x3,"x3")

        # create edges
        myGraph.AddEdge("x0","x1",0)
        myGraph.AddEdge("x0","x2",0)
        myGraph.AddEdge("x1","x3",0)
        myGraph.AddEdge("x2","x3",1)

        # bind an edge
        myGraph.BindEdge("x1",0,[0.1]*2)

        self.assertFalse(myGraph.isConstant("x3"))
        self.assertTrue(myGraph.isConstant("x1"))
        self.assertFalse(myGraph.isConstant("x2"))

        # construct an empty ModGraph
        myGraph2 = libmuqModelling.ModGraph()

        # add a few nodes
        myGraph2.AddNode(x0,"x0")
        myGraph2.AddNode(x1,"x1")
        myGraph2.AddNode(x2,"x2")
        myGraph2.AddNode(x3,"x3")

        # make sure we have the right number of inputs and outputs w/o any connections
        self.assertEqual(4,myGraph2.NumOutputs())
        self.assertEqual(5,myGraph2.NumInputs())

        # create edges
        myGraph2.AddEdge("x0","x1",0)
        myGraph2.AddEdge("x0","x2",0)
        myGraph2.AddEdge("x1","x3",0)
        myGraph2.AddEdge("x2","x3",1)
        
        # check the number of out/inputs with diamond shaped graph
        self.assertEqual(1,myGraph2.NumOutputs())
        self.assertEqual(1,myGraph2.NumInputs())

        # bind an edge
        myGraph2.BindNode("x0",[0.1]*2)

        # check the number of out/inputs with diamond shaped graph
        self.assertEqual(1,myGraph2.NumOutputs())
        self.assertEqual(0,myGraph2.NumInputs())

    def testCut(self):
        myGraph = libmuqModelling.ModGraph()
        
        # create some models
        f2 = sinSumMod(2)
        f1 = sinSumMod(2)
        x = testMod(2,2)
        y1 = testMod(1,2)
        y2 = testMod(3,2)

        # nodes
        myGraph.AddNode(f2,"f2")
        myGraph.AddNode(f1,"f1")
        myGraph.AddNode(x,"x")
        myGraph.AddNode(y1,"y1")
        myGraph.AddNode(y2,"y2")

        # edges
        myGraph.AddEdge("x","f2",0)
        myGraph.AddEdge("y1","f1",0)
        myGraph.AddEdge("y2","f1",1)
        myGraph.AddEdge("f1","f2",1)

        # bind
        myGraph.BindNode("y1",[0.1]*2)
        myGraph.BindNode("y2",[0.2]*2)

        self.assertEqual(5,myGraph.NumNodes())

        # cut the graph
        myGraph2 = myGraph.DependentCut("f2")
        self.assertEqual(3,myGraph2.NumNodes())

    def testGraphViz(self):
        myGraph = libmuqModelling.ModGraph()

        # create some models
        f2 = sinSumMod(2)
        f1 = sinSumMod(2)
        x = testMod(2,2)
        y1 = testMod(1,2)
        y2 = testMod(3,2)

        # nodes
        myGraph.AddNode(f2,"f2")
        myGraph.AddNode(f1,"f1")
        myGraph.AddNode(x,"x")
        myGraph.AddNode(y1,"y1")
        myGraph.AddNode(y2,"y2")

        # edges
        myGraph.AddEdge("x","f2",0)
        myGraph.AddEdge("y1","f1",0)
        myGraph.AddEdge("y2","f1",1)
        myGraph.AddEdge("f1","f2",1)

        myGraph.writeGraphViz("results/tests/GraphViz/GraphTestPython.png")
        myGraph.writeGraphViz("results/tests/GraphViz/GraphTestPython.jpg")
        myGraph.writeGraphViz("results/tests/GraphViz/GraphTestPython.pdf")
        myGraph.writeGraphViz("results/tests/GraphViz/GraphTestPython.eps")
        myGraph.writeGraphViz("results/tests/GraphViz/GraphTestPython.svg")

    def testConnectedTest(self):
        myGraph = libmuqModelling.ModGraph()

        # create some models
        f2 = sinSumMod(2)
        f1 = sinSumMod(2)
        x = testMod(2,2)
        y1 = testMod(1,2)
        y2 = testMod(3,2)

        # nodes
        myGraph.AddNode(f2,"f2")
        myGraph.AddNode(f1,"f1")
        myGraph.AddNode(x,"x")
        myGraph.AddNode(y1,"y1")
        myGraph.AddNode(y2,"y2")

        # edges
        myGraph.AddEdge("x","f2",0)
        myGraph.AddEdge("y1","f1",0)
        myGraph.AddEdge("y2","f1",1)
        myGraph.AddEdge("f1","f2",1)

        self.assertTrue(myGraph.NodesConnected("y1","f2"))
        self.assertFalse(myGraph.NodesConnected("y1","x"))
        
    
        


        

        



