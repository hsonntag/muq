
#include <iostream>
#include <math.h>
#include <vector>

// google testing library
#include "gtest/gtest.h"

// muq related includes
#include "MUQ/Modelling/ModParameter.h"

using namespace muq::Modelling;


TEST(ModellingModParameterTest, EvalFromEigen)
{
  int dim               = 10;
  Eigen::VectorXd Input = 0.1 * Eigen::VectorXd::Ones(dim);
  ModParameter    TestParam(Input, dim);

  // evaluate the parameter to make sure it works
  Eigen::VectorXd TestEval(dim);

  TestParam.Eval(TestEval);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(0.1, TestEval[i]);
  }
}

TEST(ModellingModParameterTest, EvalFromStd)
{
  int dim = 10;
  std::vector<double> Input(dim, 0.1);
  ModParameter TestParam(Input, dim);

  // evaluate the parameter to make sure it works
  Eigen::VectorXd TestEval(dim);

  TestParam.Eval(TestEval);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(0.1, TestEval[i]);
  }
}

TEST(ModellingModParameterTest, EvalFromDblPtr)
{
  int dim       = 10;
  double *Input = new double[dim];

  for (int i = 0; i < dim; ++i) {
    Input[i] = 0.1;
  }

  ModParameter TestParam(Input, dim);

  // evaluate the parameter to make sure it works
  Eigen::VectorXd TestEval(dim);
  TestParam.Eval(TestEval);

  for (int i = 0; i < dim; ++i) {
    ASSERT_EQ(0.1, TestEval[i]);
  }

  delete[] Input;
}