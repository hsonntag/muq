import unittest
from numpy import *

import libmuqModelling

class ModGraphOperationsTest(unittest.TestCase):
    
    def testModGraphAdd(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);
        node2 = libmuqModelling.VectorPassthroughModel(2);
        
        graph1 = libmuqModelling.ModGraph()
        graph2 = libmuqModelling.ModGraph()
        
        graph1.AddNode(node1, node1.GetName());
        graph2.AddNode(node2, node2.GetName());
        

        sumGraph = graph1 + graph2;

        sumGraph.writeGraphViz("results/tests/GraphViz/AddModGraphPython.pdf")

        sum_piece = sumGraph.ConstructModPiece(sumGraph.GetOutputNodeName())

          
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.inputSizes[1]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2],[3,4]])
        self.assertEqual(sumResult[0], 4)
        self.assertEqual(sumResult[1], 6)



    def testModPieceAdd(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);
        node2 = libmuqModelling.VectorPassthroughModel(2);

        myGraph = node1 + node2
        
        sum_piece = myGraph.ConstructModPiece(myGraph.GetOutputNodeName())
          
        self.assertTrue(len(sum_piece.inputSizes)==2)
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.inputSizes[1]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2],[3,4]])
        self.assertEqual(sumResult[0], 4)
        self.assertEqual(sumResult[1], 6)
  
      
    def testModPieceAddSame(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);

        myGraph = node1 + node1
        
        sum_piece = myGraph.ConstructModPiece(myGraph.GetOutputNodeName())
        
        self.assertTrue(len(sum_piece.inputSizes)==1)
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2]])
        self.assertEqual(sumResult[0], 2)
        self.assertEqual(sumResult[1], 4)


    def testModPieceAddMixed1(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);
        node2 = libmuqModelling.VectorPassthroughModel(2);
        
        graph1 = libmuqModelling.ModGraph()
        
        
        graph1.AddNode(node1, node1.GetName());
        

        sumGraph = graph1 + node2;

        sum_piece = sumGraph.ConstructModPiece(sumGraph.GetOutputNodeName())

          
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.inputSizes[1]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2],[3,4]])
        self.assertEqual(sumResult[0], 4)
        self.assertEqual(sumResult[1], 6)

    def testModPieceAddMixed2(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);
        node2 = libmuqModelling.VectorPassthroughModel(2);
        
        graph1 = libmuqModelling.ModGraph()
        
        
        graph1.AddNode(node1, node1.GetName());
        

        sumGraph = node2 + graph1;

        sum_piece = sumGraph.ConstructModPiece(sumGraph.GetOutputNodeName())

          
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.inputSizes[1]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2],[3,4]])
        self.assertEqual(sumResult[0], 4)
        self.assertEqual(sumResult[1], 6)        
        
    def testModGraphDiff(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);
        node2 = libmuqModelling.VectorPassthroughModel(2);
        
        graph1 = libmuqModelling.ModGraph()
        graph2 = libmuqModelling.ModGraph()
        
        graph1.AddNode(node1, node1.GetName());
        graph2.AddNode(node2, node2.GetName());

        sumGraph = graph1 - graph2;

        sum_piece = sumGraph.ConstructModPiece(sumGraph.GetOutputNodeName())

          
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.inputSizes[1]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2],[3,4]])
        self.assertEqual(sumResult[0], -2)
        self.assertEqual(sumResult[1], -2)



    def testModPieceDiff(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);
        node2 = libmuqModelling.VectorPassthroughModel(2);

        myGraph = node1 - node2
        
        sum_piece = myGraph.ConstructModPiece(myGraph.GetOutputNodeName())
          
        self.assertTrue(len(sum_piece.inputSizes)==2)
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.inputSizes[1]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2],[3,4]])
        self.assertEqual(sumResult[0], -2)
        self.assertEqual(sumResult[1], -2)
  
      
    def testModPieceDiffSame(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);

        myGraph = node1 - node1
        
        sum_piece = myGraph.ConstructModPiece(myGraph.GetOutputNodeName())
        
        self.assertTrue(len(sum_piece.inputSizes)==1)
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2]])
        self.assertEqual(sumResult[0], 0)
        self.assertEqual(sumResult[1], 0)


    def testModPieceDiffMixed1(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);
        node2 = libmuqModelling.VectorPassthroughModel(2);
        
        graph1 = libmuqModelling.ModGraph()
        
        
        graph1.AddNode(node1, node1.GetName());
        

        sumGraph = graph1 - node2;

        sum_piece = sumGraph.ConstructModPiece(sumGraph.GetOutputNodeName())

          
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.inputSizes[1]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2],[3,4]])
        self.assertEqual(sumResult[0], -2)
        self.assertEqual(sumResult[1], -2)
        
    def testModPieceDiffMixed2(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);
        node2 = libmuqModelling.VectorPassthroughModel(2);
        
        graph1 = libmuqModelling.ModGraph()
        
        
        graph1.AddNode(node1, node1.GetName());
        

        sumGraph = node2 - graph1;

        sum_piece = sumGraph.ConstructModPiece(sumGraph.GetOutputNodeName())

          
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.inputSizes[1]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2],[3,4]])
        self.assertEqual(sumResult[0], 2)
        self.assertEqual(sumResult[1], 2)
        
    def testModGraphProduct(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);
        node2 = libmuqModelling.VectorPassthroughModel(2);
        
        graph1 = libmuqModelling.ModGraph()
        graph2 = libmuqModelling.ModGraph()
        
        graph1.AddNode(node1, node1.GetName());
        graph2.AddNode(node2, node2.GetName());

        sumGraph = graph1 * graph2;

        sum_piece = sumGraph.ConstructModPiece(sumGraph.GetOutputNodeName())

          
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.inputSizes[1]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2],[3,4]])
        self.assertEqual(sumResult[0], 3)
        self.assertEqual(sumResult[1], 8)



    def testModPieceProduct(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);
        node2 = libmuqModelling.VectorPassthroughModel(2);

        myGraph = node1 * node2
        
        sum_piece = myGraph.ConstructModPiece(myGraph.GetOutputNodeName())
          
        self.assertTrue(len(sum_piece.inputSizes)==2)
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.inputSizes[1]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2],[3,4]])
        self.assertEqual(sumResult[0], 3)
        self.assertEqual(sumResult[1], 8)
  
      
    def testModPieceProductSame(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);

        myGraph = node1 * node1
        
        sum_piece = myGraph.ConstructModPiece(myGraph.GetOutputNodeName())
        
        self.assertTrue(len(sum_piece.inputSizes)==1)
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[3,2]])
        self.assertEqual(sumResult[0], 9)
        self.assertEqual(sumResult[1], 4)


    def testModPieceProductMixed1(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);
        node2 = libmuqModelling.VectorPassthroughModel(2);
        
        graph1 = libmuqModelling.ModGraph()
        
        
        graph1.AddNode(node1, node1.GetName());
        

        sumGraph = graph1 * node2;

        sum_piece = sumGraph.ConstructModPiece(sumGraph.GetOutputNodeName())

          
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.inputSizes[1]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2],[3,4]])
        self.assertEqual(sumResult[0], 3)
        self.assertEqual(sumResult[1], 8)
        
        
    def testModPieceProductMixed2(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);
        node2 = libmuqModelling.VectorPassthroughModel(2);
        
        graph1 = libmuqModelling.ModGraph()
        
        
        graph1.AddNode(node1, node1.GetName());
        

        sumGraph = node2 * graph1;

        sum_piece = sumGraph.ConstructModPiece(sumGraph.GetOutputNodeName())

          
        self.assertTrue(sum_piece.inputSizes[0]==2)
        self.assertTrue(sum_piece.inputSizes[1]==2)
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2],[3,4]])
        self.assertEqual(sumResult[0], 3)
        self.assertEqual(sumResult[1], 8)
        
    def testScalarOps(self):
        
        node1 = libmuqModelling.VectorPassthroughModel(2);

        sumGraph = [[2.,0.], [0., 2.]] * (-(double(4.) * (double(1.) + node1 + double(2.)) * double(2.)) - double(1.0)) / double(.2);

        libmuqModelling.ModGraph(sumGraph).writeGraphViz("results/tests/GraphViz/ScalarOpsPython.pdf")
        sumGraph.writeGraphViz("results/tests/GraphViz/ScalarOpsPython.pdf")

        sum_piece = sumGraph.ConstructModPiece(sumGraph.GetOutputNodeName())

        self.assertTrue(sum_piece.inputSizes[0]==2)
        
        self.assertTrue(sum_piece.outputSize==2)
        
        sumResult = sum_piece.Evaluate([[1,2]])
        self.assertEqual(sumResult[0], -330)
        self.assertEqual(sumResult[1], -410)
  
  
  
   
    class BarModNoDeriv(libmuqModelling.ModPiece):
        def __init__(self):
            libmuqModelling.ModPiece.__init__(self,[1,1,1],3, False, False, False, False, False, "BarMod")
    
        def EvaluateImpl(self,x):
            return [x[0][0], x[1][0], x[2][0]]                

    def testComposeModPiece(self):
        node1 = libmuqModelling.VectorPassthroughModel(1);
        node1.SetName("x")
        node2 = libmuqModelling.VectorPassthroughModel(1);
        node2.SetName("y")
        barNode = self.BarModNoDeriv();
        graphComp   = libmuqModelling.GraphCompose(barNode, [libmuqModelling.ModGraph(node1), #a graph
                                                         node1,#a node
                                                         node2])

        graphComp.writeGraphViz("results/tests/GraphViz/ComposeModPiecePython.pdf")

        sum_piece = graphComp.ConstructModPiece(graphComp.GetOutputNodeName(), ["x", "y"])
        
        self.assertTrue(sum_piece.inputSizes == [1,1])
        self.assertTrue(sum_piece.outputSize==3)
        
        
        sumResult = sum_piece.Evaluate([[1],[2]])

        self.assertEqual(sumResult, [1,1,2])
        
        
    def testComposeModGraph(self):
        node1 = libmuqModelling.VectorPassthroughModel(1);
        node1.SetName("x")
        node2 = libmuqModelling.VectorPassthroughModel(1);
        node2.SetName("y")
        barNode = self.BarModNoDeriv();
        barGraph   = libmuqModelling.GraphCompose(barNode, [libmuqModelling.ModGraph(node1), #a graph
                                                         node1, #a node
                                                         node2]);

        node3 = libmuqModelling.VectorPassthroughModel(1);
        node3.SetName("a")
        node4 = libmuqModelling.VectorPassthroughModel(1);
        node4.SetName("b")
        
        graph = libmuqModelling.GraphCompose(barGraph, [libmuqModelling.ModGraph(node3), 
                                                         libmuqModelling.ModGraph(node4),],["x", "y"], []);
        graph.writeGraphViz("results/tests/GraphViz/ComposeModGraphPython.pdf")
        
        output_piece = graph.ConstructModPiece(graph.GetOutputNodeName())
        
          
        self.assertTrue(output_piece.inputSizes == [1,1])
        self.assertTrue(output_piece.outputSize==3)
        
        sumResult = output_piece.Evaluate([[1], [2]])
        self.assertEqual(sumResult, [1,1,2])        
