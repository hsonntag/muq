
#include <iostream>
#include <math.h>

// include the google testing header
#include "gtest/gtest.h"


#include "MUQ/Modelling/EmpiricalRandVar.h"
#include "MUQ/Modelling/GaussianRV.h"
#include "MUQ/Modelling/RandVar.h"
#include "MUQ/Modelling/UniformRandVar.h"

using namespace muq::Modelling;


TEST(ModellingRandVarTest, EmpRandVarTestmean_MeanCov)
{
  // sample dimensions
  const int dim = 3;
  
  Eigen::VectorXd tempSamp(dim);
  
  
  // create an empirical randvar instance
  EmpiricalRandVar Samps(dim);
  
  // add some samples
  tempSamp << 1.0, 2.0, 3.0;
  Samps.addSamp(tempSamp);
  
  tempSamp << 2.0, 4.0, 6.0;
  Samps.addSamp(tempSamp);
  
  tempSamp << 4.0, 8.0, 12.0;
  Samps.addSamp(tempSamp);

  tempSamp << 3.0, 6.0, 9.0;
  Samps.addSamp(tempSamp);
  
  tempSamp << 1.5, 2.5, 3.5;
  Samps.addSamp(tempSamp);
  
  auto SampleMean = Samps.getMean();
  auto SampleCov  = Samps.getCov();

  // compare the sample mean to the actual mean
  EXPECT_DOUBLE_EQ(2.3,SampleMean[0]);
  EXPECT_DOUBLE_EQ(4.5,SampleMean[1]);
  EXPECT_DOUBLE_EQ(6.7,SampleMean[2]);
  
  EXPECT_DOUBLE_EQ(1.45,SampleCov(0, 0));
  EXPECT_DOUBLE_EQ(3,SampleCov(1, 0));
  EXPECT_DOUBLE_EQ(6.25,SampleCov(1,1));
  EXPECT_DOUBLE_EQ(4.55,SampleCov(2, 0));
  EXPECT_DOUBLE_EQ(9.5,SampleCov(2, 1));
  EXPECT_DOUBLE_EQ(14.45,SampleCov(2, 2));
  
}


TEST(ModellingRandVarTest, EmpiricalRandVar_Quantile)
{
  // sample dimensions
  const int dim = 3;

  Eigen::VectorXd tempSamp(dim);


  // create an empirical randvar instance
  EmpiricalRandVar Samps(dim);

  // add some samples
  tempSamp << 1.0, 2.0, 3.0;
  Samps.addSamp(tempSamp);

  tempSamp << 2.0, 4.0, 6.0;
  Samps.addSamp(tempSamp);

  tempSamp << 4.0, 8.0, 12.0;
  Samps.addSamp(tempSamp);


  auto sampleQuant = Samps.getQuantile(0.5);
  EXPECT_DOUBLE_EQ(2.0, sampleQuant(0));
  EXPECT_DOUBLE_EQ(4.0, sampleQuant(1));
  EXPECT_DOUBLE_EQ(6.0, sampleQuant(2));

  tempSamp << 8.0, 16.0, 24.0;
  Samps.addSamp(tempSamp);

  sampleQuant = Samps.getQuantile(0.5);
  EXPECT_DOUBLE_EQ(3.0,  sampleQuant(0));
  EXPECT_DOUBLE_EQ(6.0,  sampleQuant(1));
  EXPECT_DOUBLE_EQ(9.0,  sampleQuant(2));

  sampleQuant = Samps.getQuantile(0.25);
  EXPECT_DOUBLE_EQ(1.5,  sampleQuant(0));
  EXPECT_DOUBLE_EQ(3.0,  sampleQuant(1));
  EXPECT_DOUBLE_EQ(4.5,  sampleQuant(2));

  sampleQuant = Samps.getQuantile(0.75);
  EXPECT_DOUBLE_EQ(6.0,  sampleQuant(0));
  EXPECT_DOUBLE_EQ(12.0, sampleQuant(1));
  EXPECT_DOUBLE_EQ(18.0, sampleQuant(2));

  tempSamp << 9.0, 17.0, 25.0;
  Samps.addSamp(tempSamp);
  Samps.ExpectedSize(10);

  sampleQuant = Samps.getQuantile(0.75);
  EXPECT_DOUBLE_EQ(8.0,  sampleQuant(0));
  EXPECT_DOUBLE_EQ(16.0, sampleQuant(1));
  EXPECT_DOUBLE_EQ(24.0, sampleQuant(2));
}

TEST(ModellingRandVarTest, CorrelatedEssTest){

    // test settings
    int Nsamps = 1e4;
    int dim = 2;

    // creat a mean vector
    Eigen::VectorXd state = Eigen::VectorXd::Zero(dim);


    auto myRV = std::make_shared<GaussianRV>(state,0.02);

    // create an empirical randvar instance
    EmpiricalRandVar Samps(dim);
    Samps.ExpectedSize(Nsamps);
    for(int i=0; i<Nsamps; ++i){
        state += myRV->Sample();
        Samps.addSamp(state);
    }
	
    // get the effective sample size
    auto ess_vec = Samps.getEss();

    for(int i=0; i<dim; ++i)
        EXPECT_LT(ess_vec[i],Nsamps);
}

