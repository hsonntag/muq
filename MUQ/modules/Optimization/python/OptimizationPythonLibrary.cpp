#include "MUQ/Optimization/python/ConstraintBasePython.h"
#include "MUQ/Optimization/python/OptProbBasePython.h"
#include "MUQ/Optimization/python/OptAlgBasePython.h"

using namespace muq::Optimization;

void muq::Optimization::ExportConstraintBase()
{
  // constraint base
  boost::python::class_<ConstraintBasePython, std::shared_ptr<ConstraintBasePython>,
                        boost::noncopyable> exportConstraintBase("ConstraintBase", boost::python::init<int, int>());

  // expose functions
  exportConstraintBase.def("Eval", &ConstraintBasePython::PyEval);
  exportConstraintBase.def("ApplyJacTrans", &ConstraintBasePython::PyApplyJacTrans);
  exportConstraintBase.def("GetJacobian", &ConstraintBase::PyGetJacobian);
  exportConstraintBase.def("GetHess", &ConstraintBase::PyGetHess);
  exportConstraintBase.def("NumConstraints", &ConstraintBase::NumConstraints);
  exportConstraintBase.def("GetDim", &ConstraintBase::GetDim);

  // expose parent to python
  boost::python::register_ptr_to_python<std::shared_ptr<ConstraintBase> >();

  // allow conversion to parent
  boost::python::implicitly_convertible<std::shared_ptr<ConstraintBasePython>, std::shared_ptr<ConstraintBase> >();

  // linear constraint
  boost::python::class_<LinearConstraintPython, std::shared_ptr<LinearConstraintPython>,
                        boost::python::bases<ConstraintBase>, boost::noncopyable> exportLinConstraint(
    "LinearConstraint",
    boost::python::init<boost::python::list,
                        boost::python::list>());

  // expose functions
  exportLinConstraint.def("Eval", &ConstraintBase::PyEval);
  exportLinConstraint.def("ApplyJacTrans", &ConstraintBase::PyApplyJacTrans);

  // allow conversion to parent
  boost::python::implicitly_convertible<std::shared_ptr<LinearConstraintPython>, std::shared_ptr<ConstraintBase> >();

  // bound constraint
  boost::python::class_<BoundConstraint, std::shared_ptr<BoundConstraint>, boost::python::bases<ConstraintBase>,
                        boost::noncopyable> exportBoundConstraint("BoundConstraint",
                                                                  boost::python::init<int, int, double, bool>());

  // expose functions
  exportBoundConstraint.def("Eval", &ConstraintBase::PyEval);
  exportBoundConstraint.def("ApplyJacTrans", &ConstraintBase::PyApplyJacTrans);
  exportBoundConstraint.def("WhichDim", &BoundConstraint::WhichDim);
  exportBoundConstraint.def("GetVal", &BoundConstraint::GetVal);
  exportBoundConstraint.def("IsLower", &BoundConstraint::isLower);

  // allow conversion to parent
  boost::python::implicitly_convertible<std::shared_ptr<BoundConstraint>, std::shared_ptr<ConstraintBase> >();

  // constraint list
  boost::python::class_<ConstraintList, std::shared_ptr<ConstraintList>, boost::python::bases<ConstraintBase>,
                        boost::noncopyable> exportConstraintList("ConstraintList", boost::python::init<int>());

  // expose functions
  exportConstraintList.def("Eval", &ConstraintBase::PyEval);
  exportConstraintList.def("ApplyJacTrans", &ConstraintBase::PyApplyJacTrans);
  exportConstraintList.def("Add", &ConstraintList::Add);
  exportConstraintList.def("NumBound", &ConstraintList::NumBound);
  exportConstraintList.def("NumLinear", &ConstraintList::NumLinear);
  exportConstraintList.def("NumNonlinear", &ConstraintList::NumNonlinear);

  // allow conversion to parent
  boost::python::implicitly_convertible<std::shared_ptr<ConstraintList>, std::shared_ptr<ConstraintBase> >();
}

BOOST_PYTHON_MODULE(libmuqOptimization)
{
  ExportOptProbBase();
  ExportOptAlgBase();
  ExportConstraintBase();
}