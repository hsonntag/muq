import unittest
import numpy as np

import libmuqModelling
import libmuqOptimization

class RoseFunc(libmuqOptimization.OptProbBase):
    def __init__(self):
        libmuqOptimization.OptProbBase.__init__(self, 2)

    def Evaluate(self, x):
        return pow(1.0-x[0],2.0)+100.0*pow(x[1]-x[0]*x[0],2.0)

    def Grad(self, x):
        gradient = [0.0]*2
        gradient[0] = -400.0*(x[1]-x[0]*x[0])*x[0]-2.0*(1.0-x[0])
        gradient[1] = 200.0*(x[1]-x[0]*x[0])

        return [self.Evaluate(x), gradient]

    def ApplyInvHess(self, x, vec):
        hess = np.matrix([[0.0]*2]*2)

        hess[0,0] = 1200.0*pow(x[0],2.0) - 400.0*x[1] + 2
        hess[0,1] = -400.0*x[0]
        hess[1,0] = -400.0*x[0]
        hess[1,1] = 200

        return np.linalg.solve(hess, vec)

def CreateParams():
    params = dict()
    params["Opt.MaxIts"]             = 10000
    params["Opt.ftol"]               = 1.0e-11
    params["Opt.xtol"]               = 1.0e-13
    params["Opt.gtol"]               = 1.0e-10
    params["Opt.LineSearch.LineIts"] = 100
    
    params["Opt.NLOPT.MaxEvals"]     = 100000
    params["Opt.NLOPT.xtol_rel"]     = 1.0e-12
    params["Opt.NLOPT.xtol_abs"]     = 1.0e-12
    params["Opt.NLOPT.ftol_rel"]     = 1.0e-12
    params["Opt.NLOPT.ftol_abs"]     = 1.0e-12
    
    params["Opt.Method"]             = "NLOPT"

    return params
    
class NloptTest(unittest.TestCase):
    def setUp(self):
        # make a problem
        self.prob = RoseFunc()

        # initial guess
        self.x0 = [-1.0, 3.0]

        # parameters
        self.params = CreateParams()

    def tearDown(self):
        # create the solver
        solver = libmuqOptimization.OptAlgBase(self.prob, self.params)

        # solve
        soln = solver.Solve(self.x0)

        # check
        self.assertGreater(solver.GetStatus(), 0)
        for i in range(2):
            self.assertAlmostEqual(soln[i], 1.0, 3)
        
    def testRosenbrockCOBYLA(self):
        self.params["Opt.NLOPT.Method"] = "COBYLA"

    def testRosenbrockBOBYQA(self):
        self.params["Opt.NLOPT.Method"] = "BOBYQA"

    def testRosenbrockNEWUOA(self):
        self.params["Opt.NLOPT.Method"] = "NEWOUA"

    def testRosenbrockPRAXIS(self):
        self.params["Opt.NLOPT.Method"] = "PRAXIS"

    def testRosenbrockNM(self):
        self.params["Opt.NLOPT.Method"] = "NM"

    def testRosenbrockSbplx(self):
        self.params["Opt.NLOPT.Method"] = "Sbplx"

    def testRosenbrockMMA(self):
        self.params["Opt.NLOPT.Method"] = "MMA"

    def testRosenbrockSLSQP(self):
        self.params["Opt.NLOPT.Method"] = "SLSQP"

    def testRosenbrockLBFGS(self):
        self.params["Opt.NLOPT.Method"] = "LBFGS"

    def testRosenbrockPreTN(self):
        self.params["Opt.NLOPT.Method"] = "PreTN"

    def testRosenbrockLVM(self):
        self.params["Opt.NLOPT.Method"] = "LVM"
