
#ifndef _ConstrainedTestClasses_h
#define _ConstrainedTestClasses_h

// include the muq constraint base
#include "MUQ/Optimization/Constraints/ConstraintBase.h"

// include the unconstrainted test classes
#include "UnconstrainedTestClasses.h"

class NonlinConstraint : public muq::Optimization::ConstraintBase {
public:

  NonlinConstraint() : muq::Optimization::ConstraintBase(2, 1) {}

  // compute a simple quadratic constraint
  virtual Eigen::VectorXd eval(const Eigen::VectorXd& xc)
  {
    Eigen::VectorXd out = Eigen::VectorXd::Zero(1);

    out <<  -1.0 * xc[1] - (xc[0] - 0.5) * (xc[0] - 0.5) + 1;

    return out;
  }

  virtual Eigen::VectorXd ApplyJacTrans(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn)
  {
    Eigen::VectorXd output = Eigen::VectorXd::Zero(2);

    output[0] = -1.0 * vecIn[0];
    output[1] = -2.0 * vecIn[0] * (xc[0] - 0.5);

    return output;
  }
};


// set up a gtest test fixture for unconstrained deterministic solvers
class Optimization_BoundConstrainedRoseTest : public::testing::Test {
public:

  // the initial point to start the optimization
  Eigen::VectorXd x0;

  // a base ptree, swap the Opt.Method entry for testing different algorithms
  boost::property_tree::ptree testParams;

  // an instance of the quadratic optimization problem
  std::shared_ptr<muq::Optimization::OptProbBase> prob;

  // constructor
  Optimization_BoundConstrainedRoseTest()
  {
    // set the initial condition
    x0 = Eigen::VectorXd::Ones(2);
    x0 << 0, 0.1;

    // std::cout << "Starting point: " << x0.transpose() << std::endl;

    // set some of the optimization parameters
    testParams.put("Opt.MaxIts", 10000);
    testParams.put("Opt.ftol", 1e-12);
    testParams.put("Opt.xtol", 1e-13);
    testParams.put("Opt.gtol", 1e-12);
    testParams.put("Opt.ctol", 1e-12);
    testParams.put("Opt.LineSearch.LineIts", 100);
    testParams.put("Opt.StepLength", 1);
    testParams.put("Opt.verbosity", 0);

    testParams.put("Opt.NLOPT.MaxEvals", 100000);
    testParams.put("Opt.NLOPT.xtol_rel", 1e-5);
    testParams.put("Opt.NLOPT.xtol_abs", 1e-5);
    testParams.put("Opt.NLOPT.ftol_rel", 1e-5);
    testParams.put("Opt.NLOPT.ftol_abs", 1e-5);

    // create an instance of the optimization problem
    prob = std::make_shared<RoseFunc>();

    // add a couple bound constraints
    std::shared_ptr<muq::Optimization::ConstraintBase> Bound1 = std::make_shared<muq::Optimization::BoundConstraint>(2,
                                                                                                                     0,
                                                                                                                     0.9,
                                                                                                                     false); //
                                                                                                                             //
                                                                                                                             //
                                                                                                                             // x
                                                                                                                             //
                                                                                                                             //
                                                                                                                             // <=
                                                                                                                             //
                                                                                                                             //
                                                                                                                             // 0.9
    std::shared_ptr<muq::Optimization::ConstraintBase> Bound2 = std::make_shared<muq::Optimization::BoundConstraint>(2,
                                                                                                                     1,
                                                                                                                     0.9,
                                                                                                                     false); //
                                                                                                                             //
                                                                                                                             //
                                                                                                                             // y
                                                                                                                             //
                                                                                                                             //
                                                                                                                             // <=
                                                                                                                             //
                                                                                                                             //
                                                                                                                             // 0.9

    prob->AddConstraint(Bound1, false);
    prob->AddConstraint(Bound2, false);
  }

  // the tear-down function is called just before the constructor, and in our case is where the tests are actually
  // performed
  void TearDown()
  {
    // set up the solver
    auto Solver = muq::Optimization::OptAlgBase::Create(prob, testParams);

    // solve the optimization problem
    Eigen::VectorXd xOpt = Solver->solve(x0);

    int optStat = Solver->GetStatus();


    // we expect a successful optimization
    EXPECT_GT(optStat, 0);

    // make sure the returned optimum is what we expect
    EXPECT_NEAR(xOpt[0], 0.9,  1e-1);
    EXPECT_NEAR(xOpt[1], 0.81, 1e-1);
  }
};


// set up a gtest test fixture for unconstrained deterministic solvers
class Optimization_FullyBoundRoseTest : public::testing::Test {
public:

  // the initial point to start the optimization
  Eigen::VectorXd x0;

  // a base ptree, swap the Opt.Method entry for testing different algorithms
  boost::property_tree::ptree testParams;

  // an instance of the quadratic optimization problem
  std::shared_ptr<muq::Optimization::OptProbBase> prob;

  // constructor
  Optimization_FullyBoundRoseTest()
  {
    // set the initial condition
    x0 = Eigen::VectorXd::Ones(2);
    x0 << -0.7, 0.8;

    // std::cout << "Starting point: " << x0.transpose() << std::endl;

    // set some of the optimization parameters
    testParams.put("Opt.MaxIts", 10000);
    testParams.put("Opt.ftol", 1e-11);
    testParams.put("Opt.xtol", 1e-13);
    testParams.put("Opt.gtol", 1e-10);
    testParams.put("Opt.ctol", 1e-10);
    testParams.put("Opt.LineSearch.LineIts", 100);
    testParams.put("Opt.StepLength", 1);
    testParams.put("Opt.verbosity", 0);

    testParams.put("Opt.NLOPT.MaxEvals", 100000);
    testParams.put("Opt.NLOPT.xtol_rel", 1e-12);
    testParams.put("Opt.NLOPT.xtol_abs", 1e-12);
    testParams.put("Opt.NLOPT.ftol_rel", 1e-13);
    testParams.put("Opt.NLOPT.ftol_abs", 1e-13);

    // create an instance of the optimization problem
    prob = std::make_shared<RoseFunc>();

    // add a couple bound constraints
    std::shared_ptr<muq::Optimization::ConstraintBase> Bound1 = std::make_shared<muq::Optimization::BoundConstraint>(2,
                                                                                                                     0,
                                                                                                                     2,
                                                                                                                     false); //
                                                                                                                             //
                                                                                                                             //
                                                                                                                             // x
                                                                                                                             //
                                                                                                                             //
                                                                                                                             // <=
                                                                                                                             //
                                                                                                                             //
                                                                                                                             // 2
    std::shared_ptr<muq::Optimization::ConstraintBase> Bound2 = std::make_shared<muq::Optimization::BoundConstraint>(2,
                                                                                                                     1,
                                                                                                                     2,
                                                                                                                     false); //
                                                                                                                             //
                                                                                                                             //
                                                                                                                             // y
                                                                                                                             //
                                                                                                                             //
                                                                                                                             // <=
                                                                                                                             //
                                                                                                                             //
                                                                                                                             // 2
    std::shared_ptr<muq::Optimization::ConstraintBase> Bound3 = std::make_shared<muq::Optimization::BoundConstraint>(2,
                                                                                                                     0,
                                                                                                                     -2,
                                                                                                                     true); //
                                                                                                                            //
                                                                                                                            //
                                                                                                                            // x
                                                                                                                            //
                                                                                                                            //
                                                                                                                            // >=
                                                                                                                            //
                                                                                                                            //
                                                                                                                            // -2
    std::shared_ptr<muq::Optimization::ConstraintBase> Bound4 = std::make_shared<muq::Optimization::BoundConstraint>(2,
                                                                                                                     1,
                                                                                                                     -2,
                                                                                                                     true); //
                                                                                                                            //
                                                                                                                            //
                                                                                                                            // y
                                                                                                                            //
                                                                                                                            //
                                                                                                                            // >=-2

    prob->AddConstraint(Bound1, false);
    prob->AddConstraint(Bound2, false);
    prob->AddConstraint(Bound3, false);
    prob->AddConstraint(Bound4, false);
  }

  // the tear-down function is called just before the constructor, and in our case is where the tests are actually
  // performed
  void TearDown()
  {
    // set up the solver
    auto Solver = muq::Optimization::OptAlgBase::Create(prob, testParams);

    // solve the optimization problem
    Eigen::VectorXd xOpt = Solver->solve(x0);

    int optStat = Solver->GetStatus();

    // we expect a successful optimization
    EXPECT_GT(optStat, 0);

    // make sure the returned optimum is what we expect
    EXPECT_NEAR(xOpt[0], 1, 1e-2);
    EXPECT_NEAR(xOpt[1], 1, 1e-2);
  }
};


// set up a gtest test fixture for unconstrained deterministic solvers
class Optimization_NonlinEqualityConstrainedTest : public::testing::Test {
public:

  // the initial point to start the optimization
  Eigen::VectorXd x0;

  // a base ptree, swap the Opt.Method entry for testing different algorithms
  boost::property_tree::ptree testParams;

  // an instance of the quadratic optimization problem
  std::shared_ptr<muq::Optimization::OptProbBase> prob;
  std::shared_ptr<muq::Optimization::ConstraintBase> NonLin1;

  // constructor
  Optimization_NonlinEqualityConstrainedTest()
  {
    // set the initial condition
    x0 = Eigen::VectorXd::Ones(2);
    x0 << 0.4, 0.8;

    // set some of the optimization parameters
    testParams.put("Opt.MaxIts", 10000);
    testParams.put("Opt.ftol", 1e-11);
    testParams.put("Opt.xtol", 1e-13);
    testParams.put("Opt.gtol", 1e-10);
    testParams.put("Opt.ctol", 1e-10);
    testParams.put("Opt.LineSearch.LineIts", 100);
    testParams.put("Opt.AugLag.maxOuter", 1000);
    testParams.put("Opt.StepLength", 1);
    testParams.put("Opt.verbosity", 0);

    testParams.put("Opt.NLOPT.MaxEvals", 100000);
    testParams.put("Opt.NLOPT.xtol_rel", 1e-12);
    testParams.put("Opt.NLOPT.xtol_abs", 1e-12);
    testParams.put("Opt.NLOPT.ftol_rel", 1e-13);
    testParams.put("Opt.NLOPT.ftol_abs", 1e-13);

    // create an instance of the optimization problem
    prob = std::make_shared<RoseFunc>();

    // add a couple bound constraints
    NonLin1 = std::make_shared<NonlinConstraint>();
    std::shared_ptr<muq::Optimization::ConstraintBase> NonLin2 = std::make_shared<NonlinConstraint>();
    prob->AddConstraint(NonLin2, true);
  }

  // the tear-down function is called just before the constructor, and in our case is where the tests are actually
  // performed
  void TearDown()
  {
    // set up the solver
    auto Solver = muq::Optimization::OptAlgBase::Create(prob, testParams);

    // solve the optimization problem
    Eigen::VectorXd xOpt = Solver->solve(x0);

    int optStat = Solver->GetStatus();

    // we expect a successful optimization
    EXPECT_GT(optStat, 0);

    // make sure the returned optimum is what we expect
    EXPECT_NEAR(xOpt[0], 0.91156412762192729, 1e-2);
    EXPECT_NEAR(xOpt[1], 0.8306149688528918,  1e-2);

    // make sure the constraint is satisfied
    Eigen::VectorXd cVals = NonLin1->eval(xOpt);
    EXPECT_NEAR(0, cVals[0], 1e-2);
  }
};


// set up a gtest test fixture for unconstrained deterministic solvers
class Optimization_NonlinInequalityConstrainedTest : public::testing::Test {
public:

  // the initial point to start the optimization
  Eigen::VectorXd x0;

  // a base ptree, swap the Opt.Method entry for testing different algorithms
  boost::property_tree::ptree testParams;

  // an instance of the quadratic optimization problem
  std::shared_ptr<muq::Optimization::OptProbBase> prob;
  std::shared_ptr<muq::Optimization::ConstraintBase> NonLin1;

  // constructor
  Optimization_NonlinInequalityConstrainedTest()
  {
    // set the initial condition
    x0 = Eigen::VectorXd::Ones(2);
    x0 << 0.4, 0.8;

    // set some of the optimization parameters
    testParams.put("Opt.MaxIts", 10000);
    testParams.put("Opt.ftol", 1e-11);
    testParams.put("Opt.xtol", 1e-13);
    testParams.put("Opt.gtol", 1e-10);
    testParams.put("Opt.ctol", 1e-10);
    testParams.put("Opt.LineSearch.LineIts", 100);
    testParams.put("Opt.AugLag.maxOuter", 1000);
    testParams.put("Opt.StepLength", 1);
    testParams.put("Opt.verbosity", 0);

    testParams.put("Opt.NLOPT.MaxEvals", 100000);
    testParams.put("Opt.NLOPT.xtol_rel", 1e-12);
    testParams.put("Opt.NLOPT.xtol_abs", 1e-12);
    testParams.put("Opt.NLOPT.ftol_rel", 1e-13);
    testParams.put("Opt.NLOPT.ftol_abs", 1e-13);

    // create an instance of the optimization problem
    prob = std::make_shared<RoseFunc>();

    // add a couple bound constraints
    NonLin1 = std::make_shared<NonlinConstraint>();
    std::shared_ptr<muq::Optimization::ConstraintBase> NonLin2 = std::make_shared<NonlinConstraint>();
    prob->AddConstraint(NonLin2, true);
  }

  // the tear-down function is called just before the constructor, and in our case is where the tests are actually
  // performed
  void TearDown()
  {
    // set up the solver
    auto Solver = muq::Optimization::OptAlgBase::Create(prob, testParams);

    // solve the optimization problem
    Eigen::VectorXd xOpt = Solver->solve(x0);

    int optStat = Solver->GetStatus();

    // we expect a successful optimization
    EXPECT_GT(optStat, 0);

    // make sure the returned optimum is what we expect
    EXPECT_NEAR(xOpt[0], 0.91156412762192729, 1e-2);
    EXPECT_NEAR(xOpt[1], 0.8306149688528918,  1e-2);

    // make sure the constraint is satisfied
    Eigen::VectorXd cVals = NonLin1->eval(xOpt);
    EXPECT_LT(cVals[0], 1e-5);
  }
};


#endif // ifndef _ConstrainedTestClasses_h
