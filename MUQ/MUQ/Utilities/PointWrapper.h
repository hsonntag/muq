
#ifndef QUADRATUREPOINTWRAPPER_H_
#define QUADRATUREPOINTWRAPPER_H_

#include <assert.h>

#include <boost/serialization/access.hpp>

#include <Eigen/Core>

namespace muq {
namespace Utilities {
/**
 * A simple class for wrapping a point. Generally not for user code,
 * specifically created so we can create a std::map that uses vectors as
 * the key.
 */
class PointWrapper {
public:

  PointWrapper();
  PointWrapper(Eigen::VectorXd const& point);
  virtual ~PointWrapper();

  Eigen::VectorXd point;

  ///Return if two multiindices are equal, used for std::map
  bool operator==(const PointWrapper& b) const;

  bool operator<(const PointWrapper& b) const;

protected:

  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive& ar, const unsigned int version);
};

/**
 * A comparison structure used to initialize std::map to hold multi-indices.
 */
struct PointComp {
  bool operator()(const PointWrapper& lhs, const PointWrapper& rhs) const
  {
    return lhs < rhs;
  }

  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive& ar, const unsigned int version);
};
} //namespace Utilities
} //namespace muq
#endif /* QUADRATUREPOINTWRAPPER_H_ */
