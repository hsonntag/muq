
#ifndef HilbertTransform_h_
#define HilbertTransform_h_

#include <Eigen/Core>

namespace muq{
namespace Utilities{
    /** @class HilbertTransform
	    @ingroup Utilities
	    @brief Computes the Hilbert transform and Analytic signal of a real valued signal.
	*/
	class HilbertTransform
	{
	
	public:
	
	    /** 
		 * Compute the Hilbert transform from the input signal.  This function copies the Signal and uses an FFT to compute the Hilbert transform.
		   @param[in] inputSignal The real-valued signal of length N that we want to transform.
	     */
	    void Compute(const Eigen::VectorXd &inputSignal);
	
	    /** 
		 * This function returns the analytic signal of the original signal.  The analytic signal is a complex-valued vector where the real component
		 * is the original signal (the signal given to inputSignal) and the imaginary component is the Hilbert transform.  Also, if only the magnitude of the complex
		 * output is needed, the GetEnvelope() function will be faster.
		 * @return A vector of std::complex<double> containing the analytic signal.
		 */
		Eigen::VectorXcd GetAnalytic() const;
		
		/**
		 * The magnitude of the components in the analytic signal gives the envelope of a signal.  This function returns the envelope.
		 * @return The evelope of the signal passed to the Compute function.
		 */
		Eigen::VectorXd GetEnvelope() const;
		
		/**
		 * This function returns the Hilbert transform of the original signal.  The Hilbert transform is the imaginary component of the analytic signal.
		 * @return The Hilbert transform of the signal.
		 */
		Eigen::VectorXd GetHilbert() const;
		
		/**
		 * This function simply returns a copy of the signal originally given to the Compute function.
		 */
		Eigen::VectorXd GetSignal() const;
		
	
	private:
		Eigen::VectorXcd analyticSig;
	
	};

		
}//namespace Utilities
}//namespace muq



#endif