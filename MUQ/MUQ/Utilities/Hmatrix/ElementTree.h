
#ifndef _ElementTree_h
#define _ElementTree_h

// stl headers
#include <memory>

// other external includes
#include <Eigen/Core>

// other muq-related includes
#include "MUQ/Utilities/mesh/Mesh.h"
#include "MUQ/Geostatistics/IsotropicCovKernel.h"

namespace muq {
namespace Utilities {
// define some of the other classes we will make friends with
template<unsigned int dim>
class ElementNode;

template<unsigned int dim>
class ElementPairNode;

template<unsigned int dim>
class HMatrix;

template<unsigned int dim>
class ElementTree {
public:

  // make the ElementNode class a friend
  friend class ElementNode<dim>;
  friend class ElementPairNode<dim>;
  friend class HMatrix<dim>;

  // construct the element tree from a mesh
  ElementTree(std::shared_ptr < Mesh < dim >>& MeshIn);

  // store the root node
  std::shared_ptr < ElementNode < dim >> root;


  // get the diameter of a node
  double GetDiam(std::shared_ptr < ElementNode < dim >> &Node) const;

  // get the distance between two nodes
  double GetDist(std::shared_ptr < ElementNode < dim >> &Node1, std::shared_ptr < ElementNode < dim >> &Node2) const;

  // recurse through the element tree and find the element containing this point.
  unsigned int GetElement(const Eigen::Matrix<double, dim, 1>& pos);

private:

  // store the elements refered to by the ElementNode class
  std::vector<unsigned int> Elements;

  // store the matrix of element positions
  Eigen::Matrix<double, dim, Eigen::Dynamic> ElePos;

  void DecompEleList(std::shared_ptr < ElementNode < dim >> &Parent);
};
}
}
#endif // ifndef _ElementTree_h
