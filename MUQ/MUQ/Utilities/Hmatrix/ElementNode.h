
#ifndef _ElementNode_h
#define _ElementNode_h

// stl includes
#include <memory>
#include "MUQ/Utilities/Hmatrix/ConvexHull.h"

namespace muq {
namespace Utilities {
template<unsigned int dim>
class ElementNode {
public:

  ElementNode() : child1(nullptr), child2(nullptr) {}

  ElementNode(unsigned int s, unsigned int e) : StartInd(s), EndInd(e), child1(nullptr), child2(nullptr) {}

  // build the convex hull
  void SetConvexHull(const Eigen::Matrix<double, dim,
                                         Eigen::Dynamic>& ElePos, const std::vector<unsigned int>& Elements);

  unsigned int StartInd;
  unsigned int EndInd;

  std::shared_ptr < ElementNode < dim >> child1;
  std::shared_ptr < ElementNode < dim >> child2;

  // hold onto a convex hull of the points so we can quickly and easily compute distances and/or diameters
  ConvexHull<dim> CHull;

  // keep the dominant direction used to decompose this ElementNode into child1 and child2
  Eigen::Matrix<double, dim, 1> EigVec;

  // hold onto the mean position of this element node for later searching
  Eigen::Matrix<double, dim, 1> MeanPos;
};
} // namespace Utilities
} //namespace muq
#endif // ifndef _ElementNode_h
