
#ifndef LOGCONFIG_H_
#define LOGCONFIG_H_

#ifdef MUQ_GLOG
# include <glog/logging.h>
#else // ifdef MUQ_GLOG
# include <iosfwd>
struct nullstream {};

// the nullstream struct will just eat everything and perform nothing which means any use should get removed by compiler
template<typename T>
nullstream& operator<<(nullstream& s, T const&)
{
  return s;
} // constant reference template

nullstream& operator<<(nullstream& s, std::ostream& (std::ostream &));

static nullstream logstream;

# define LOG(severity) logstream

#endif // ifdef MUQ_GLOG

namespace muq {
namespace Utilities {
/**
 * This file provides an easy wrapper around the Google logging framework. The code here
 * is designed around boost.log, but as that library is not actually supported,
 * it is not suitable for distribution. So, without altering the code elsewhere,
 * we've changed the definitions here to call the google library instead.
 *
 * The primary interface from before is the macro: BOOST_LOG_POLY(channel, severity), for example:
 * BOOST_LOG_POLY("pce",debug) << "your message" << printable object;
 *
 * The primary google call is  LOG(INFO) << message. All the other calls are transformed into
 * these with a #def.
 *
 * Call executables with flags to determine how much to log and whether to show
 * it in stderr. For example, --logtostderr=1 See the google docs.
 * http://google-glog.googlecode.com/svn/trunk/doc/glog.html
 */


///This enum defines the levels of the debug messages. Legacy.
enum severity_level {
  fntrace,     //Very detailed tracing in and out of functions
  debug,       //A standard set of useful debug messages
  performance, //Maybe useful, a limited set that prints more detailed values to tell how the code is doing
  //E.g. error indicators as an algorithm iterates.
  normal,      //Basic reporting on what the code is doing. Shouldn't be a ton of this.
  warning,     //Something unexpected has happened.
  error,       //Something bad has happened.
  critical     //Something very bad has happened. Probably going to crash or exit.
};


/**
 * Legacy macro to rewrite logging.
 */
    #define BOOST_LOG_POLY(chan, lvl) \
  LOG(INFO)

/**
 * Boilerplate code, goes in main functions to initialize the logging code. Will exit if
 * logging initialization is unsuccessful.
 */
void InitializeLogging(int argc, char **argv);
}
}

#endif /* LOGCONFIG_H_ */
