
#ifndef _IndexType_h
#define _IndexType_h

#include <list>
#include <vector>
#include <set>

namespace muq {
namespace Utilities {
typedef std::list<int> TreeIndexContainer;
}
}

#endif // ifndef _IndexType_h
