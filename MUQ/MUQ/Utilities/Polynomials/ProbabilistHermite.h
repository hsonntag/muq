#ifndef PROBABILISTHERMITE_H_
#define PROBABILISTHERMITE_H_


#include <boost/serialization/access.hpp>

#include "MUQ/Utilities/Polynomials/RecursivePolynomialFamily1D.h"

namespace muq {
namespace Utilities {
  
/**
 * @class ProbabilistHermite
 * @ingroup Polynomials1D
 * @brief Implements the Probabilist Hermite Polynomials.
 * @details 
 * Subclasses the numerically stable RecursivePolynomialFamily1D with the three-term
 * recurrence necessary to generate the Hermite polynomials. Uses the probabilist's
 * version of the Hermite, from http://en.wikipedia.org/wiki/Hermite_polynomials
 **/
class ProbabilistHermite : public Static1DPolynomial<ProbabilistHermite> {
public:

  friend class Static1DPolynomial<ProbabilistHermite>;
  
  typedef std::shared_ptr<ProbabilistHermite> Ptr;

  ProbabilistHermite() = default;
  virtual ~ProbabilistHermite() = default;

  ///Implements normalization.
  static double  GetNormalization_static(unsigned int n);

  static Eigen::VectorXd GetMonomialCoeffs(const int order);
  
  static double gradient_static(unsigned int order, double x);
  static double secondDerivative_static(unsigned int order, double x);
  
  
private:

  ///Grant access to serialization
  friend class boost::serialization::access;

  ///Serialization method
  template<class Archive>
  void   serialize(Archive& ar, const unsigned int version);

  //implement methods defining recurrence
  static double alpha(double x, unsigned int k);
  static double beta(double x, unsigned int k);
  static double phi0(double x);
  static double phi1(double x);
};
}
}

BOOST_CLASS_EXPORT_KEY(muq::Utilities::Static1DPolynomial<muq::Utilities::ProbabilistHermite>)
BOOST_CLASS_EXPORT_KEY(muq::Utilities::ProbabilistHermite)

#endif /* HERMITEPOLYNOMIALS1DRECURSIVE_H_ */
