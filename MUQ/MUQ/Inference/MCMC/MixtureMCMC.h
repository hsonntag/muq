/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *  USA.
 *
 *  MIT UQ Library
 *  Copyright (C) 2013 MIT
 */

#ifndef MixtureMCMC_H_
#define MixtureMCMC_H_

#include "MUQ/Inference/MCMC/SingleChainMCMC.h"
#include "MUQ/Modelling/GaussianPair.h"


namespace muq {
namespace Inference {

class MixtureMCMC : public SingleChainMCMC {
public:

  MixtureMCMC(std::shared_ptr<AbstractSamplingProblem> inferenceProblem, boost::property_tree::ptree& properties);


  virtual void setLocalWeight(double w1);


  /** @brief Take a step.
   *             Take a step using the invariant propsoal and Metropolis-Hastings rule.
   */
  virtual muq::Inference::SingleChainMCMC::ProposalResult MakeProposal() override;

  REGISTER_MCMC_CONSTRUCTOR(MixtureMCMC)

protected:

	std::shared_ptr<muq::Modelling::GaussianPair> localProp, globalProp;
	double weight;

private:

  REGISTER_MCMC_DEC_TYPE(MixtureMCMC)
};
} // namespace Inference
} // namespace muq

#endif /* MixtureMCMC_H_ */
