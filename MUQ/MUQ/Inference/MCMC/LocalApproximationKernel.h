#ifndef LOCALAPPROXIMATIONKERNEL_H
#define LOCALAPPROXIMATIONKERNEL_H

#include "MUQ/Inference/MCMC/MCMCKernel.h"
#include "MUQ/Inference/MCMC/MCMCProposal.h"
#include "MUQ/Approximation/Incremental/PolynomialApproximator.h"

namespace muq {
namespace Inference {
class LocalApproximationKernel : public muq::Inference::MCMCKernel {
public:

  LocalApproximationKernel(std::shared_ptr<muq::Inference::AbstractSamplingProblem> density,
                               boost::property_tree::ptree                            & properties);

  virtual ~LocalApproximationKernel() = default;

  void SetApproximateModel(std::shared_ptr<muq::Approximation::Approximator> approximateModPieceIn)
  {
    approximateModPiece = approximateModPieceIn;
  }

  virtual void                                       SetupStartState(const Eigen::VectorXd& xStart) override;

  virtual std::shared_ptr<muq::Inference::MCMCState> ConstructNextState(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    int const                                     iteration,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) override;

  /** Get the acceptance rate.
   *             @return The acceptance rate in [0,1]
   */
  virtual double GetAccept() const;

  virtual void   PrintStatus() const override;

private:

  /**
   * Return gamma, not alpha, that is, don't take alpha=min(1,gamma).
   *
   * If the "recompute" flags are true, compute the states again before computing the probability. This is helpful
   * for cross validation.
   */
  virtual double ComputeAcceptanceProbability(std::shared_ptr<muq::Inference::MCMCState> currentStateIn,
                                              std::shared_ptr<muq::Inference::MCMCState> proposedStateIn,
                                              bool                                       recomputeCurrent,
                                              bool                                       recomputeProposed);

  std::shared_ptr<muq::Approximation::Approximator> approximateModPiece;

  std::shared_ptr<muq::Inference::MCMCProposal> proposal;

  double baseBeta              = .01;
  double baseGamma             = 0.1;
  double betaExponent          = -.2;
  double gammaExponent         = -.1;
  int    maxRefinementsPerEval = 3;
  bool   referenceRun          = false; //turn off nearest neighbors, running in the direct mode
  double initialSampleRadius   = 0;
  
  /// keep track of the number of accepted steps
  int numAccepts = 0;

  /// keep track of the total number of times ConstructNextState was called
  int totalSteps = 0;
};
}
}



#endif // LOCALAPPROXIMATIONKERNEL_H
