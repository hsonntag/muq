/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *  USA.
 *
 *  MIT UQ Library
 *  Copyright (C) 2013 MIT
 */

#ifndef IMH_H_
#define IMH_H_

#include "MUQ/Inference/MCMC/SingleChainMCMC.h"
#include "MUQ/Modelling/DensityRVPair.h"
#include "MUQ/Modelling/Density.h"
#include "MUQ/Modelling/RandVar.h"

namespace muq {
namespace Inference {
/**
 *         \class IMH
 *         \author Matthew Parno
 *         \brief Simple Independent Metropolis-Hastings (IMH) algorithm
 *         This class implements a basic Independence Metropolis style MCMC algorithms using an arbitrary proposal distribution.
 */
class IMH : public SingleChainMCMC {
public:

  /** Read in parameters a from parsed xml document.  This constructor assumes the proposal will be an isotropic Gaussian distribution.  For
   * more sophisticated proposals, see the other constructor.
   * @param[in] inferenceProblem The sampling problem to be solved with this MCMC algorithm.
   * @param[in] paramList Parameter list containing the MCMC parameters such as number of steps, burnin, and
   * isotropic Gaussian variance.  
   */
  IMH(std::shared_ptr<AbstractSamplingProblem> inferenceProblem, boost::property_tree::ptree& properties);
  
  
 /** Read in parameters a from parsed xml document.  This constructor allows for arbitrary independence distributions defined by
  *  the DensityRVPair input.
  * @param[in] inferenceProblem The sampling problem to be solved with this MCMC algorithm.
  * @param[in] paramList Parameter list containing the MCMC parameters such as number of steps, burnin, and
  * isotropic Gaussian variance.  
  * @param[in] distPair A density and random variable that can be used to sample and evaluate the proposal distribution.
  */
 template<typename rvType, typename densityType, typename specificationType>
  IMH(std::shared_ptr<AbstractSamplingProblem> inferenceProblem, boost::property_tree::ptree& properties,
     std::shared_ptr<muq::Modelling::DensityRVPair<rvType, densityType, specificationType>> distPair) : SingleChainMCMC(inferenceProblem,properties),
	 																									 proposalDensity(distPair->density),
																										 proposalRV(distPair->rv)
  {
     assert(proposalDensity->inputSizes.size()==1);
	 assert(proposalDensity->inputSizes(0)==samplingProblem->samplingDim);
	 assert(proposalRV->outputSize==samplingProblem->samplingDim);
	 assert(proposalRV->inputSizes.size()==0);		
	 
	 samplingProblem->SetStateComputations(false, false, false);																						 	
  };


  /** Set the independent proposal.
   *  @param[in] newDistPair Smart pointer (shared_ptr or the like) to a DensityRVPair that defines the independent proposal.
   **/
  template<typename rvType, typename densityType, typename specificationType>
  void setProposal(std::shared_ptr<muq::Modelling::DensityRVPair<rvType, densityType, specificationType>> newDistPair){
      
     assert(newDistPair->density->inputSizes.size()==1);
 	 assert(newDistPair->density->inputSizes(0)==samplingProblem->samplingDim);
 	 assert(newDistPair->rv->outputSize==samplingProblem->samplingDim);
 	 assert(newDistPair->rv->inputSizes.size()==0);
	 
	 proposalDensity = newDistPair->density;
	 proposalRV = newDistPair->rv;
  };


  /** @brief Take a step.
   *  Take a step using the independent proposal and a Metropolis-Hastings correction.
   */
  virtual muq::Inference::SingleChainMCMC::ProposalResult MakeProposal() override;

  REGISTER_MCMC_CONSTRUCTOR(IMH)

protected:

  /// pointer to proposal
  std::shared_ptr<muq::Modelling::Density> proposalDensity;
  std::shared_ptr<muq::Modelling::RandVar> proposalRV;

private:

  REGISTER_MCMC_DEC_TYPE(IMH)
};
} // namespace Inference
} // namespace muq

#endif /* IMH_H_ */
