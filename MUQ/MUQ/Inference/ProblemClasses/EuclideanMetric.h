
#ifndef EUCLIDEANMETRIC_H
#define EUCLIDEANMETRIC_H

#include "MUQ/Inference/ProblemClasses/AbstractMetric.h"

namespace muq {
namespace Inference {
class EuclideanMetric : public AbstractMetric {
public:

  EuclideanMetric() : isId(true) {}

  EuclideanMetric(const Eigen::MatrixXd& met) : isId(false), metricMat(met) {}

  virtual ~EuclideanMetric() = default;

  inline virtual Eigen::MatrixXd ComputeMetric(Eigen::VectorXd const& point) override
  {
    return isId ? Eigen::MatrixXd::Identity(point.rows(), point.rows()) : metricMat;
  }

protected:

  const bool isId;
  const Eigen::MatrixXd metricMat;
};
}
}

#endif // EUCLIDEANMETRIC_H
