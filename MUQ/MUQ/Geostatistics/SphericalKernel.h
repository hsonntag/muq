
#ifndef _SphericalKernel_h
#define _SphericalKernel_h

#include "MUQ/Geostatistics/IsotropicCovKernel.h"

namespace muq {
namespace Geostatistics {
/**
 *  @class SphericalKernel
 *
 *  @brief This is a basic class for building covariance matrices from a spherical covariance kernel.
 *
 *  This class implements covariance kernels of the form:
 *  /f[
 *  cov(d) = \sigma\left[1-\frac{2}{\pi}\left(\frac{d}{L}\sqrt{1-\left(\frac{d}{L}\right)^2 +
 * \sin^{-1}\frac{d}{L}\right)
 *     \right]
 *  /f]
 *
 *  @author Matthew Parno
 *
 */
class SphericalKernel : public IsotropicCovKernel {
public:

  /** Construct the kernel from the characteristic length scale and variance.
   *  @param[in] LengthIn characteristic length scale
   *  @param[in] VarIn variance - i.e. diagonal of covariance matrix
   */
  SphericalKernel(double LengthIn, double VarIn);

  virtual ~SphericalKernel() = default;

  /** This function evaluates the spherical kernel.
   *  @param[in] d Distance between two points
   *  @return covariance of random field between two points at a distance d
   */
  virtual double          DistKernel(double d) const;

  /** Set the covariance kernel parameters from a vector of parameters.
   *  @param[in] theta MuqVector or parameter values for the covariance kernel.
   */
  virtual void            SetParms(const Eigen::VectorXd& theta);

  virtual void            GetGrad(double d, Eigen::VectorXd& grad) const override;

  virtual Eigen::VectorXd GetParms()
  {
    Eigen::VectorXd Out(2); Out(0) = Length; Out(1) = Var; return Out;
  }

protected:

  double Length;
  double Var;
};

// class CovKernel
} // namespace Geostatistics
} //namespace muq


#endif // ifndef _SphericalKernel_h
