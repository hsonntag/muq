
#ifndef _MaternKernel_h
#define _MaternKernel_h

#include "MUQ/Geostatistics/IsotropicCovKernel.h"

namespace muq {
namespace Geostatistics {
/**
 *  @class MaternKernel
 *
 *  @brief This is a basic class for building covariance matrices from a Matern covariance kernel.
 *
 *  This class implements covariance kernels of the form:
 *  /f[
 *  cov(d) = \frac{2\sigma}{\Gamma(\nu)}\left(\frac{d}{2\rho}\right)^\nu K_\nu\left(\frac{d}{\rho}\right)
 *  /f]
 *
 *  @author Matthew Parno
 *
 */
class MaternKernel : public IsotropicCovKernel {
public:

  /** Construct the kernel from the matern kernel parameters.
   *  @param[in] Rho characteristic length scale
   *  @param[in] Nu related to how "smooth" the kernel is
   *  @param[in] VarIn variance - i.e. diagonal of covariance matrix
   */
  MaternKernel(double RhoIn, double NuIn, double VarIn);

  virtual ~MaternKernel() = default;

  /** This function evaluates the matern kernel.
   *  @param[in] d Distance between two points
   *  @return covariance of random field between two points at a distance d
   */
  virtual double          DistKernel(double d) const;

  /** Set the covariance kernel parameters from a vector of parameters.
   *  @param[in] theta MuqVector or parameter values for the covariance kernel.
   */
  virtual void            SetParms(const Eigen::VectorXd& theta);

  virtual void            GetGrad(double d, Eigen::VectorXd& grad) const override;

  virtual Eigen::VectorXd GetParms()
  {
    Eigen::VectorXd Output(3); Output[0] = rho; Output[1] = nu; Output[2] = var; return Output;
  }

protected:

  double rho;
  double nu;
  double var;
};

// class CovKernel
} // namespace Geostatistics
} // namespace muq

#endif // ifndef _MaternKernel_h
