
#ifndef KERNELMODELPYTHON_H_
#define KERNELMODELPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Pde/KernelModel.h"

namespace muq {
namespace Pde {
/// Python wrapper around muq::Pde::KernelModel
class KernelModelPython : public KernelModel, public boost::python::wrapper<KernelModel> {
public:

  KernelModelPython(boost::python::list const                      & pts,
                    unsigned int                                     numBasis,
                    std::shared_ptr<muq::Geostatistics::CovKernel> const& kernel);

  KernelModelPython(boost::python::list const                      & pts,
                    unsigned int                                     numBasis,
                    boost::python::list const                      & mean,
                    std::shared_ptr<muq::Geostatistics::CovKernel> const& kernel);

  /// Python wrapper around method to get the covariacne
  boost::python::list PyGetCovariance() const;
};

inline void           ExportKernelModel()
{
  boost::python::class_<KernelModelPython, std::shared_ptr<KernelModelPython>,
                        boost::python::bases<muq::Modelling::LinearModel>, boost::noncopyable> exportKernelModel(
    "KernelModel",
    boost::python::init<boost::python::list const&, unsigned int,
                        std::shared_ptr<muq::Geostatistics::CovKernel> const&>());

  exportKernelModel.def(boost::python::init<boost::python::list const&, unsigned int,
                                            boost::python::list const&,
                                            std::shared_ptr<muq::Geostatistics::CovKernel> const&>());

  exportKernelModel.def("GetCovariance", &KernelModelPython::PyGetCovariance);

  boost::python::implicitly_convertible<std::shared_ptr<KernelModelPython>,
                                        std::shared_ptr<muq::Modelling::ModPiece> >();
}
} // namespace Pde
} // namespace muq

#endif // ifndef KERNELMODELPYTHON_H_
