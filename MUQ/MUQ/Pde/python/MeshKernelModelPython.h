#ifndef MESHKERNELMODELPYTHON_H_
#define MESHKERNELMODELPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Pde/MeshKernelModel.h"

namespace muq {
namespace Pde {
/// Python wrapper for muq::Pde::MeshKernelModel
class MeshKernelModelPython : public MeshKernelModel, public boost::python::wrapper<MeshKernelModel> {
public:

  MeshKernelModelPython(std::string const                                   & paraName,
                        std::shared_ptr<libMesh::EquationSystems> const     & parasystem,
                        std::vector<Eigen::VectorXd> const                  & pts,
                        unsigned int const                                    numBasis,
                        std::shared_ptr<muq::Geostatistics::CovKernel> const& kernel);

  MeshKernelModelPython(std::string const                                   & paraName,
                        std::shared_ptr<libMesh::EquationSystems> const     & parasystem,
                        std::vector<Eigen::VectorXd> const                  & pts,
                        unsigned int const                                    numBasis,
                        Eigen::VectorXd const                               & mean,
                        std::shared_ptr<muq::Geostatistics::CovKernel> const& kernel);

  /// Construct a mesh kernel model with zero mean
  /**
   *  @param[in] parasystem The equation system holding the mesh on which the parameter is defined
   *  @param[in] kernel The covariance kernel
   * @param[in] dict A dictionary containing all the options
   */
  static std::shared_ptr<MeshKernelModelPython> ConstructZeroMean(
    std::shared_ptr<muq::Pde::GenericEquationSystems> const& parasystem,
    std::shared_ptr<muq::Geostatistics::CovKernel> const   & kernel,
    boost::python::dict const                              & dict);

  /// Construct a mesh kernel model with nonzero mean
  /**
   * @param[in] parasystem The equation system holding the mesh on which the parameter is defined
   * @param[in] kernel The covariance kernel
   * @param[in] mean The mean
   * @param[in] dict A dictionary containing all the options
   */
  static std::shared_ptr<MeshKernelModelPython> ConstructNonzeroMean(
    std::shared_ptr<muq::Pde::GenericEquationSystems> const& parasystem,
    std::shared_ptr<muq::Geostatistics::CovKernel> const   & kernel,
    boost::python::list const                              & mean,
    boost::python::dict const                              & dict);

private:
};

/// Export muq::Pde::MeshKernelModel to python
void ExportMeshKernelModel();
} // namespace Pde
} // namespace muq

#endif // ifndef MESHKERNELMODELPYTHON_H_
