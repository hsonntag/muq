
#ifndef KERNELMODEL_H_
#define KERNELMODEL_H_

#include "libmesh/dof_map.h"

#include "MUQ/Geostatistics/CovKernel.h"

#include "MUQ/Modelling/LinearModel.h"

#include "MUQ/Pde/GenericEquationSystems.h"

namespace muq {
namespace Pde {
/// Model a parameter via its kernel evaluation
class KernelModel : public muq::Modelling::LinearModel {
public:

  /// Construct the model
  /**
   * @param[in] pts The points (e.g. from a mesh) where we want to evaluate the kernel
   * @param[in] numBasis The number of basises of the kernel KL decomp. to consider
   * @param[in] kernel The kernel
   */
  KernelModel(std::vector<Eigen::VectorXd> const             & pts,
              unsigned int                                     numBasis,
              std::shared_ptr<muq::Geostatistics::CovKernel> const& kernel);

  /// Construct the model
  /**
   * @param[in] pts The points (e.g. from a mesh) where we want to evaluate the kernel
   * @param[in] numBasis The number of basises of the kernel KL decomp. to consider
   * @param[in] mean The mean of the reconstructed parameter
   * @param[in] kernel The kernel
   */
  KernelModel(std::vector<Eigen::VectorXd> const             & pts,
              unsigned int                                     numBasis,
              Eigen::VectorXd const                          & mean,
              std::shared_ptr<muq::Geostatistics::CovKernel> const& kernel);

  /// Get the covariance matrix
  /**
   *  We have stored the matrix square root of the cov. matrix.  This function recovers the covariance matrix on the
   *points.
   */
  Eigen::MatrixXd GetCovariance() const;

  /// Update the mean of the model
  /**
   *  @param[in] mean The new mean
   */
  void UpdateMean(Eigen::VectorXd const& mean);

private:
};
} // namespace Pde
} // namespace muq

#endif // ifndef KERNELMODEL_H_
