#ifndef PDEMODPIECE_H_
#define PDEMODPIECE_H_

#include "MUQ/Modelling/OdeModPiece.h"
#include "MUQ/Modelling/SolverModPiece.h"
#include "MUQ/Modelling/IdentityObserver.h"

#include "MUQ/Pde/Nontransient.h"
#include "MUQ/Pde/Transient.h"
#include "MUQ/Pde/PointObserver.h"

namespace muq {
namespace Pde {
/// A class to solve both transient and nontransient partial differential equations
/**
   This is a templated class; the template is the parent.  In the case of a nontransient system the parent is muq::Modelling::SolverModPiece and in the case of a transient system the parent is muq::Modelling::OdeModPiece
 */
template<typename parent>
class PDEModPiece : public parent {
public:

  /// Constructor to create a nontransient system
  /**
   *  @param[in] pde A model to compute the residual of the system
   *  @param[in] sensor A model that takes the system result and computes a quantity of interest
   *  @param[in] param All the parameters for the system and the solver
   */
  PDEModPiece(std::shared_ptr<PDE> const                     & pde,
              std::shared_ptr<muq::Modelling::ModPiece> const& sensor,
              boost::property_tree::ptree                    & param) :
    parent(pde, sensor, param)
  {
    assert(pde->outputSize != 1);
  }

  /// Constructor to create a transient system
  /**
   *  @param[in] pde A model to compute the residual of the system
   *  @param[in] sensor A model that takes the system result and computes a quantity of interest
   *  @param[in] obsTimes The times when the system is observed
   *  @param[in] param All the parameters for the system and the solver
   */
  PDEModPiece(std::shared_ptr<PDE> const                 & pde,
              std::shared_ptr<muq::Modelling::ModPiece> const& sensor,
              Eigen::VectorXd const                          & obsTimes,
              boost::property_tree::ptree                    & param) :
    parent(pde, sensor, obsTimes, param) {}

    /// Print solution to an exodus file
    /**
     Save the solution in result to file.  Result should be a vector of length obsTimes.size()*outputSize, this prints each function as a timestep.
     @param[in] file The name of the file were this result will be saved
     @param[in] result The result of running PDE::Evaluate(inputs)
   */
    void PrintToExodus(std::string const& file, Eigen::VectorXd const& result) const
    {
    // get a PDE
    auto pde = std::dynamic_pointer_cast<PDE>(parent::f);

    assert(pde);

    // get the number of timesteps
    const unsigned int tSteps = result.size() / pde->outputSize;
    assert(tSteps > 0);

    // make and exodus object
    auto exodus = std::make_shared<libMesh::ExodusII_IO>(pde->GetEquationSystemsPtr()->get_mesh());

    for (unsigned int t = 0; t < tSteps; ++t) {
      // translate a timestep in result and save on mesh
      muq::Utilities::Translater<Eigen::VectorXd, std::vector<double> > trans(result.segment(pde->outputSize *t,
                                                                                                    pde->outputSize));
      pde->GetEquationSystemsPtr()->get_system(pde->GetName()).solution->operator=(*trans.GetPtr());

      exodus->write_timestep(file, *(pde->GetEquationSystemsPtr()), t + 1, parent::obsTimes(t));
    }
  }

#if MUQ_PYTHON == 1
    void PrintToExodusPy(std::string const& file, boost::python::list const& result) const
    {
      PrintToExodus(file, muq::Utilities::GetEigenVector<Eigen::VectorXd>(result));
    }
#endif // if MUQ_PYTHON == 1

#if MUQ_PYTHON == 1
  /// Python implementation of Evaluate
  virtual boost::python::list PyEvaluate(boost::python::list const& inputs)
  {
    std::vector<Eigen::VectorXd> eigenInputs = muq::Utilities::PythonListToVector(inputs);

    return muq::Utilities::GetPythonVector<Eigen::VectorXd>(parent::Evaluate(eigenInputs));
  }
  
  /// Allow the user to call the gradient function from python
  virtual boost::python::list PyGradient(boost::python::list const& pyInputs,
                                         boost::python::list const& pySensitivity,
                                         int const                  inputDimWrt = 0)
  {
    Eigen::VectorXd sensitivity = muq::Utilities::GetEigenVector<Eigen::VectorXd>(pySensitivity);

    std::vector<Eigen::VectorXd> inputs = muq::Utilities::PythonListToVector(pyInputs);
    assert((int)inputs.size() == parent::inputSizes.size());

    return muq::Utilities::GetPythonVector<Eigen::VectorXd>(parent::Gradient(inputs, sensitivity, inputDimWrt));
  }

  /// Allow the user to call the Jacobian function from python
  virtual boost::python::list PyJacobian(boost::python::list const& pyInputs, int const inputDimWrt = 0)
  {
    std::vector<Eigen::VectorXd> inputs = muq::Utilities::PythonListToVector(pyInputs);
    assert((int)inputs.size() == parent::inputSizes.size());

    return muq::Utilities::GetPythonMatrix(parent::Jacobian(inputs, inputDimWrt));
  }

  /// Allow the user to call the Jacobian action function from python
  virtual boost::python::list PyJacobianAction(boost::python::list const& pyInputs,
                                               boost::python::list const& pyTarget,
                                               int const                  inputDimWrt = 0)
  {
    std::vector<Eigen::VectorXd> inputs = muq::Utilities::PythonListToVector(pyInputs);
    assert((int)inputs.size() == parent::inputSizes.size());

    Eigen::VectorXd target = muq::Utilities::GetEigenVector<Eigen::VectorXd>(pyTarget);

    return muq::Utilities::GetPythonVector<Eigen::VectorXd>(parent::JacobianAction(inputs, target, inputDimWrt));
  }

  /// Allow the user to call the Hessian function from python
  virtual boost::python::list PyHessian(boost::python::list const& pyInputs,
                                        boost::python::list const& pySensitivity,
                                        int const                  inputDimWrt = 0)
  {
    Eigen::VectorXd sensitivity = muq::Utilities::GetEigenVector<Eigen::VectorXd>(pySensitivity);

    std::vector<Eigen::VectorXd> inputs = muq::Utilities::PythonListToVector(pyInputs);
    assert((int)inputs.size() == parent::inputSizes.size());

    return muq::Utilities::GetPythonMatrix(parent::Hessian(inputs, sensitivity, inputDimWrt));
  }

#endif // if MUQ_PYTHON == 1

private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override
  {
    // convert model to a pde type 
    auto pde = std::dynamic_pointer_cast<PDE>(parent::f);

    // make sure it worked 
    assert(pde);
    
    // store the inputs (does not require initial guess; don't inclue first input)
    pde->StoreInputs(std::vector<Eigen::VectorXd>(inputs.begin() + 1, inputs.end()));

    // non const inputs; the initial guess must be changed to match the Dirichlet BCs
    std::vector<Eigen::VectorXd> ins(inputs);

    // set the dirichlet conditions 
    ins[0] = pde->SetDirichletConditions(inputs[0]);
    
    // the rest of the inputs are the same 
    for (unsigned int i=1; i<inputs.size(); ++i) {
      ins[i] = inputs[i];
    }

    // call the Sundials solver (implemented in parent)
    return parent::EvaluateImpl(ins);
  }

  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int const inputDimWrt) override
  {
    // convert model to a pde type 
    auto pde = std::dynamic_pointer_cast<PDE>(parent::f);

    // make sure it worked 
    assert(pde);
    
    // store the inputs (does not require initial guess; don't inclue first input)
    pde->StoreInputs(std::vector<Eigen::VectorXd>(inputs.begin() + 1, inputs.end()));

    // non const inputs; the initial guess must be changed to match the Dirichlet BCs
    std::vector<Eigen::VectorXd> ins(inputs);

    // set the dirichlet conditions 
    ins[0] = pde->SetDirichletConditions(inputs[0]);
    
    // the rest of the inputs are the same 
    for (unsigned int i=1; i<inputs.size(); ++i) {
      ins[i] = inputs[i];
    }

    // call the Sundials solver (implemented in parent)
    return parent::JacobianImpl(ins, inputDimWrt);
  }

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& inputs, Eigen::VectorXd const& sens, int const inputDimWrt) override
  {
    // convert model to a pde type 
    auto pde = std::dynamic_pointer_cast<PDE>(parent::f);

    // make sure it worked 
    assert(pde);
    
    // store the inputs (does not require initial guess; don't inclue first input)
    pde->StoreInputs(std::vector<Eigen::VectorXd>(inputs.begin() + 1, inputs.end()));

    // non const inputs; the initial guess must be changed to match the Dirichlet BCs
    std::vector<Eigen::VectorXd> ins(inputs);

    // set the dirichlet conditions 
    ins[0] = pde->SetDirichletConditions(inputs[0]);
    
    // the rest of the inputs are the same 
    for (unsigned int i=1; i<inputs.size(); ++i) {
      ins[i] = inputs[i];
    }

    // call the Sundials solver (implemented in parent)
    return parent::GradientImpl(ins, sens, inputDimWrt);
  }

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& inputs, Eigen::VectorXd const& target, int const inputDimWrt) override
  {
    // convert model to a pde type 
    auto pde = std::dynamic_pointer_cast<PDE>(parent::f);

    // make sure it worked 
    assert(pde);
    
    // store the inputs (does not require initial guess; don't inclue first input)
    pde->StoreInputs(std::vector<Eigen::VectorXd>(inputs.begin() + 1, inputs.end()));

    // non const inputs; the initial guess must be changed to match the Dirichlet BCs
    std::vector<Eigen::VectorXd> ins(inputs);

    // set the dirichlet conditions 
    ins[0] = pde->SetDirichletConditions(inputs[0]);
    
    // the rest of the inputs are the same 
    for (unsigned int i=1; i<inputs.size(); ++i) {
      ins[i] = inputs[i];
    }

    // call the Sundials solver (implemented in parent)
    return parent::GradientImpl(ins, target, inputDimWrt);
  }

};

/// Nonmember function to construct nontransient versions of muq::Pde::PDEModPiece
/**
 *  Assumes an identity observer (muq::Modelling::IdentityObserver) on the system solution.
 *  @param[in] inputSizes The input sizes of the system parameters
 *  @param[in] system System hold the domain mesh
 *  @param[in] param All the parameters for the system and the solver
 *  \return A nontransient version of muq::Pde::PDEModPiece
 */
std::shared_ptr<PDEModPiece<muq::Modelling::SolverModPiece> > ConstructPDE(
  Eigen::VectorXi const                                  & inputSizes,
  std::shared_ptr<muq::Pde::GenericEquationSystems> const& system,
  boost::property_tree::ptree                            & param);

/// Nonmember function to construct nontransient versions of muq::PDE::PDEModPiece
/**
 *  Assumes a point observer (muq::GenericPDE::PointObserver) on the system solution.
 *  @param[in] inputSizes The input sizes of the system parameters
 *  @param[in] pnts The points in the domain to observe
 *  @param[in] system System hold the domain mesh
 *  @param[in] param All the parameters for the system and the solver
 *  \return A nontransient version of muq::Pde::PDEModPiece
 */
std::shared_ptr<PDEModPiece<muq::Modelling::SolverModPiece> > ConstructPDE(
  Eigen::VectorXi const                                  & inputSizes,
  std::vector<Eigen::VectorXd> const                     & pnts,
  std::shared_ptr<muq::Pde::GenericEquationSystems> const& system,
  boost::property_tree::ptree                            & param);

/// Nonmember function to construct transient versions of muq::Pde::PDEModPiece
/**
 *  Assumes an identity observer (muq::Modelling::IdentityObserver) on the system solution.
 *  @param[in] inputSizes The input sizes of the system parameters
 *  @param[in] obsTimes The times when the system is observed
 *  @param[in] system System hold the domain mesh
 *  @param[in] param All the parameters for the system and the solver
 *  \return A transient version of muq::Pde::PDEModPiece
 */
std::shared_ptr<PDEModPiece<muq::Modelling::OdeModPiece> > ConstructPDE(
  Eigen::VectorXi const                                  & inputSizes,
  Eigen::VectorXd const                                  & obsTimes,
  std::shared_ptr<muq::Pde::GenericEquationSystems> const& system,
  boost::property_tree::ptree                            & param);

/// Nonmember function to construct transient versions of muq::Pde::PDEModPiece
/**
 *  Assumes a point observer (muq::Generic::PointObserver) on the system solution.
 *  @param[in] inputSizes The input sizes of the system parameters
 *  @param[in] pnts The points in the domain to observe
 *  @param[in] obsTimes The times when the system is observed
 *  @param[in] system System hold the domain mesh
 *  @param[in] param All the parameters for the system and the solver
 *  \return A transient version of muq::Pde::PDEModPiece
 */
std::shared_ptr<PDEModPiece<muq::Modelling::OdeModPiece> > ConstructPDE(
  Eigen::VectorXi const                                  & inputSizes,
  std::vector<Eigen::VectorXd> const                     & pnts,
  Eigen::VectorXd const                                  & obsTimes,
  std::shared_ptr<muq::Pde::GenericEquationSystems> const& system,
  boost::property_tree::ptree                            & param);
} // namespace Pde
} // namespace muq

#endif // ifndef PDEMODPIECE_H_
