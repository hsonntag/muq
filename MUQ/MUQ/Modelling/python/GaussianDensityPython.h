#ifndef GAUSSIANDENSITYPYTHON_H_
#define GAUSSIANDENSITYPYTHON_H_

#include "MUQ/Modelling/GaussianDensity.h"

namespace muq {
  namespace Modelling {
    void ExportGaussianDensity();
  } // namespace Modelling
} // namespace muq

#endif // ifndef GAUSSIANDENSITYPYTHON_H_
