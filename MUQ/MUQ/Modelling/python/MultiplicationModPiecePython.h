#ifndef MULTIPLICATIONMODPIECEPYTHON_H_
#define MULTIPLICATIONMODPIECEPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/MultiplicationModPiece.h"

namespace muq {
namespace Modelling {
class MultiplicationModPiecePython : public MultiplicationModPiece,
                                     public boost::python::wrapper<MultiplicationModPiece> {
public:

  MultiplicationModPiecePython(boost::python::list const& inputDims);

private:
};
void ExportMultiplicationModPiece();
} // namespace Modelling
} // namespace muq


#endif // ifndef MULTIPLICATIONMODPIECEPYTHON_H_
