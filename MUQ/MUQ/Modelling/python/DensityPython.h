
#ifndef DENSITYPYTHON_H_
#define DENSITYPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/Density.h"

namespace muq {
namespace Modelling {
/// A python wrapper around muq::Modelling::Density
class DensityPython : public Density, public boost::python::wrapper<Density> {
public:

  DensityPython(Eigen::VectorXi const& inputSizes,
                bool const             hasDirectGradient,
                bool const             hasDirectJacobian,
                bool const             hasDirectJacobianAction,
                bool const             hasDirectHessian);

  /// User implected (in python) version of LogDensityImpl
  double PyLogDensityImpl(boost::python::list const& inputs);

private:

  /// Tell LogDensity where to look for python implementation
  virtual double LogDensityImpl(std::vector<Eigen::VectorXd> const& inputs) override;
};

/// Tell python about Density
void ExportDensity();
} // namespace modelling
} // namespace muq

#endif // ifndef DENSITYPYTHON_H_
