#ifndef ODEMODPIECEPYTHON_H_
#define ODEMODPIECEPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Modelling/OdeModPiece.h"

namespace muq {
namespace Modelling {
  
/// Python wrapper around OdeModPiece
class OdeModPiecePython : public OdeModPiece, public boost::python::wrapper<OdeModPiece> {
public:

  OdeModPiecePython(std::shared_ptr<ModPiece> const&    fIn,
                    std::shared_ptr<ModPiece> const&    gIn,
                    boost::python::list const&   evalTimes,
                    boost::python::dict const&   dict);
  
  OdeModPiecePython(std::shared_ptr<ModPiece> const&   fIn,
                    std::shared_ptr<ModPiece> const&   gIn,
                    boost::python::list const&   evalTimes);
                    
  OdeModPiecePython(std::shared_ptr<ModPiece> const&   fIn,
                    boost::python::list const&   evalTimes,
                    boost::python::dict const&   dict);
  
  OdeModPiecePython(std::shared_ptr<ModPiece> const&   fIn,
                    boost::python::list const&   evalTimes);

};

/// Expose muq::Modelling::OdeModPiece to python
void ExportOdeModPiece();

} // namespace muq
} // namespace inference

#endif // ifndef MCMCBASEPYTHON_H_
