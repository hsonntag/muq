
#ifndef _SecExpr_h
#define _SecExpr_h


#include "MUQ/Modelling/AnalyticFunctions/ComponentwiseModel.h"
#include "MUQ/Modelling/ModGraph.h"

namespace muq {
namespace Modelling {
/**
 *  @class SecModel
 *  @ingroup Modelling
 *  @brief Implemenation of componentwise Sec
 *
 */
class SecModel : public ComponentwiseModel {
public:

  SecModel(int dim) : ComponentwiseModel(dim) {}

  virtual ~SecModel() = default;
  
  /** Apply the base symbolic function
   *  @param[in] input  the value to be altered
   *  @return f(x) where f is a simple analytic functions such as exp(x), sin(x), etc...
   */
  virtual double BaseFunc(const double& x) const
  {
    return 1/cos(x);
  }

  /** return the derivative of the base symbolic function evaluated at point
   *  @param[in] input  where we want to evaluate the derivative
   *  @return df/dx the derivative of the analytic function at the input point, examples are exp(x), cos(x), etc...
   */
  virtual double BaseDeriv(const double& x) const
  {
    return sin(x)/pow(cos(x),2.0);
  }
  
   /** return the second derivative of the base symbolic function evaluated at point
   *   @param[in] input  where we want to evaluate the second derivative
   *   @return df/dx the derivative of the analytic function at the input point, examples are exp(x), cos(x), etc...
   */
  virtual double BaseSecondDeriv(const double& x) const
  {
    return pow(1.0/cos(x),3.0)+pow(sin(x)/cos(x),2.0)/cos(x);

   }
};

///Create direct operator-like calls
inline std::shared_ptr<ModGraph> Sec(const std::shared_ptr<ModGraph>& x)
{
  //copy the graph
  auto newGraph = x->DeepCopy();
  
  //Find out the size, asserting that the graph only has one output
  auto outputSize = newGraph->outputSize();
  
  //Create the new model
  auto newComponentwiseNode = std::make_shared<SecModel>(outputSize);

  //add it to the graph
  newGraph->AddNode(newComponentwiseNode, newComponentwiseNode->GetName());
  //connect the nodes
  newGraph->AddEdge(x->GetOutputNodeName(), newComponentwiseNode->GetName(), 0);

  return newGraph;
}

inline std::shared_ptr<ModGraph> Sec(const std::shared_ptr<ModPiece>& x)
{
  return Sec(std::make_shared<ModGraph>(x));
}
}
} // namespace muq


#endif // ifndef _SecExpr_h
