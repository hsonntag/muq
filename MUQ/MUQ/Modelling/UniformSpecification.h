
#ifndef UNIFORMSPECIFICATION_H
#define UNIFORMSPECIFICATION_H

#include <functional>
#include <limits>
#include <Eigen/Dense>


namespace muq {
namespace Modelling {
///Abstract base for
class UniformSpecification {
public:

  UniformSpecification(int const dim) : dim(dim) {}

  virtual ~UniformSpecification() {}

  int const dim;

  virtual Eigen::VectorXd Sample() = 0;

  ///Uniform over inputs where this > 0
  virtual double          PredicateFn(Eigen::VectorXd const& input) = 0;
  virtual double          Density(Eigen::VectorXd const& input)
  {
    return (PredicateFn(input) > 0.0) ? 1.0 : -std::numeric_limits<double>::infinity();
  }
};

class UniformBox : public UniformSpecification {
public:

  UniformBox(Eigen::VectorXd const& lb, Eigen::VectorXd const& ub);
  virtual ~UniformBox() = default;

  virtual Eigen::VectorXd Sample() override;

  ///Uniform over inputs where this > 0
  virtual double          PredicateFn(Eigen::VectorXd const& input) override;



  const Eigen::VectorXd lb;
  const Eigen::VectorXd ub;

private:
  Eigen::VectorXd const scale;
};

class UniformUserPredicate : public UniformSpecification {
public:

  UniformUserPredicate(std::function<double(Eigen::VectorXd const&)> const pred, int const dim) : UniformSpecification(
                                                                                                    dim),
                                                                                                  pred(pred)
  {}

  virtual ~UniformUserPredicate() = default;

  virtual Eigen::VectorXd Sample() override
  {
    assert(false); //cannot sample from arbitrary predicates
    return Eigen::VectorXd();
  }

  ///Uniform over inputs where this > 0
  virtual double PredicateFn(Eigen::VectorXd const& input) override
  {
    return pred(input);
  }

private:

  std::function<double(Eigen::VectorXd const&)> const pred;
};
}
}

#endif // UNIFORMSPECIFICATION_H
