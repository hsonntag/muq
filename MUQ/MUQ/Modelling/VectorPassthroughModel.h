
#ifndef VECTORPASSTHROUGHMODEL_H
#define VECTORPASSTHROUGHMODEL_H

#include "MUQ/Modelling/Density.h"
#include "MUQ/Modelling/ModPieceTemplates.h"

namespace muq {
namespace Modelling {
/**
 * Simply passes along its input, which is a single vector. Introduced so it can be the start of a diamond model,
 * such as a posterior where both prior and likelihood pull from this.
 **/

class VectorPassthroughModel : public OneInputFullModPiece {
public:

 VectorPassthroughModel(int const dim) : OneInputFullModPiece(dim, dim) {TurnCachingOff();}

  virtual ~VectorPassthroughModel() = default;

private:

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input)
  {
    return input;
  }

  virtual Eigen::VectorXd GradientImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity)
  {
    return sensitivity;
  }

  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input)
  {
    return Eigen::MatrixXd::Identity(outputSize, outputSize);
  }

  virtual Eigen::VectorXd JacobianActionImpl(Eigen::VectorXd const& input,  Eigen::VectorXd const& target)
  {
    return target;
  }

  virtual Eigen::MatrixXd HessianImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity)
  {
    return Eigen::MatrixXd::Zero(inputSizes[0], inputSizes[0]);
  }
};


class VectorPassthroughDensity : public Density {
public:

  VectorPassthroughDensity() : Density(Eigen::VectorXi::Constant(1, 1), true, true, true, true) {}

  virtual ~VectorPassthroughDensity() = default;

private:

  virtual double LogDensityImpl(std::vector<Eigen::VectorXd> const& input) override
  {
    return input.at(0) (0);
  }

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override
  {
    return input.at(0);
  }

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override
  {
    return sensitivity;
  }

  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input,
                                       int const                           inputDimWrt) override
  {
    return Eigen::MatrixXd::Identity(outputSize, outputSize);
  }

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt) override
  {
    return target;
  }

  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt) override
  {
    return Eigen::MatrixXd::Zero(inputSizes[0], inputSizes[0]);
  }
};
}
}
#endif // VECTORPASSTHROUGHMODEL_H
