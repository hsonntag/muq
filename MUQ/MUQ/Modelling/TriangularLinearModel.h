
#ifndef _TriangularLinearModel_h
#define _TriangularLinearModel_h

#include "MUQ/Modelling/LinearModel.h"

namespace muq {
namespace Modelling {
  
  /** \author Matthew Parno
 *  \brief Linear operator forward model using a lower triangular matrix.
 *  @ingroup Modelling
 *  @class TriangularLinearModel
 *  \details This class represents the action of a lower triangular matrix on the input..
 */
class TriangularLinearModel : public LinearModel {
public:


  /** Construct the LinearModel from an Eigen::MatrixXd */
  TriangularLinearModel(const Eigen::MatrixXd& Ain) : LinearModel(Ain){};
  TriangularLinearModel(const Eigen::VectorXd& bin, const Eigen::MatrixXd& Ain) : LinearModel(bin,Ain){};
  virtual ~TriangularLinearModel() = default;

protected:

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override{
    if (isAffine) {
      return A.triangularView<Eigen::Lower>() * input;
    } else {
      return A.triangularView<Eigen::Lower>() * input + b;
    }
  };

}; // class TriangularLinearModel

}// namespace Modelling
}// namespace muq


#endif
