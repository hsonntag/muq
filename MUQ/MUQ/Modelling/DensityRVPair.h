
#ifndef _DensityRVPair_h
#define _DensityRVPair_h


#include <memory>

namespace muq {
namespace Modelling {
///A very simple class that constructs a coordinated RV and Density based on a specification class.
template<typename RV, typename Density, typename SpecificationType>
class DensityRVPair {
public:

  ///Construct the specification, then pass it to the rv and density types.
  template<typename ... Args>
  DensityRVPair(Args ... args) : specification(std::make_shared<SpecificationType>(args ...)),
                                 rv(std::make_shared<RV>(specification)),
                                 density(std::make_shared<Density>(specification))
  {}


  virtual ~DensityRVPair() = default;


  std::shared_ptr<SpecificationType> const specification;


  std::shared_ptr<RV> const rv;

  std::shared_ptr<Density> const density;
};
}
}

#endif //_DensityRVPair_h
