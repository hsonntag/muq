
#ifndef SliceModel_h_
#define SliceModel_h_

#include "MUQ/Modelling/ModPieceTemplates.h"

namespace muq {
namespace Modelling {
/** @ingroup Modelling
 *   @class SliceModel
 *  @brief Extract a subset of the entries in a vector.
 *  @details In cases, we may only want to look at part of a vector or field.  The slice class allows us to extract this
 *     information from a larger expression
 */
class SliceModel : public OneInputFullModPiece {
public:

  /** Construct from a start index, end index, and skip interval.  Both indices are inclusive
   */
  SliceModel(int InputDim, int StartIndIn, int EndIndIn, int SkipIn = 1);

  /** Construct from a vector of the the indices to keep. */
  SliceModel(int InputDim, const std::vector<unsigned int>& IndicesIn);

  /** Fill in the update function -- this function just copies values from the input to this expressions state. */
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input);

  /** This funtion just stamps the SensIn vector into a longer vector for the input size, and returns the results in the
   *  GradVec vector.
   */
  virtual Eigen::VectorXd GradientImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity);

  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input);
  virtual Eigen::VectorXd JacobianActionImpl(Eigen::VectorXd const& input,  Eigen::VectorXd const& target);

  /* return the Hessian (a zero square matrix of inputSizes[0]) */
  virtual Eigen::MatrixXd HessianImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity);

protected:

  /** keep a vector of the indices to use. */
  std::vector<unsigned int> Indices;
};

// class SliceModel
} // namespace Modelling
} // namespace muq

#endif // ifndef SliceModel_h_
