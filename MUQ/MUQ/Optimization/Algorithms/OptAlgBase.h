//
//  OptBase.h
//
//
//  Created by Matthew Parno on 5/13/13.
//  Copyright (c) 2013 MIT. All rights reserved.
//

#ifndef _OptBase_h
#define _OptBase_h

// std library includes
#include <memory>
#include <map>

// eigen includes
#include <Eigen/Core>

// boost includes
#include <boost/property_tree/ptree.hpp>

#if MUQ_PYTHON == 1
# include <boost/python.hpp>
# include <boost/python/def.hpp>
# include <boost/python/class.hpp>

# include "MUQ/Utilities/python//PythonTranslater.h"
#endif // if MUQ_PYTHON == 1

// other muq includes
#include "MUQ/Optimization/Problems/OptProbBase.h"

namespace muq {
namespace Optimization {
template<typename T>
struct DerivedOptRegister;

/** @class OptAlgBase
 *   @ingroup Optimization
 *   @brief Abstract base and factory for optimization algorithms.
 *   @details This class provides an abstract base for unconstrained and constrained optimization algorithms for both
 *deterministic and stochastic problems.  The goal of the optimization module in general is to have a varity of
 *optimization algorithms, some using gradients, some derivative free, others geared toward stochastic problems, and
 *even others for use on nonlinear least squares problems.  This class provides a framework for defining optimization
 *algorithms, optimization problems, and constraints are defined as children of OptProbBase and OptConstBase
 * respectively.
 *
 *   Child classes of this base register themselves as well as the type of problems they operate on (i.e. determistic
 *constrained, deterministic unconstrained, stochastic unconstrained, least squares, etc...).
 *
 *   Additionally, child classes need to inform the base class of the types of problems they can handle directly, i.e.
 *Bound constrained problems, Linear constrained problems, or Nonlinear constrained problems.  To do this,
 *implementations should include the HANDLES_BOUND_DEC, HANDLES_LINEAR_DEC, or HANDLES_NONLINEAR_DEC macros in the
 *public part of the class definition.
 */
class OptAlgBase {
public:

  /** Construct this base from a problem pointer and a property list.  Currently, this only stores the problem ptr and
   * does nothing with the property tree.
   *  @param[in] ProbPtr A shared pointer to the optimization problem
   *  @param[in] properties A boost ptree holding any additional parameters that may be need by this class (currently
   *unused)
   */
  OptAlgBase(std::shared_ptr<OptProbBase> ProbPtr,
             boost::property_tree::ptree& properties,
             bool                         unconst,
             bool                         partBound,
             bool                         fullBound,
             bool                         linear,
             bool                         nonlinear,
             bool                         equality,
             bool                         inequality,
             bool                         stochastic = false);

  /** Create an instance of OptAlgBase to solve the optimization stored in ProbPtr using the algorithm and algorithm
   * parameters defined in the properties parameter list.
   *   @param[in] ProbPtr A shared_ptr to the optimization problem we want to solve.  The OptProbBase class knows what
   *type of problem it is (i.e. constrained, unconstrained, etc...) and this type will be checked against the type of
   *problems the algorithm described in the property tree can handle.
   *   @param[in] properties A property tree containing the optimization method and method parameters.  Note that for
   *constrained problems, the algorithm defined in the property tree may be a combination of unconstrained optimizer
   *with an algorithm for handling constrained (e.g. Augmented Lagrangian).
   */
  static std::shared_ptr<OptAlgBase> Create(std::shared_ptr<OptProbBase> ProbPtr,
                                            boost::property_tree::ptree& properties);

  /** Solve the optimization problem using x0 as a starting point for the iteration.
   *   @param[in] x0 The initial point for the optimizer.
   *   @return A vector holding the best point found by the optimizer.  Note that the optimizer may not have converged
   *and the GetStatus() function should be called to ensure that the algorithm converged and this vector is meaningful.
   */
  virtual Eigen::VectorXd    solve(const Eigen::VectorXd& x0) = 0;
  #if MUQ_PYTHON == 1
  inline boost::python::list PySolve(boost::python::list const& x0)
  {
    return muq::Utilities::GetPythonVector<Eigen::VectorXd>(solve(muq::Utilities::GetEigenVector<Eigen::
                                                                                                               VectorXd>(
                                                                           x0)));
  }

  #endif // if MUQ_PYTHON == 1

  /** Get the status of the optimization algorithm after solve has been called.  This function will return a positive
   * value for a successful optimization run and a negative value otherwise.  We follow the same error codes as NLOPT:
   *   @return The status of the optimization run:
   *           <ul>
   *               <li> 1 = Generic success </li>
   *               <li> 2 = Optimization stopped because goal objective was reached. </li>
   *               <li> 3 = Optimization stopped because change in objective was below relative or absolute tolerances.
   * </li>
   *               <li> 4 = Optimization stopped because change in position was below relative of absolute tolerances.
   * </li>
   *               <li> 5 = Optimization stopped because the maximum number of objective evaluations was reached. </li>
   *               <li> 6 = Optimization stopped because the maximum allowed running time was reached. </li>
   *               <li> -1 = Generic failure </li>
   *               <li> -2 = Invalid arguments to optimizer or solve has not been called yet. </li>
   *               <li> -3 = Ran out of memroy </li>
   *               <li> -4 = Optimization terminated because roundoff errors limited progress. (often still a useful
   * result)</li>
   *               <li> -5 = Forced termination </li>
   *           </ul>
   */
  virtual int GetStatus() const
  {
    return status;
  }

  /** Return a pointer to the optimization problem we want to solve.
   *  @return A shared_ptr to the optimization problem
   */
  virtual std::shared_ptr<OptProbBase> GetProb()
  {
    return OptProbPtr;
  }

  ///Typedef for the map used to track available opt types. Not user code.
  typedef std::map<std::string, std::function<
                     std::shared_ptr<OptAlgBase>(std::shared_ptr<OptProbBase>,
                                                 boost::property_tree::ptree&)> > AvailableOptMapType;

  ///Typedef for the type of the opt "constructor"
  typedef std::function<std::shared_ptr<OptAlgBase>(std
                                                    ::shared_ptr<OptProbBase>,
                                                    boost::property_tree::ptree&)> OptConstructorType;

  ///Globally available way to get the map that tells you what optimization methods are available.
  static std::shared_ptr<AvailableOptMapType> GetOptMap();


  // functions to filter the types of problem optimizers can handle
  const bool directlyHandlesUnconst;   // does this class directly handle unconstrained problems?
  const bool handlesPartlyBoundConsts; // does this class handle partly bound constrained problems?
  const bool requiresFullyBoundConsts; // does this class require fully bound constrained? problems
  const bool handlesLinearConsts;      // does this class handle linearly constrained problems?
  const bool handlesNonlinearConsts;   // does this class handle nonlinearly constrained problems?
  const bool handlesEqualityConsts;    // can this class handle equality constraints?
  const bool handlesInequalityConsts;  // can this class handle inequality constraints?
  const bool handlesStochastic;        // does this class handle stochastic problems?

  /// A shared pointer to the optimization problem we are working on
  std::shared_ptr<OptProbBase> OptProbPtr;

protected:

  /// The status of the optimization run, see GetStatus for possible values and their meanings
  int status = -2;

  /// optimization specific parameters.  Note that not all child classes will likely use all these parameters (like
  // gtol)
  double ctol, ftol, gtol, xtol, fabstol, stepLength;
  int    maxIts, verbose;
};

// class OptAlgBase


template<typename T>
struct DerivedOptRegister {
  DerivedOptRegister(std::string s)
  {
    OptAlgBase::GetOptMap()->insert(std::make_pair(s, static_cast<OptAlgBase::OptConstructorType>(T::Create)));
  }
};
} // namespace Optimization
} // namespace muq


#define  REGISTER_OPT_DEC_TYPE(NAME) \
  static std::shared_ptr<muq::Optimization::DerivedOptRegister<NAME> > reg;

//NOTE this requires a using namespace call
#define  REGISTER_OPT_DEF_TYPE(NAME)                                                          \
  std::shared_ptr<muq::Optimization::DerivedOptRegister<NAME> > NAME::reg = std::make_shared< \
    muq::Optimization::DerivedOptRegister<                                                    \
      NAME> >(# NAME);

#define REGISTER_OPT_CONSTRUCTOR(NAME) static std::shared_ptr<OptAlgBase> Create(std::shared_ptr<OptProbBase> ProbPtr, \
                                                                                 boost::property_tree::ptree &         \
                                                                                 properties)                           \
  {                                                                                                                    \
    return std::make_shared<NAME>(ProbPtr, properties);                                                                \
  };


#endif // ifndef _OptBase_h
