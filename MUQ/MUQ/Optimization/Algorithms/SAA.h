
#ifndef _SAA_h
#define _SAA_h

#include "MUQ/Optimization/Algorithms/OptAlgBase.h"

namespace muq {
namespace Optimization {
/**
 *  @class SAA
 *  @ingroup Optimization
 *  @brief Sample Average Approximation solver for stochastic/robust optimization problems
 *  @details Sample Average Approximation methods (SAA) work by transforming a stochastic optimization problem into a
 *     deterministic problem by fiddling around with the way we generate random random numbers to compute the
 * expectation
 *     approximations.  By always using the same seed, the same random vectors will be generated, and the problem
 * becomes
 *     deterministic.  Then, you can use your favorite deterministic solver to perform the optimization.
 *
 *    NOTE: For SAA to function properly, all randomness in the objective must come from the RandomGenerator class in
 *       muq::utilities.  SAA will set call RandomGenerator::SetSeed() and this should fix the seed for all
 * random
 *       numbers used in the objective.
 */
class SAA : public OptAlgBase {
public:

  REGISTER_OPT_CONSTRUCTOR(SAA)
  /** Construc an SAA solver using the parameters in the property tree, the property tree should contain the
   *  deterministic solver to use in Opt.SAA.Method
   *  @param[in] ProbPtr A shared pointer to the object function.  In this method, we allow this objective to be
   *     stochastic, meaning that each time the eval() method is called at the same point, a different value can be
   *     returned.
   *  @param[in] properties A boost ptree containing the deterministic solver to use and other SAA and general
   *     optimization parameters
   */
  SAA(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties);

  /** Solve the stochastic optimization problem with a deterministic solver by setting the random seed everytime the
   *  objective is called, which approximates the true stochastic problem by a deterministic problem.  The optimization
   *  may be repeated a few times with different seeds and the mean solution returned.
   *  @param[in] xStart The initial position for the optimization routine
   */
  virtual Eigen::VectorXd solve(const Eigen::VectorXd& x0);

protected:

  // keep a smart pointer to the deterministic optimization routine
  std::shared_ptr<OptAlgBase> DeterministicSolver;

  // how many optimization runs do we want to perform?
  int NumRuns;

  // a boost property tree defining the subproblem deterministic solver
  boost::property_tree::ptree subProp;

private:

  REGISTER_OPT_DEC_TYPE(SAA)
};
} // namespace Optimization
} // namespace muq


#endif // ifndef _SAA_h
