/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *  USA.
 *
 *  MIT UQ Library
 *  Copyright (C) 2013 MIT
 */

#ifndef CONSTRAINTBASEPYTHON_H_
#define CONSTRAINTBASEPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#include "MUQ/Optimization/Constraints/ConstraintBase.h"
#include "MUQ/Optimization/Constraints/LinearConstraint.h"

namespace muq {
namespace Optimization {
/// A python wrapper around muq::optimization::ConstraintBase
class ConstraintBasePython : public ConstraintBase, public boost::python::wrapper<ConstraintBase> {
public:

  /// Constructor
  ConstraintBasePython(int DimInIn, int DimOutIn) : ConstraintBase(DimInIn, DimOutIn) {}

  /// Allow user to implement evaluate in python
  virtual boost::python::list PyEval(boost::python::list const& xc) override
  {
    return this->get_override("Eval") (xc);
  }

  /// Allow user to implement apply jacobian transpose in python
  virtual boost::python::list PyApplyJacTrans(boost::python::list const& xc, boost::python::list const& vecIn) override
  {
    return this->get_override("ApplyJacTrans") (xc, vecIn);
  }

private:

  /// Call user implemented python evaluate
  virtual Eigen::VectorXd eval(Eigen::VectorXd const& xc) override
  {
    return muq::Utilities::GetEigenVector<Eigen::VectorXd>(PyEval(muq::Utilities::GetPythonVector<Eigen::
                                                                                                                VectorXd>(
                                                                           xc)));
  }

  /// Call user implemented python apply jacobian transpose
  virtual Eigen::VectorXd ApplyJacTrans(Eigen::VectorXd const& xc, Eigen::VectorXd const& vecIn) override
  {
    return muq::Utilities::GetEigenVector<Eigen::VectorXd>(PyApplyJacTrans(muq::Utilities::GetPythonVector
                                                                                  <Eigen::VectorXd>(xc),
                                                                                  muq::Utilities::GetPythonVector
                                                                                  <Eigen::VectorXd>(vecIn)));
  }
};

/// A python wrapper around muq::optimization::LinearConstraint
class LinearConstraintPython : public LinearConstraint, public boost::python::wrapper<LinearConstraint> {
public:

  LinearConstraintPython(boost::python::list const& A, boost::python::list const& b) : LinearConstraint(muq::Utilities::GetEigenMatrix(
                                                                                                          A),
                                                                                                        muq::Utilities::GetEigenVector<
                                                                                                          Eigen::
                                                                                                          VectorXd>(b))
  {}
};

void ExportConstraintBase();
} // namespace muq
} // namespace optimization

#endif // ifndef CONSTRAINTBASEPYTHON_H_
