#!/bin/bash

if git rev-parse --verify HEAD >/dev/null 2>&1
then
	against=HEAD
else
	# Initial commit: diff against an empty tree object
	against=4b825dc642cb6eb9a060e54bf8d69288fbee4904
fi

uncrustify -v foo >/dev/null 2>&1 || { echo >&2 "I require uncrustify but it's not installed.  Aborting."; exit 1; }

if [ ! -f uncrustify.cfg ]; then
 printf "Error: uncrustify.cfg not found"
 exit 1
fi


# create a random filename to store our generated patch
prefix="muq-pre-commit-uncrustify"
suffix="$(date +%s)"
patch="/tmp/$prefix-$suffix.patch"

# clean up any older uncrustify patches
$DELETE_OLD_PATCHES && rm -f /tmp/$prefix*.patch

#need so we reformat the version that will be committed, not the version in the working tree
#http://danilodellaquila.com/blog/use-git-stash-in-your-pre-commit-hook
git stash -q --keep-index

FILE_EXTS=".c .h .cpp .hpp"

PARSE_EXTS=true

# create one patch containing all changes to the files
git diff-index --cached --diff-filter=ACMR --name-only $against -- | while read file;
do
    # ignore file if we do check for file extensions and the file
    # does not match any of the extensions specified in $FILE_EXTS

    if [[ $file != *.cpp ]] && [[ $file != *.h ]]; then
        continue;
    fi


    # uncrustify our sourcefile, create a patch with diff and append it to our $patch
    # The sed call is necessary to transform the patch from
    #    --- $file timestamp
    #    +++ - timestamp
    # to both lines working on the same file and having a a/ and b/ prefix.
    # Else it can not be applied with 'git apply'.
   uncrustify -c uncrustify.cfg -f "$file" | \
        diff -u "$file" - | \
        sed -e "1s|--- |--- a/|" -e "2s|+++ -|+++ b/$file|" >> "$patch"
done

# if no patch has been generated all is ok, clean up the file stub and exit
if [ ! -s "$patch" ] ; then
    printf "Files in this commit comply with the uncrustify rules.\n"
    rm -f "$patch"
git stash pop -q
    exit 0
fi

# a patch has been created, notify the user and exit
printf "\nThe following differences were found between the code to commit "
printf "and the uncrustify rules:\n\n"
cat "$patch"

printf "\nYou can apply these changes with:\n git apply $patch\n"
printf "(may need to be called from the root directory of your repository)\n"
printf "Aborting commit. Apply changes and commit again or skip checking with"
printf " --no-verify (not recommended).\n"
git stash pop -q
exit 1











